<?php

use App\Models\Evenements;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateEvenements extends Migration
{
    private $model;

    public function __construct()
    {
        $this->model = new Evenements();
    }
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::statement('CREATE TABLE ' . $this->model->getTable() . ' LIKE spip_' . $this->model->getTable() . '; ');
        DB::statement('INSERT IGNORE INTO ' . $this->model->getTable() . '  SELECT * FROM spip_' . $this->model->getTable() . '; ');

        DB::statement('CREATE TABLE evenements_participants LIKE spip_evenements_participants; ');
        DB::statement('INSERT IGNORE INTO evenements_participants SELECT * FROM spip_evenements_participants; ');

        Schema::table($this->model->getTable(), function (Blueprint $table) {
            $table->string('adresse', 255)->nullable()->change();
            $table->string('places', 255)->default(0)->nullable()->change();
            $table->string('couleur', 50)->nullable();
            $table->string('url', 255)->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {

        Schema::table($this->model->getTable(), function (Blueprint $table) {
            $table->dropColumn('couleur');
            $table->dropColumn('url');
        });

        DB::statement('DROP TABLE ' . $this->model->getTable() . ';');
        DB::statement('DROP TABLE evenements_participants;');

    }
}
