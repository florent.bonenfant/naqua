<?php

use App\Models\Auteurs;
use App\Models\Registration;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;

class UpdateInscriptionIdAuteur extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {

        $crawlRegistration = new Registration();
        $back = $crawlRegistration->all();

        Schema::table('inscriptions', function (Blueprint $table) {
            // $table->renameColumn('certificats', 'certificat');
            $table->string('teldom')->nullable()->change();
            $table->string('telburo')->nullable()->change();
            $table->string('fax')->nullable()->change();
            $table->string('serviceclub')->nullable()->change();
            $table->string('remarques')->nullable()->change();
            $table->string('nomaccident')->nullable()->change();
            $table->string('lienaccident')->nullable()->change();
            $table->string('telaccident')->nullable()->change();
            $table->string('adresseaccident')->nullable()->change();
            $table->string('anneepi')->default(0)->nullable()->change();
            $table->date('datenaiss')->change();
            $table->date('datederbrev')->change();
            $table->boolean('rgpdaccident');
            // $table->dropColumn(['reglementinterieurok']);
        });

        foreach ($back as $el) {
            $el->datenaiss = $this->conversionDate($el->datenaiss);
            $el->datederbrev = $this->conversionDate($el->datederbrev);
            $el->allergieaspirine = ($el->allergieaspirine == 'oui' ? 1 : 0);
            $el->interetnagelibre = ($el->interetnagelibre == 'oui' ? 1 : 0);
            $el->interetapnee = ($el->interetapnee == 'oui' ? 1 : 0);
            $el->interetpalmes = ($el->interetpalmes == 'oui' ? 1 : 0);
            $el->interetperfect = ($el->interetperfect == 'oui' ? 1 : 0);
            $el->interetsecourisme = ($el->interetsecourisme == 'oui' ? 1 : 0);
            $el->update();
        }

        Schema::table('inscriptions', function (Blueprint $table) {
            $table->renameColumn('datenaiss', 'datenaissance');
            $table->renameColumn('datederbrev', 'dernierbrevet');
            $table->boolean('allergieaspirine')->change();
            $table->boolean('interetnagelibre')->change();
            $table->boolean('interetapnee')->change();
            $table->boolean('interetpalmes')->change();
            $table->boolean('interetperfect')->change();
            $table->boolean('interetsecourisme')->change();
        });

        $listeAuteurs = Auteurs::getWithMail();
        // on set en premier lieu les mails communs
        foreach ($listeAuteurs as $auteur) {
            DB::table('inscriptions')
                ->where('email', $auteur->email)
                ->update(['id_auteur' => $auteur->id_auteur]);
        }
        // on termine par l'association des prénoms si ils sont unique, désolé Pauline...
        foreach ($listeAuteurs as $auteur) {
            DB::table('inscriptions')
                ->where('prenom', '<>', $auteur->nom)
                ->where('prenom', '<>', 'pauline')
                ->where('id_auteur', 0)
                ->update(['id_auteur' => $auteur->id_auteur]);
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('inscriptions', function (Blueprint $table) {
            $table->renameColumn('datenaissance', 'datenaiss');
            $table->renameColumn('dernierbrevet', 'datederbrev');
            $table->dropColumn('rgpdaccident');
            // $table->renameColumn('certificat', 'certificats');
        });

    }

    private function conversionDate($date)
    {
        if (strpos($date, '/') !== false) {
            $retourDate = explode('/', $date);
            $j = str_pad($retourDate[0], 2, 0, STR_PAD_LEFT);
            $m = str_pad($retourDate[1], 2, 0, STR_PAD_LEFT);
            $y = str_pad($retourDate[2], 4, 0, STR_PAD_LEFT);
            return $y . '-' . $m . '-' . $j;
        }
    }
}
