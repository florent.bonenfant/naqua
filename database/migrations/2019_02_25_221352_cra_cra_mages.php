<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\Storage;
use Intervention\Image\Facades\Image as InterventionImage;
use App\Models\File;

/**
 * Superbe migration pour les images, c'est hyper dégueu mais ça fait le café !
 */
class CraCraMages extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {

        // création des dossiers
        if (!file_exists(public_path(DISK_PATH_IMG))) {
            mkdir(public_path(DISK_PATH_IMG));
            touch(public_path(DISK_PATH_IMG . DIRECTORY_SEPARATOR . 'index.html'));
        }
        if (!file_exists(public_path(DISK_PATH_IMG_FULL))) {
            mkdir(public_path(DISK_PATH_IMG_FULL));
            touch(public_path(DISK_PATH_IMG_FULL . DIRECTORY_SEPARATOR . 'index.html'));
        }
        if (!file_exists(public_path(DISK_DRIVER_IMG_STD))) {
            mkdir(public_path(DISK_DRIVER_IMG_STD));
            touch(public_path(DISK_DRIVER_IMG_STD . DIRECTORY_SEPARATOR . 'index.html'));
        }
        if (!file_exists(public_path(DISK_DRIVER_IMG_TH))) {
            mkdir(public_path(DISK_DRIVER_IMG_TH));
            touch(public_path(DISK_DRIVER_IMG_TH . DIRECTORY_SEPARATOR . 'index.html'));
        }
        if (!file_exists(public_path(DISK_PATH_ARCHIVES))) {
            mkdir(public_path(DISK_PATH_ARCHIVES));
            touch(public_path(DISK_PATH_ARCHIVES . DIRECTORY_SEPARATOR . 'index.html'));
        }
        if (!file_exists(public_path(DISK_PATH_CERTIFICATS))) {
            mkdir(public_path(DISK_PATH_CERTIFICATS));
            touch(public_path(DISK_PATH_CERTIFICATS . DIRECTORY_SEPARATOR . 'index.html'));
        }
        if (!file_exists(public_path(DISK_PATH_DOCS))) {
            mkdir(public_path(DISK_PATH_DOCS));
            touch(public_path(DISK_PATH_DOCS . DIRECTORY_SEPARATOR . 'index.html'));
        }
        if (!file_exists(public_path(DISK_PATH_AVATARS))) {
            mkdir(public_path(DISK_PATH_AVATARS));
            touch(public_path(DISK_PATH_AVATARS . DIRECTORY_SEPARATOR . 'index.html'));
        }
        if (!file_exists(public_path(DISK_PATH_UI))) {
            mkdir(public_path(DISK_PATH_UI));
            touch(public_path(DISK_PATH_UI . DIRECTORY_SEPARATOR . 'index.html'));
        }
        $listeFichiers = File::all();

        foreach ($listeFichiers as $fichier) {
            $fichier->fichier = trim(strstr($fichier->fichier, '/'), '/');
            $fichier->save();
        }
        if (!file_exists(public_path('IMG'))) {
            print('Attention le répertoire image de Spip ne semble pas être ici.');
            return;
        } else {
            $folders = scandir(public_path('IMG'));
        }

        // copie des fichiers
        foreach ($folders as $folder) {
            if (is_dir(public_path('IMG/' . $folder))) {
                // gestion des sous dossiers
                foreach (scandir(public_path('IMG/' . $folder)) as $fichier) {
                    switch ($folder) {
                        case 'jpg':
                            if (is_file(public_path('IMG/' . $folder . '/' . $fichier)) && strpos($fichier, 'jpg') !== false) {
                                copy(public_path('IMG/' . $folder . '/' . $fichier), public_path(DISK_PATH_IMG_FULL . $fichier));

                                // gestion miniatures
                                $image = InterventionImage::make(public_path(DISK_PATH_IMG_FULL . $fichier))->widen(IMG_MAX_SIZE_STD);
                                Storage::disk(DISK_DRIVER_IMG_STD)->put($fichier, $image->encode());
                                $image = InterventionImage::make(public_path(DISK_PATH_IMG_FULL . $fichier))->widen(IMG_MAX_SIZE_TH);
                                Storage::disk(DISK_DRIVER_IMG_TH)->put($fichier, $image->encode());
                            }
                            break;
                        case 'pdf':
                            if (is_file(public_path('IMG/' . $folder . '/' . $fichier))) {
                                copy(public_path('IMG/' . $folder . '/' . $fichier), public_path(DISK_PATH_DOCS . $fichier));
                            }
                            break;
                        case 'zip':
                            if (is_file(public_path('IMG/' . $folder . '/' . $fichier))) {
                                copy(public_path('IMG/' . $folder . '/' . $fichier), public_path(DISK_PATH_ARCHIVES . $fichier));
                            }
                            break;
                    }
                }
            } elseif (is_file(public_path('IMG/' . $folder))) {
                // traitement des fichiers
                if (strpos($folder, 'auton') !== false) {
                    copy(public_path('IMG/' . $folder), public_path(DISK_PATH_AVATARS . $folder));
                } else {
                    copy(public_path('IMG/' . $folder), public_path(DISK_PATH_UI . $folder));
                }
            }
        }
        // Suppression de la source
        shell_exec('rm -rf ' . public_path('IMG/'));

        // on change le chemin des fichiers, on ne regardera pas le dossier de destination des archives ou document de spip -_-'
        $listeFichiers = File::all();

        // foreach ($listeFichiers as $fichier) {
        //     $fichier->fichier = trim(strstr($fichier->fichier, '/'), '/');
        //     $fichier->save();
        // }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
