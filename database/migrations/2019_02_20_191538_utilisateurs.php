<?php

use App\Mail\InitUstisateur;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Mail;

class Utilisateurs extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->integer('role')
                ->after('email')
                ->default(1);
        });

        $users = DB::select('SELECT a.id_auteur, i.nom, i.prenom, a.bio, i.email, a.statut, a.prefs
        FROM auteurs a
        INNER JOIN inscriptions i ON i.id_auteur = a.id_auteur
        GROUP BY a.id_auteur');

        $insert = 'INSERT IGNORE INTO users (`id`, `name`, `email`, `password`, `remember_token`, `role`, `created_at`, `updated_at`) VALUES ';
        foreach ($users as $user) {
            $acl = (substr($user->statut, 0, 1) == '6' ? 2 : 8);
            $insert .= '("' . $user->id_auteur . '", "' . ucfirst(strtolower($user->prenom)) . ' ' . strtoupper($user->nom) . '", "' . $user->email . '", "' . Hash::make(Str::random(20)) . '", "' . Str::random(60) . '", "' . $acl . '", NOW(), NULL),';

            Mail::to($user->email)
                ->send(new InitUstisateur($user));
        }

        DB::statement(trim($insert, ','));
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::statement('TRUNCATE `users`');
    }
}
