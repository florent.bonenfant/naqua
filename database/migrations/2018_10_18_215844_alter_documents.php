<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\Facades\DB;

class AlterDocuments extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {

        DB::statement('CREATE TABLE documents LIKE spip_documents; ');
        DB::statement('INSERT IGNORE INTO documents SELECT * FROM spip_documents; ');

        DB::statement('CREATE TABLE documents_liens  LIKE spip_documents_liens ; ');
        DB::statement('INSERT IGNORE INTO documents_liens  SELECT * FROM spip_documents_liens ; ');

        DB::statement('CREATE TABLE articles LIKE spip_articles; ');
        DB::statement('INSERT IGNORE INTO articles  SELECT * FROM spip_articles; ');

        DB::statement('CREATE TABLE auteurs_rubriques LIKE spip_auteurs_rubriques; ');
        DB::statement('INSERT IGNORE INTO auteurs_rubriques SELECT * FROM spip_auteurs_rubriques; ');

        DB::statement('CREATE TABLE auteurs_articles LIKE spip_auteurs_articles; ');
        DB::statement('INSERT IGNORE INTO auteurs_articles SELECT * FROM spip_auteurs_articles; ');

        DB::statement('CREATE TABLE auteurs LIKE spip_auteurs; ');
        DB::statement('INSERT IGNORE INTO auteurs SELECT * FROM spip_auteurs; ');

        DB::statement('CREATE TABLE urls LIKE spip_urls; ');
        DB::statement('INSERT IGNORE INTO urls SELECT * FROM spip_urls; ');

        DB::statement('CREATE TABLE rubriques LIKE spip_rubriques; ');
        DB::statement('INSERT IGNORE INTO rubriques  SELECT * FROM spip_rubriques; ');

        // DB::statement('CREATE TABLE zones_auteurs LIKE spip_zones_auteurs; ');
        // DB::statement('INSERT IGNORE INTO zones_auteurs SELECT * FROM spip_zones_auteurs; ');

        // DB::statement('CREATE TABLE zones_rubriques LIKE spip_zones_rubriques; ');
        // DB::statement('INSERT IGNORE INTO zones_rubriques SELECT * FROM spip_zones_rubriques; ');

        Schema::table('documents', function (Blueprint $table) {
            $table->string('dossier', 100)
                ->after('descriptif')
                ->default('images/full/');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::statement('DROP TABLE documents;');

        DB::statement('DROP TABLE documents_liens ;');

        DB::statement('DROP TABLE articles;');

        DB::statement('DROP TABLE auteurs_rubriques;');

        DB::statement('DROP TABLE auteurs_articles;');

        DB::statement('DROP TABLE auteurs;');

        DB::statement('DROP TABLE urls;');

        DB::statement('DROP TABLE rubriques;');

        // DB::statement('DROP TABLE zones_auteurs;');
        // DB::statement('DROP TABLE zones_rubriques;');
    }
}
