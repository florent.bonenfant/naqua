<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMeta extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('meta', function (Blueprint $table) {
            $table->increments('meta_id');
            $table->string('cle', 100)->nullable();
            $table->string('type', 50)->nullable();
            $table->string('valeur', 255);
            $table->timestamps();
        });

        DB::table('meta')->insert(
            array(
                'cle' => 'mail_contact',
                'type' => 'config',
                'valeur' => 'florent@niweb.eu'
            )

            //@todo tresorerie
        );
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('meta');
    }
}
