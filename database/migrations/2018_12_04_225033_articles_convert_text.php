<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use App\Models\Articles;
use App\Models\File;

class ArticlesConvertText extends Migration
{
    private $model;

    public function __construct()
    {
        $this->model = new Articles();
    }

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $listeFichiers = File::all();
        foreach ($listeFichiers as $fichier) {
            $fichier->fichier = trim(strstr($fichier->fichier, '/'), '/');
            $fichier->save();
        }

        $listeArticles = Articles::all();
        foreach ($listeArticles as $article) {
            $article->texte = $this->convert($article->texte);
            $article->save();
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
    }

    public function convert($texte)
    {

        // suppression des tirets inutiles
        $texte = str_replace(
            [ '_' ],
            ['' ],
            $texte
            );

        // création des liens hypertexte et des ancres
        $texte = preg_replace_callback(
            '`\[(.*)(->|<-)(.*)\]`',
                function ($matche) {
                    if ($matche[2] == '->') {
                        if (substr($matche[3], 0, 3) === 'doc' && $this->isInteger(substr($matche[3], 3))) {
                            $image = (new File())->find(substr($matche[3], 3));
                            if (is_object($image)) {
                                if ($image->mode === 'document') {
                                    $matche[3] = (DISK_PATH_DOCS . $image->fichier);
                                } elseif ($image->mode === 'image') {
                                    $matche[3] = url(DISK_PATH_IMG_STD . $image->fichier);
                                } else {
                                    $matche[3] = url(DISK_PATH_DOCS . $image->fichier);
                                }
                            }
                        }
                        return '<a href="' . $matche[3] . '" class="spip_ancre">' . $matche[1] . '</a>';
                    } else {
                        return '<a name="' . $matche[1] . '"></a>';
                    }
                },
                $texte
            );

        // gestion des listes
        $texte = preg_replace_callback(
            '`\-\*(.*)`',
            function ($matche) {
                return '<li>'.$matche[1].'</li>';
            },
            $texte
        );

        $addUL= explode("\r\n", $texte);
        foreach ($addUL as $k => $ligne) {
            if (substr($ligne, 0, 4) === '<li>') {
                $addUL[$k] = '<ul>' . PHP_EOL . str_replace(["\r", "\n"], ['', ''], $ligne) . PHP_EOL . '</ul>';
            }
        }
        $texte = implode("\r\n", $addUL);

        // gestion des autres balises
        $texte = str_replace(
                ['{{', '}}', '{1{', '}1}', '{2{', '}2}', '[*', '*]', '_ ' ],
                ['<b>', '</b>', '<h3 class="spip">', '</h3>', '<h4 class="spip">', '</h4>', '<strong class="caractencadre-spip spip">', '</strong>', '' ],
                $texte
            );

        $conversion = false;
        if (strpos($texte, '</') === false) {
            $conversion = true;
        }

        // creation des liens hypertextes la ou il n'y en a pas
        $texte = preg_replace_callback(
            '`https?://[a-zA-Z0-9\.-]+\.[a-zA-Z]{2,4}(/\S*|)?`',
            function ($matche) {
                return '<a href="' . $matche[0] . '" target="_blank" class="spip_ancre">' . $matche[0] . '</a>';
            },
            $texte
        );

        // gestion des documents
        $texte = str_replace(array('<doc', '|left>', '|right>', '|center>'), array('[doc', '/]', '/]', '/]',), $texte);
        $texte = preg_replace_callback(
                '`\[doc(.*)\/]`isU',
            function ($idImg) {
                $image = (new File())->find($idImg[1]);
                if ($image['mode'] === 'document') {
                    return '<a href="' . url(DISK_PATH_DOCS . $image['fichier']) . '" title="' . $image['titre'] . '">' . (empty($image['descriptif']) ? $image['fichier'] : $image['descriptif']) . '</a>';
                } elseif ($image['mode'] === 'image') {
                    return '<img src="' . url(DISK_PATH_IMG_STD . $image['fichier']) . '" title="' . $image['titre'] . '" alt="' . $image['desc'] . '"/>';
                } else {
                    return '<a href="' . url(DISK_PATH_DOCS . $image['fichier']) . '" title="' . $image['titre'] . '">' . (empty($image['descriptif']) ? $image['fichier'] : $image['descriptif']) . '</a>';
                }
            },
            $texte
        );

        // conversion des retour chariots en dernier pour ne pas perturber les autres traitements
        if ($conversion) {
            $texte = nl2br($texte);
        }

        return $texte;
    }
}
