<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'AccueilController@index')->name('accueil.index');
Auth::routes();
Route::get('logout', 'Auth\LoginController@logout');

Route::get('/accueil', 'AccueilController@index')->name('accueil.index');
Route::get('/article/{id}', 'ArticleController@show')->name('article.show');
Route::get('/category/{id}', 'CategoryController@show')->name('category.show');
Route::get('/Inscription', 'InscriptionController@create')->name('inscription.create');
Route::post('/InscriptionStore', 'InscriptionController@store')->name('inscription.store');
Route::get('/Contact', 'ContactController@create')->name('contact.show');
Route::get('/contact/check/{contact}/{token}', 'ContactController@validation')->name('contact.check');
Route::post('/contact', 'ContactController@store')->name('contact.store');

Route::prefix('adm13')->name('admin.')->namespace('Back')->middleware('can:gate-connexion')->group(function () {
    Route::get('/', 'CategoryController@index')->name('back.home');
    // Route::get('/', 'IndexController@index')->name('back.home');

    Route::post('/article/addFile', 'ArticleController@addFile')->name('article.addFile');
    Route::post('/article/delFile{id}', 'AjaxController@delFile')->name('ajax.delFile');
    Route::resource('/article', 'ArticleController');
    Route::get('/mes-articles', 'ArticleController@my')->name('article.my');;
    Route::post('/export-membres', 'RegistrationController@export')->name('registration.export');;
    Route::resource('/registration', 'RegistrationController');
    Route::post('/category/create', 'CategoryController@create')->name('category.create');
    Route::post('/category/addChildren', 'CategoryController@addChildren')->name('category.addChildren');
    Route::post('/category/addFile', 'CategoryController@addFile')->name('category.addFile');
    Route::post('/category/delFile/{idCategorie}', 'CategoryController@delFile')->name('category.delFile');
    Route::resource('/category', 'CategoryController');
    Route::resource('/file', 'FileController');
    Route::resource('/evenement', 'EventController');


});


// Route::get('/home', 'HomeController@index')->name('home');

Route::get('{url}', 'DynamicController@manage')
        ->where('url', '([A-Za-z0-9-_]+)');


Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
