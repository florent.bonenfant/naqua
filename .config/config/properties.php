<?php

$unix_username = exec('whoami');
$git_name      = exec('git config user.name');
$git_email     = exec('git config user.email');
$project_slug  = 'Naqua';

return [
    // Local Config
    'local' => [
        'GIT_PATH' => [
            'question' => 'Path to git',
            'default' => '/usr/local/bin/git',
        ],
        'UNZIP_PATH' => [
            'question' => 'Path to unzip',
            'default' => '/usr/bin/unzip',
        ],

        /**
         * USER
         */
        'DEVELOPER_MAIL' => [
            'question' => 'Developer email',
            'default'  => $git_email,
        ],
        'DEVELOPER_NAME' => [
           'question' => 'Developer name',
           'default'  => $git_name,
        ],
    ],

    // Project constant
    'settings' => [
        'APP_NAME' => $project_slug,
        'SEEDS_FOLDER' => 'db/seeds',
        'MIGRATIONS_FOLDER' => 'db/migrations',
        'EMAIL_STORE' => 'false',
    ],

    // Project configuration
    'config' => [
        /**
         * DATABASE
         */
        'DB_NAME' => [
            'question' => 'Database name',
            'default' => strtolower($project_slug),
        ],
        'DB_USER' => [
            'question' => 'Database username',
            'default' => 'username',
        ],
        'DB_PWD' => [
            'question' => 'Database password',
            'default' => 'password',
        ],
        'DB_HOST' => [
            'question' => 'Database host',
            'default' => '127.0.0.1',
        ],
        'DB_PORT' => [
            'question' => 'Database port',
            'default' => '3306',
        ],
        'DB_DRIVER' => [
            'question' => 'Database driver',
            'default' => 'mysqli',
        ],

        /**
         * APP CONF
         */
        'APP_ENV' => [
            'question' => 'Environment',
            'choices' => ['local', 'develop', 'production'],
            'default' => 'develop'
        ],
        'WEB_SCHEME' => [
            'question' => 'Site scheme',
            'choices' => ['http', 'https'],
            'default' => 'http',
        ],
        'APPLI_DOMAIN' => [
            'question' => 'Application domain',
            'default' => 'localhost',
        ],
        'APPLI_PATH' => [
            'question' => 'Application base path',
            'default' => '/' . strtolower($project_slug) . '/public',
            'formater' => function ($value) {
                return '/' . ltrim($value, '/');
            }
        ],
        'APPLI_DIRECTORY_PATH' => [
            'question' => 'Application directory path',
            'default' => dirname(__DIR__, 2),
        ],

        /**
         * MAIL CONF
         */
        'MAIL_DRIVER' => [
            'question' => 'Mail server',
            'default' => 'smtp',
        ],
        'MAIL_HOST' => [
            'question' => 'Mail host',
            'default' => 'localhost',
        ],
        'MAIL_PORT' => [
            'question' => 'Mail port',
            'default' => '1025',
        ],
        'MAIL_USERNAME' => [
            'question' => 'Mail username',
            'default' => 'null',
        ],
        'MAIL_PASSWORD' => [
            'question' => 'Mail password',
            'default' => 'null',
        ],
        'MAIL_ENCRYPTION' => [
            'question' => 'Mail encryption',
            'default' => 'null',
        ],
    ]
];
