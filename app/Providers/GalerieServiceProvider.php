<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;

class GalerieServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }

    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->singleton('Galerie', function($app) {
            return new Galerie();
        });
    }
}
