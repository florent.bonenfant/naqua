<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;

class UrlsServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }

    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        require_once base_path('Helpers/Urls.php');//;app_path() . '/Helpers/Urls.php';
    }
}
