<?php

namespace App\Providers;

use Illuminate\Support\Facades\Gate;
use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;
use App\Http\Controllers\InscriptionController;
use App\Policies\InscriptionPolicy;
// use App\Http\Controllers\Back\RegistrationController;

class AuthServiceProvider extends ServiceProvider
{
    /**
     * The policy mappings for the application.
     *
     * @var array
     */
    protected $policies = [
        'App\Model' => 'App\Policies\ModelPolicy',
        Registration::class => InscriptionPolicy::class,
    ];

    /**
     * Register any authentication / authorization services.
     *
     * @return void
     */
    public function boot()
    {
        $this->registerPolicies();

        // vérification droits
        Gate::define('gate-connexion', function($user) {
            return isStaff();
        });

        Gate::define('gate-staff', function($user) {
            return isStaff();
        });

        Gate::define('gate-admin', function($user) {
            return isAdmin();
        });

        Gate::define('gate-author', function($user) {
            return isAuthor();
        });

        $this->authorOfArticle();
        $this->authorOfCategory();
    }

    /**
     * Autorise le staff et les auteurs d'un article
     *
     * @return    [bool]    [return description]
     */
    private function authorOfArticle() {
        Gate::define('gate-author-of-article', function($user, $article) {
            if (isStaff()) {
                return true;
            }

            foreach($article->Auteurs as $articleUser) {
                if ($articleUser->id === $user->id) {
                    return true;
                }
            }

            return false;
        });
    }

    /**
     * Autorise les auteurs et les auteurs d'une catégorie à afficher la page
     *
     * @return    [bool]    [return description]
     */
    private function authorOfCategory() {
        Gate::define('gate-author-of-category', function($user, $category) {
            if (isAuthor()) {
                return true;
            }

            if (count($category->Auteurs) === 0 && $category->statut !== Category::ETAT_PUBLIE) {
                return true;
            }

            foreach($category->Auteurs as $categoryUser) {
                if ($categoryUser->id === $user->id) {
                    return true;
                }
            }

            return false;
        });
    }
}
