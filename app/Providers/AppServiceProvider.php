<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        \Response::macro('downloadCsv', function ($content, $fichier) {
            $headers = [
                'Content-type'        => 'text/csv; charset=UTF-8',
                'Content-Disposition' => 'attachment; filename="'. $fichier .'"',
            ];

            return \Response::make($content, 200, $headers);
        });
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
