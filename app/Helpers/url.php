<?php

use App\Models\Urls;

if (!function_exists('getUrlByTypeAndId')) {
    function getUrlByTypeAndId($type, $id)
    {
        $model = new Urls();
        return $model->getByTypeAndId($type, (int) $id);
    }
}

if (!function_exists('getUrlByUrl')) {
    function getUrlByUrl($slug)
    {
        $model = new Urls();
        return $model->getByUrl($slug);
    }
}