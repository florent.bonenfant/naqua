<?php


if (!function_exists('isGod')) {
    function isGod()
    {
        if (auth()->check() && Auth::user()->isGod()) {
            return Auth::user();
        }
        return false;
    }
}

if (!function_exists('isAdmin')) {
    function isAdmin()
    {
        if (auth()->check() && Auth::user()->isAdmin()) {
            return Auth::user();
        }
        return false;
    }
}

if (!function_exists('isStaff')) {
    function isStaff()
    {
        if (auth()->check() && Auth::user()->isStaff()) {
            return Auth::user();
        }
        return false;
    }
}

if (!function_exists('isAuthor')) {
    function isAuthor()
    {
        if (auth()->check() && Auth::user()->isAuthor()) {
            return Auth::user();
        }
        return false;
    }
}

if (!function_exists('isDiver')) {
    function isDiver()
    {
        if (auth()->check() && Auth::user()->isDiver()) {
            return Auth::user();
        }
        return false;
    }
}

if (!function_exists('isGuest')) {
    function isGuest()
    {
        if (auth()->check() && Auth::user()->isGuest()) {
            return Auth::user();
        }
        return false;
    }
}