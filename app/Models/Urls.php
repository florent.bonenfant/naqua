<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Urls extends Model
{
    protected $table = 'urls';
    protected $primaryKey = 'url';

    const CREATED_AT = 'date';
    const UPDATED_AT = null;

    protected $fillable = [ ];
    protected $hidden = [ ];


    public function getByTypeAndId($type, $id)
    {
        if ($type !== URLS_TYPE_ARTICLE && $type !== URLS_TYPE_AUTEUR && $type !== URLS_TYPE_RUBRIQUE) {
            throw new \Exception(__('common.exception_type_inconnu'));
        }

        return (DB::table($this->table)
                        ->select('*')
                        ->where('type', $type)
                        ->where('id_objet', $id)
                        ->get()
                        ->first()
                );
    }

    public function getByUrl($url)
    {
        $retour = (
            DB::table($this->table)
                        ->select('*')
                        ->where('url', $url)
                        ->get()
                        ->first()
                );

        if (is_null($retour)) {
            throw new \Exception(__('common.exception_url_inconnu', ['url' => $url]));
        }
        return $retour;
    }

    /**
     * Récupération des fichiers liés à un articles via la table pivot
     *
     * @return  [collection]  liste d'entités App\Models\Articles
     */
    public function Article() {
        return $this->belongsTo(Articles::class, 'article_id', 'id_objet')->wherePivot('type', 'article');
    }
}
