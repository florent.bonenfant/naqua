<?php

namespace App\Models;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable;

    protected $table = 'users';
    protected $primaryKey = 'id';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public function isGod() {
        return ($this->role >= IS_GOD);
    }

    public function isAdmin() {
        return ($this->role >= IS_ADMIN);
    }

    public function isStaff() {
        return ($this->role >= IS_STAFF);
    }

    public function isAuthor() {
        return ($this->role >= IS_AUTHOR);
    }

    public function isDiver() {
        return ($this->role >= IS_DIVER);
    }

    public function isGuest() {
        return ($this->role >= IS_GUEST);
    }


    /**
     * Récupération des eveneemnts liés à un user via la table pivot
     *
     * @return  [collection]  liste d'entités App\Models\Evenements
     */
    public function Evenements() {
        return $this->belongsToMany(Evenements::class, 'evenements_participants', 'id_auteur', 'id_evenement')->withPivot('date', 'reponse');;
    }

    /**
     * Récupération des articles liés à un user via la table pivot
     *
     * @return  [collection]  liste d'entités App\Models\Articles
     */
    public function Articles() {
        return $this->belongsToMany(Articles::class, 'auteurs_articles', 'id_auteur', 'id_article');
    }

    /**
     * Récupération des inscriptions liés à un user via la table pivot
     *
     * @return  [collection]  liste d'entités App\Models\Evenements
     */
    public function Registrations() {
        return $this->hasMany(Registration::class, 'id_auteur');
    }

    /**
     * Récupération des articles liés à un user via la table pivot
     *
     * @return  [collection]  liste d'entités App\Models\Articles
     */
    public function Categories() {
        return $this->belongsToMany(Category::class, ' 	auteurs_rubriques', 'id_auteur', 'id_rubrique');
    }
}
