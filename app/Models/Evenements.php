<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Evenements extends Model
{
    protected $table = 'evenements';
    protected $primaryKey = 'id_evenement';

    const CREATED_AT = null;
    const UPDATED_AT = 'maj';

    protected $fillable = [ 'date_debut', 'date_fin', 'titre', 'descriptif', 'lieu', 'adresse', 'places', 'couleur', 'url'  ];
    protected $hidden = [   ];

    /**
     * Récupération des participants liés à un evenement via la table pivot
     *
     * @return  [collection]  liste d'entités App\Models\User
     */
    public function Participants() {
        return $this->belongsToMany(User::class, 'evenements_participants', 'id_evenement', 'id_auteur')->withPivot('date', 'reponse');
    }
}
