<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Registration extends Model
{
    protected $table = 'inscriptions';
    protected $primaryKey = 'id';

    const CREATED_AT = 'date_inscription';
    const UPDATED_AT = null;

    protected $fillable = [    ];

    protected $hidden = [    ];
    protected $guarded = ['_token', 'ok', 'reglementok', 'certificat'];

    /**
     * Récupération des inscriptions liés à un user via la table pivot
     *
     * @return  [collection]  liste d'entités App\Models\Evenements
     */
    public function User() {
        return $this->belongsTo(User::class, 'id_auteur');
    }
}