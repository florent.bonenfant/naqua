<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class File extends Model
{
    protected $table = 'documents';
    protected $primaryKey = 'id_document';

    const CREATED_AT = 'date';
    const UPDATED_AT = 'maj';

    protected $fillable = [ ];
    protected $hidden = [ ];

    public function DocumentOfFile() {
        return $this->belongsTo('App\Models\FileLink', 'id_document', 'id_document');
    }
    public function Filelink() {
        return $this->belongsTo('App\Models\FileLink', 'id_document', 'id_document');
    }

    /**
     * Récupération des fichiers liés à un articles via la table pivot
     *
     * @return  [collection]  liste d'entités App\Models\Articles
     */
    public function Articles() {
        return $this->belongsToMany(Articles::class, 'documents_liens', 'id_document', 'id_objet')->wherePivot('objet', 'article');
    }

    /**
     * Récupération des fichiers liés à un articles via la table pivot
     *
     * @return  [collection]  liste d'entités App\Models\Category
     */
    public function Category() {
        return $this->belongsToMany(Category::class, 'documents_liens', 'id_document', 'id_objet')->wherePivot('objet', 'rubrique');
    }

    public function getAll() {
        return (DB::table($this->table)
                        ->select('*')
                        ->orderBy('maj', 'desc')
                ->get()
//                        ->paginate(config('app.nbElements.back.global'))
                );
    }
}
