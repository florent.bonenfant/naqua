<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Category extends Model
{
    protected $table = 'rubriques';
    protected $primaryKey = 'id_rubrique';

    const CREATED_AT = 'date_tmp';
    const UPDATED_AT = 'maj';

    const ETAT_PUBLIE = 'publie';

    protected $fillable = [    ];

    protected $hidden = [    ];

    /**
     * @desc retourne la liste des factures pour un client
     * @return collection Devis
     */
    public function ArticlesOfCategory()
    {
        return $this->hasMany('App\Models\Articles', 'id_rubrique', 'id_rubrique')->latest('date_modif');
    }

    /**
     * @desc Récupère la liste des documents
     * @param int $limit
     * @return collection Produits
     */
    public function FilesOfCategory($id)
    {
        return DB::table('documents')->selectRaw('documents.*')
            ->leftjoin('documents_liens', 'documents_liens.id_document', '=', 'documents.id_document')
            ->leftjoin('rubriques', 'documents_liens.id_objet', '=', 'rubriques.id_rubrique')
            ->where('documents_liens.objet', '=', 'rubrique')
            ->where('rubriques.id_rubrique', '=', $id)
            ->orderBy('rubriques.maj', 'desc')
            ->get();
    }

    /**
     * Récupération des fichiers liés à un articles via la table pivot
     *
     * @return  [collection]  liste d'entités App\Models\File
     */
    public function Fichiers()
    {
        // Table de destination, table pivot, clé étrangère de File, clé primaire d'article       // filtre sur le type de fichier
        return $this->belongsToMany(File::class, 'documents_liens', 'id_objet', 'id_document')->wherePivot('objet', 'rubrique');
    }

    /**
     * Récupération des auteurs liés à une rubrique via la table pivot
     *
     * @return  [collection]  liste d'entités App\Models\User
     */
    public function Auteurs() {
        return $this->belongsToMany(User::class, 'auteurs_rubriques', 'id_rubrique', 'id_auteur');
    }
}
