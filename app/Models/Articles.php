<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Articles extends Model
{
    protected $table = 'articles';
    protected $primaryKey = 'id_article';

    const CREATED_AT = 'date';
    const UPDATED_AT = 'date_modif';

    const ETAT_PUBLIE = 'publie';

    protected $fillable = [
//        'id_client', 'client_nom',
    ];

    protected $hidden = [
//        'pwd', 'token'
    ];

    /**
     * Récupération des fichiers liés à un articles via la table pivot
     *
     * @return  [collection]  liste d'entités App\Models\File
     */
    public function Fichiers()
    {
        // Table de destination, table pivot, clé étrangère de File, clé primaire d'article       // filtre sur le type de fichier
        return $this->belongsToMany(File::class, 'documents_liens', 'id_objet', 'id_document')->wherePivot('objet', 'article');
    }

    /**
     * Récupération des auteurs liés à un article via la table pivot
     *
     * @return  [collection]  liste d'entités App\Models\User
     */
    public function Auteurs() {
        return $this->belongsToMany(User::class, 'auteurs_articles', 'id_article', 'id_auteur');
    }

    /**
     * Récupération des fichiers liés à un articles via la table pivot
     *
     * @return  [collection]  liste d'entités App\Models\File
     */
    public function Category()
    {
        // Table de destination, table pivot, clé étrangère de File, clé primaire d'article       // filtre sur le type de fichier
        return $this->belongsTo(Category::class, 'id_rubrique');
    }

    public function Slug() {
        return $this->hasOne(Urls::class, 'id_objet', 'id_article')->wherePivot('type', 'article');
    }
}
