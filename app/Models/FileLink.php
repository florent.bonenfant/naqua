<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class FileLink extends Model {

    protected $table = 'documents_liens';
    protected $primaryKey = 'id_document';

    const CREATED_AT = null;
    const UPDATED_AT = null;

    protected $fillable = [];
    protected $hidden = [];

    public function Filedetail() {
        return $this->hasMany('App\Models\File', 'id_document', 'id_document');
    }
}
