<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Meta extends Model
{
    protected $table            = 'meta';
    protected $primaryKey       = 'meta_id';

    protected $fillable = [
       'cle', 'type', 'valeur'
    ];

    protected $hidden = [];

    const META_TYPE_CONFIG      = 'config';

    public static function getMeta($type, $cle) {
        return (DB::table('meta')
            ->select('valeur')
            ->where('type', '=', $type)
            ->where('cle', '=', $cle)
            ->first()
            ->valeur
        );
    }
}
