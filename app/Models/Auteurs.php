<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Auteurs extends Model
{
    protected $table = 'auteurs';
    protected $primaryKey = 'id_auteur';

    const CREATED_AT = null;
    const UPDATED_AT = null;

    protected $fillable = [
//        'id_client', 'client_nom',
    ];
    
    protected $hidden = [
//        'pwd', 'token'
    ];

    public static function getWithMail() {
        return DB::table('auteurs')->selectRaw('*')
                        ->where('email', '<>', '\'\'')
                        ->get();
    }

}
