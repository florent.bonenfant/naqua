<?php
namespace App\Services\Galerie;

use Illuminate\Support\Facades\Facade;

class GalerieFacade extends Facade
{
    protected static function getFacadeAccessor()
    {
        return Galerie::class;
    }
}