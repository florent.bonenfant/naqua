<?php
namespace App\Services\Galerie;

use App\Models\File;
use App\Models\FileLink;

class Galerie
{

    private $fileLink;
    private $file;

    public function __construct()
    {
        $this->fileLink = new FileLink();
        $this->file = new File();
    }


    public static function getHtml($idObjet)
    {
        $html = '<div id="jssor_1" style="position:relative;margin:0 auto;top:0px;left:0px;width:650px;height:480px;overflow:hidden;visibility:hidden;">';
                // <!-- Loading Screen -->
            $html .= '<div data-u="loading" class="jssorl-009-spin" style="position:absolute;top:0px;left:0px;width:80%;height:100%;text-align:center;background-color:rgba(0,0,0,0.7);">
                    <img style="margin-top:-19px;position:relative;top:50%;width:38px;height:38px;" src="../svg/loading/static-svg/spin.svg" />
                </div>';

        $html .= '<ul id="screenshot" class="galerie_photos" data-u="slides"  style="cursor:default;position:relative;top:0px;left:0px;width:680px;height:380px;overflow:hidden;">';
        foreach(Galerie::getData($idObjet) as $fichier) {
            foreach ($fichier->Filedetail as $photo) {
                if (file_exists(public_path(DISK_PATH_IMG_STD . $photo->fichier))) {
                    $html .= '<li>
                        <a href="' . url(DISK_PATH_IMG_FULL . $photo->fichier) . '" data-fancybox="gallery" data-caption="' . $photo->titre . '">
                            <img id="galerie_photo_' . $photo->id_document . '" src="' . url(DISK_PATH_IMG_STD . $photo->fichier) . '" title="' . $photo->titre . '" alt="' . $photo->desc . '" data-u="image">
                        </a>
                        <img id="galerie_photo_th_' . $photo->id_document . '" src="' . url(DISK_PATH_IMG_TH . $photo->fichier) . '" title="' . $photo->titre . '" alt="' . $photo->desc . '" data-u="thumb">
                        </li>';
                }
            }
        }
        $html .= '</ul>';

        // <!-- Thumbnail Navigator -->
            $html .= '<div data-u="thumbnavigator" class="jssort101" style="position:absolute;left:0px;bottom:0px;width:650px;height:100px;background-color:#000;" data-autocenter="1" data-scale-bottom="0.75">
                <div data-u="slides">
                    <div data-u="prototype" class="p" style="width:190px;height:84px;">
                        <div data-u="thumbnailtemplate" class="t"></div>
                        <svg viewBox="0 0 16000 16000" class="cv">
                            <circle class="a" cx="8000" cy="8000" r="3238.1"></circle>
                            <line class="a" x1="6190.5" y1="8000" x2="9809.5" y2="8000"></line>
                            <line class="a" x1="8000" y1="9809.5" x2="8000" y2="6190.5"></line>
                        </svg>
                    </div>
                </div>
            </div>';
            // <!-- Arrow Navigator -->
            $html .= '<div data-u="arrowleft" class="jssora106" style="width:55px;height:55px;top:162px;left:30px;" data-scale="0.75">
                <svg viewBox="0 0 16000 16000" style="position:absolute;top:0;left:0;width:100%;height:100%;">
                    <circle class="c" cx="8000" cy="8000" r="6260.9"></circle>
                    <polyline class="a" points="7930.4,5495.7 5426.1,8000 7930.4,10504.3 "></polyline>
                    <line class="a" x1="10573.9" y1="8000" x2="5426.1" y2="8000"></line>
                </svg>
            </div>
            <div data-u="arrowright" class="jssora106" style="width:55px;height:55px;top:162px;right:30px;" data-scale="0.75">
                <svg viewBox="0 0 16000 16000" style="position:absolute;top:0;left:0;width:100%;height:100%;">
                    <circle class="c" cx="8000" cy="8000" r="6260.9"></circle>
                    <polyline class="a" points="8069.6,5495.7 10573.9,8000 8069.6,10504.3 "></polyline>
                    <line class="a" x1="5426.1" y1="8000" x2="10573.9" y2="8000"></line>
                </svg>
            </div>
        </div>';

        return $html;
    }

    public static function getData($idObjet) {
        return (new FileLink())->where('id_objet', $idObjet)->with('Filedetail')->get();
    }

}