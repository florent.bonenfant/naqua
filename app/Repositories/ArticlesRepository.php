<?php

namespace App\Repositories;

use Illuminate\Support\Facades\DB;
use App\Models\Articles;
use App\Models\File;
use App\Repositories\UrlsRepository;
use Illuminate\Http\Request;

class ArticlesRepository {

    /**
     * Get all quotation collection paginated.
     *
     * @return \Illuminate\Contracts\Pagination\LengthAwarePaginator
     */
    public function getAll() {
        return (DB::table('articles')
                        ->select('*', 'auteurs.*', 'rubriques.titre as rubrique', 'articles.titre as article')
                        ->leftJoin('auteurs_articles', 'auteurs_articles.id_article', '=', 'articles.id_article')
                        ->leftJoin('auteurs', 'auteurs.id_auteur', '=', 'auteurs_articles.id_auteur')
                        ->leftJoin('rubriques', 'rubriques.id_rubrique', '=', 'articles.id_rubrique')
//                        ->groupBy('devis.id_devis')
                        ->latest('articles.date_modif')
                ->get()
//                        ->paginate(config('app.nbElements.back.global'))
                );
    }

    /**
     * Get all articles collection.
     *
     * @param  int  $idAdmin
     * @return \Illuminate\Contracts\Pagination\LengthAwarePaginator
     */
    public function getAllByAuteur($id = null) {
        if ($id == null) {
            $id = auth()->id();
        } else {
            $id = (int) $id;
        }

          return (DB::table('articles')
                        ->select('*', 'auteurs.*', 'rubriques.titre as rubrique', 'articles.titre as article')
                        ->leftJoin('auteurs_articles', 'auteurs_articles.id_article', '=', 'articles.id_article')
                        ->leftJoin('auteurs', 'auteurs.id_auteur', '=', 'auteurs_articles.id_auteur')
                        ->leftJoin('rubriques', 'rubriques.id_rubrique', '=', 'articles.id_rubrique')
                        ->where('auteurs.id_auteur', '=', $id)
                        ->latest('articles.date_modif')
                ->get()
                );

    }

    /**
     * @desc récupére la liste des articles pour une catégorie
     * @param Category $id
     * @return collection d'articles
     */
    public function getAllByCategory($id) {
          return (DB::table('articles')
                        ->select('*', 'auteurs.*')
                        ->leftJoin('auteurs_articles', 'auteurs_articles.id_article', '=', 'articles.id_article')
                        ->leftJoin('auteurs', 'auteurs.id_auteur', '=', 'auteurs_articles.id_auteur')
//                        ->leftJoin('rubriques', 'rubriques.id_rubrique', '=', 'articles.id_rubrique')
                        ->where('articles.id_rubrique', '=', $id->id_rubrique)
                        ->latest('articles.date_modif')
                ->get()
                );

    }

    public function save(Request $request, Articles $article) {
        $article->texte = $request->get('nicarea');
        $article->texte = str_replace(array('<doc', '|left>', '|right>', '|center>'), array('[doc', '/]', '/]', '/]',), $request->get('nicarea'));
        $article->texte = preg_replace_callback(
                '`\[doc(.*)\/]`isU',
            function ($idImg) {
                $image = (new File())->find($idImg[1]);
                if ($image->mode === 'document') {
                    return '<a href="' . url(DISK_PATH_DOCS . $image->fichier) . '" title="' . $image->titre . '">' . $image->titre . '</a>';
                } elseif ($image->mode === 'image') {
                    return '<img src="' . url(DISK_PATH_IMG_STD . $image->fichier) . '" title="' . $image->titre . '" alt="' . $image->desc . '"/>';
                } else {
                    return '<a href="' . url(DISK_PATH_DOCS . $image->fichier) . '" title="' . $image->titre . '">' . $image->titre . '</a>';
                }
            },
            $article->texte
        );
        $article->id_rubrique = $request->get('rubrique');
        $article->titre = $request->get('titre');
        $article->maj = date('Y-m-d H:i:s');
        $article->date_modif = date('Y-m-d H:i:s');
        $article->date = date('Y-m-d H:i:s');
        $article->save();

        $article->Auteurs()->save(isGuest());

        $url = new UrlsRepository();
        $url->saveUrl($article->id_article, 'article', $article->titre);

        return $article;
    }

}
