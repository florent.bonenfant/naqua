<?php

namespace App\Repositories;

use Illuminate\Support\Facades\DB;
use App\Models\Urls;
use Illuminate\Support\Str;

class UrlsRepository
{

    /**
     * [Enregistre l'url formaté pour coller à l'ancienne mécanique de spip]
     *
     * @param   [int]       $id    [identifiant de l'objet]
     * @param   [string]    $type  [rubrique, article...]
     * @param   [string]    $nom   [Titre de l'objet]
     *
     * @return  [voir|int]         [return description]
     * @todo vérifier doublon avant insertion
     */
    public function saveUrl($id, $type, $nom) {
        $url = Urls::where('id_objet', $id)
            ->where('type', $type)
            ->first();
        if (is_null($url)) {
            $checkUrl = Urls::where('url', Str::slug($nom))->first();

            $url = new Urls;
            $url->id_objet = (int) $id;
            $url->type = $type;
            if (is_null($checkUrl)) {
                $url->url = Str::slug($nom);
            } else {
                $url->url = Str::slug($id . '-' . $nom);
            }
            $url->date = date('Y-m-d');
            $url->save();
        } else {
            $url->date = date('Y-m-d');
            $url->update();
        }
    }

}
