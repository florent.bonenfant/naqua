<?php

namespace App\Repositories;

use Illuminate\Support\Facades\DB;
use App\Models\Registration;

class RegistrationRepository
{

    public function __construct()
    {
        $this->model = new Registration();
    }
    /**
     * Get all quotation collection paginated.
     *
     * @return \Illuminate\Contracts\Pagination\LengthAwarePaginator
     */
    public function getAll()
    {
        $date = date('Y-m-d');
        if ((int) (substr($date, 5, 2)) < 8 && (int) (substr($date, 5, 2)) > 0) {
            $date = (int) substr($date, 0, 4) - 1;
        } else {
            $date = substr($date, 0, 4);
        }
        $date = $date . '-06-28';

        return (DB::table('inscriptions')
                        ->select('*')
                        ->where('date_inscription', '>', $date)
                        ->orderBy('id', 'desc')
                        ->get()
                );
    }

    public function getLastRegistrationUser()
    {
        if (
            $res = DB::table($this->model->getTable())
                ->select('*')
                ->where('id_auteur', isGuest()->id)
                ->orderBy($this->model->getKeyName(), 'DESC')
                ->first()
        ) {
            return $res;
        } else {
            return new Registration();
        }
    }
}
