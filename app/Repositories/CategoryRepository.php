<?php

namespace App\Repositories;

use Illuminate\Support\Facades\DB;
use App\Models\Category;
use App\Models\FileLink;
use App\Repositories\UrlsRepository;

class CategoryRepository {

    /**
     * Get all quotation collection paginated.
     *
     * @return \Illuminate\Contracts\Pagination\LengthAwarePaginator
     */
    public function getAll() {
        return (DB::table('rubriques')
                        ->select('*')
                        ->orderBy('id_parent', 'ASC')
                        ->orderBy('id_rubrique', 'ASC')
                        ->get()
//                        ->paginate(config('app.nbElements.back.global'))
                );
    }

    public function getAllByCategory($id) {
        $cat = (new Category())->with([
                    'ArticlesOfCategory',
                ])
                ->whereIdRubrique($id)
                ->firstOrFail();
        $cat->FilesOfCategory = $cat->FilesOfCategory($id);
        return $cat;
    }

    /**
     * @desc ajoute une catégorie enfant et enregistre son url
     * @param int $idParent
     * @param string $name
     * @return bool
     */
    public function addChildren($idParent, $name) {
        $children = (new Category());
        $children->id_parent = $idParent;
        $children->titre = $name;
        $children->export = 'oui';
        $children->statut = 'new';
        $children->lang = 'fr';
        $children->descriptif = '';
        $children->texte = '';

        $children->save();
        $url = new UrlsRepository();
        $url->saveUrl($children->id_rubrique, 'rubrique', $children->titre);
        return $children->save();
    }

    /**
     * @desc ajoute une catégorie parente et enregistre son url
     * @param int $idParent
     * @param string $name
     * @return bool
     */
    public function add($name) {
        $master = new Category();
        $master->id_parent = 0;
        $master->titre = $name;
        $master->export = 'oui';
        $master->statut = 'new';
        $master->lang = 'fr';
        $master->descriptif = '';
        $master->texte = '';
        $master->save();
        $master->id_secteur = $master->id_rubrique;
        $master->titre = $master->titre;

        $url = new UrlsRepository();
        $url->saveUrl($master->id_rubrique, 'rubrique', $master->titre);
        return $master->save();
    }

    /**
     * Créer la liaison du fichier donnée vers la catégorie voulu
     * @param type $idCategory
     * @param \App\Models\File $file
     */
    public function addFile($idCategory, \App\Models\File $file) {
        return DB::table((new FileLink())->getTable())->insert(
                [
                    'id_document' => (int) $file->id_document,
                    'id_objet' => (int) $idCategory,
                    'objet' => 'rubrique',
                    'vu' => 'non',
                ]
        );
    }

}
