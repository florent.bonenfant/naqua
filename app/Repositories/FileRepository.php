<?php

namespace App\Repositories;

use Illuminate\Support\Facades\DB;
use App\Models\File;
use App\Models\FileLink;
use Illuminate\Http\UploadedFile;
use Illuminate\Support\Facades\Storage;

class FileRepository {

    /**
     * Ajoute un fichier dans la base de données
     * @param \Illuminate\Http\Request $request
     * @param string $path
     * @param int $article      id article ou rubrique
     * @param string $desc
     * @param string $type      article, rubrique
     * @return boolean|File
     */
    public function addFile(UploadedFile $requestFile, $path, $article = 0, $desc = '', $type = 'article') {
        $file = new File();
        $file->extension = $requestFile->extension();
        $file->titre = $requestFile->getClientOriginalName();
        $file->taille = $requestFile->getSize();
        $file->descriptif = $desc;
        if ($file->extension === 'pdf') {
            $file->mode = 'document';
            $file->dossier = DISK_PATH_DOCS;
        } elseif ($file->extension === 'zip') {
            $file->mode = 'archive';
            $file->dossier = DISK_PATH_ARCHIVES;
        } else {
            $file->mode = 'image';
            $file->dossier = DISK_PATH_IMG_FULL;
        }
        $file->fichier = $path;
        $file->id_vignette = 0;
        $file->largeur = 0;
        $file->hauteur = 0;
        $file->distant = 'non';
        $file->date = date('Y-m-d H:i:s');
        $file->maj = date('Y-m-d H:i:s');

        if ($file->save()) {
            return $this->addLink($article, $file, $type);
        } else {
            return false;
        }
    }

    /**
     * Créer la liaison du fichier donnée vers la catégorie voulu
     * @param type $idCategory
     * @param \App\Models\File $file
     */
    protected function addLink($idCategory, File $file, $type) {
        if (DB::table((new FileLink())->getTable())->insert(
                        [
                            'id_document' => (int) $file->id_document,
                            'id_objet' => (int) $idCategory,
                            'objet' => $type,
                            'vu' => 'non',
                        ]
                )) {

            return $file;
        } else {
            return false;
        }
    }

    public function delFile($idFichier, $idObjet) {
        $link = new FileLink();
        try {
            $linkFind = $link->where('id_document', $idFichier)
                ->where('id_objet', $idObjet)
                ->firstOrFail();
        } catch (Illuminate\Database\Eloquent\ModelNotFoundException $ex) {
            return false;
        }
        if ($linkFind->delete()) {
            if ($link->where('id_document', $idFichier)->count() <= 1) {
                try {
                    $file = new File();
                    $referenceFichier = $file->where('id_document', $idFichier)->firstOrFail();
                    if ($referenceFichier->mode === 'image') {
                        Storage::disk(DISK_DRIVER_IMG_FULL)->delete($referenceFichier->fichier);
                        Storage::disk(DISK_DRIVER_IMG_STD)->delete($referenceFichier->fichier);
                        Storage::disk(DISK_DRIVER_IMG_TH)->delete($referenceFichier->fichier);
                    } elseif ($referenceFichier->mode === 'zip') {
                        Storage::disk(DISK_DRIVER_ARCHIVES)->delete($referenceFichier->fichier);
                    } else {
                        Storage::disk(DISK_DRIVER_DOCS)->delete($referenceFichier->fichier);
                    }
                    return $referenceFichier->delete();
                } catch (Illuminate\Database\Eloquent\ModelNotFoundException $ex) {
                    return false;
                }

            }
        } else {
            return false;
        }
        return true;
    }

    public function deleteFileForAll($idFichier) {
        try {
            $linkFind =  (new FileLink())->where('id_document', $idFichier)
                ->firstOrFail();
        } catch (Illuminate\Database\Eloquent\ModelNotFoundException $ex) {
            return false;
        }

        if ($this->delFile($linkFind->id_document, $linkFind->id_objet)) {
            return $linkFind->delete();
        }
        return false;
    }

}
