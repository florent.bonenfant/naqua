<?php

namespace App\Repositories;

use Illuminate\Support\Facades\DB;
use App\Models\Contact;
use Illuminate\Http\Request;

class ContactRepository {

    public function checkIfMailActive($contact) {
        if(
            DB::table((new Contact())->getTable())
                ->where('email', $contact->email)
                ->where('valid', 1)
                ->first()
        ) {
            return true;
        } else {
            return false;
        }
    }

}
