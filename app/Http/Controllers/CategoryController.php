<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Repositories\CategoryRepository;
use App\Models\Category;
use App\Models\File;
use App\Models\FileLink;
use App\Http\Requests\CategoryChildrenRequest;
//use App\Repositories\ArticlesRepository;
use Illuminate\Support\Facades\Storage;
use Intervention\Image\Facades\Image as InterventionImage;
use Gate;

class CategoryController extends Controller {

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index() {
        return view('back.category.index', ['category' => (new CategoryRepository())->getAll()]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create() {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request) {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id) {
        $categorie = (new CategoryRepository())->getAllByCategory($id);

        if (
            ($categorie->statut === Category::ETAT_PUBLIE) ||
            Gate::allows('gate-author-of-category', $categorie)
        ) {
            return view('front.category.show', ['category' => $categorie]);
        } else {
            return redirect()->route('accueil.index')->with('messageError', __('common.autorisation_page_front'));
        }

        // return view('front.category.show', [
        //     'category' => $id,
        //     'articles' => (new ArticlesRepository())->getAllByCategory($id)
        // ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id) {
        //
    }

    private function img($id) {
        dd($id);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id) {
        $cat = (new Category())->find($id);
        $cat->texte = $request->get('nicarea');
        $cat->texte = str_replace(array('<doc', '|left>', '|right>', '|center>'), array('[doc', '/]', '/]', '/]',), $request->get('nicarea'));
        $cat->texte = preg_replace_callback(
                '`\[doc(.*)\/]`isU', function ($idImg) {
            $image = (new File())->find($idImg[1]);
            if ($image->mode === 'document') {
                return '<a href="' . url(DISK_PATH_DOCS . $image->fichier) . '" title="' . $image->titre . '">' . $image->titre . '</a>';
            } elseif ($image->mode === 'image') {
                return '<img src="' . url(DISK_PATH_IMG_STD . $image->fichier) . '" title="' . $image->titre . '" alt="' . $image->desc . '"/>';
            } else {
                return '<a href="' . url(DISK_PATH_DOCS . $image->fichier) . '" title="' . $image->titre . '">' . $image->titre . '</a>';
            }

        }, $cat->texte
        );
        $cat->save();
        return redirect()->route('category.show', $id)->with('messageSuccess', __('Modification enregistré'));
    }

    /**
     * Ajoute une catégorie enfant
     *
     * @param  \Illuminate\Http\Requests\CategoryChildrenRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function addChildren(CategoryChildrenRequest $request) {
        foreach ($request->input() as $input => $value) {
            if ($input !== '_token') {
                $child = $value;
                $idParent = substr($input, 9);
                break;
            }
        }
        if ((new CategoryRepository())->addChildren($idParent, $child) !== false) {
            return redirect()->route('category.show', $idParent)->with('messageSuccess', __('Ajout de la catégorie enregistré'));
        } else {
            return redirect()->route('category.show', $idParent)->with('messageError', __('Aucune modification apporté'));
        }
    }

    /**
     * Ajoute un fichier à une rubrique
     *
     * @param  \Illuminate\Http\Requests  $request
     * @return \Illuminate\Http\Response
     */
    public function addFile(Request $request) {

        foreach ($request->file() as $input => $requestFile) {
            $idCategory = trim(strstr($input, '_'), '_');

            $diskStorage = DISK_DRIVER_DOCS;
            // Création des miniatures
            if (exif_imagetype($requestFile->getPathname()) !== false) {
                $diskStorage = DISK_DRIVER_IMG_FULL;

                // Save image
                $path = Storage::disk($diskStorage)->putFile('', $requestFile);

                $image = InterventionImage::make($requestFile)->widen(500);
                Storage::disk(DISK_DRIVER_IMG_STD)->put($path, $image->encode());
                $image = InterventionImage::make($requestFile)->widen(250);
                Storage::disk(DISK_DRIVER_IMG_TH)->put($path, $image->encode());
            } else {
                $path = Storage::disk($diskStorage)->putFile('', $requestFile);
            }
            if (Storage::disk($diskStorage)->exists($path)) {
                // Save in base
                return json_encode(
                        (new \App\Repositories\FileRepository())->addFile($requestFile, $path, $idCategory)
                );
            } else {
                return json_encode(false);
            }
        }
        return json_encode(false);
    }

    /**
     * Ajoute un fichier à une rubrique
     *
     * @param  \Illuminate\Http\Requests  $request
     * @return \Illuminate\Http\Response
     */
    public function delFile(Request $request, $idCategorie) {
        return json_encode(
                (new \App\Repositories\FileRepository())->delFile($request->input()['id'], $idCategorie)
        );
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id) {
        //
    }

}
