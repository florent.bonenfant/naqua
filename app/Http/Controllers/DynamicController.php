<?php
namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Notifications\Notifiable;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;
use App\Models\Articles;
use App\Models\Urls;
use App\Repositories\ArticlesRepository;
use App\Repositories\CategoryRepository;
use App\Services\Galerie\Galerie;

class DynamicController extends Controller
{
    public function manage($slugUrl)
    {
        try {
            $page = getUrlByUrl($slugUrl);

            switch ($page->type) {
                case URLS_TYPE_ARTICLE:
                    return app()->make(ArticleController::class)->callAction('show', [$page->id_objet]);
            //    return view('front.article.show', [URLS_TYPE_ARTICLE => (new Articles())->find($page->id_objet)]);
                break;
                case URLS_TYPE_RUBRIQUE:
                    return app()->make(CategoryController::class)->callAction('show', [$page->id_objet]);
                break;
            }
        } catch (\Exception $e) {
            var_dump($e->getMessage());
            // die;
        }
    }
}
