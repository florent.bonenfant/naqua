<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Models\Articles;
use App\Models\Evenements;
use Illuminate\Support\Carbon;

class AccueilController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $evenements = Evenements::whereDate('date_debut',
        '>',
       Carbon::now()
       ->subMonth())
       ->orderBy('date_debut', 'asc')
       ->get();

        return view('front.accueil.index', [
            'edito' => (new Articles())->find(3),
            'evenements' => $evenements,
        ]);
    }

}
