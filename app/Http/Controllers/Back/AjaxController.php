<?php

namespace App\Http\Controllers\Back;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class AjaxController extends Controller
{
    /**
     * Ajoute un fichier à une rubrique
     *
     * @param  \Illuminate\Http\Requests  $request
     * @return \Illuminate\Http\Response
     */
    public function delFile(Request $request, $id) {
        return json_encode(
                (new \App\Repositories\FileRepository())->delFile($request->input()['id'], $id)
        );
    }
}