<?php

namespace App\Http\Controllers\Back;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\File;
use Gate;
use Illuminate\Support\Facades\DB;
use App\Models\FileLink;
class FileController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('back.file.index', ['documents' => (new File())->getAll()]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($idFile)
    {
        // return response()->json(DB::table((new FileLink())->getTable())->where('id_document', $idFile)->get());
        // dd((new FileLink())->getTable()->where('id_document', $idFile)->get());
        // foreach(DB::table((new FileLink())->getTable())->where('id_document', $idFichier)->get() as $link) {
        if (Gate::allows('gate-admin') || Gate::allows('gate-author')) {
            return json_encode(
                    (new \App\Repositories\FileRepository())->deleteFileForAll($idFile)
            );
        } else {
            return response()->json('forbiden');
        }
    }
}
