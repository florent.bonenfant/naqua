<?php

namespace App\Http\Controllers\Back;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests\EventRequest;
use App\Models\Evenements;
use Illuminate\Support\Carbon;
use Gate;

class EventController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $evenements = Evenements::whereDate('date_debut',
            '>',
            Carbon::now()
            ->subYear())
            ->orderBy('date_debut', 'desc')
            ->get();
        return view('back.event.index', ['evenements' => $evenements]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        if (Gate::allows('gate-staff')) {
            return view('back.event.create', ['event' => null]);
        } else {
            return redirect()->route('admin.back.home')->with('messageError', __('common.autorisation_page'));
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(EventRequest $request, Evenements $event)
    {
        $request->merge([
            'date_debut' => Carbon::createFromFormat('d/m/Y H:i', $request->input('date_debut'))->format('Y-m-d H:i:s'),
            'date_fin' => Carbon::createFromFormat('d/m/Y H:i', $request->input('date_fin'))->format('Y-m-d H:i:s'),
            ]);
        $event->create($request->all());
        return redirect()->route('admin.evenement.index')->with('ok', __ ('common.message_enregistrement'));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Evenements $evenement)
    {
        if (Gate::allows('gate-staff')) {
            return view('back.event.edit', ['event' => $evenement]);
        } else {
            return redirect()->route('admin.back.home')->with('messageError', __('common.autorisation_page'));
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(EventRequest $request, Evenements $evenement)
    {
        $request->merge([
            'date_debut' => Carbon::createFromFormat('d/m/Y H:i', $request->input('date_debut'))->format('Y-m-d H:i:s'),
            'date_fin' => Carbon::createFromFormat('d/m/Y H:i', $request->input('date_fin'))->format('Y-m-d H:i:s'),
            ]);
        $evenement->update($request->all());
        return redirect()->route('admin.evenement.index')->with('ok', __ ('common.message_enregistrement'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        if (Gate::allows('gate-staff')) {
            return response()->json(Evenements::destroy($id));
        } else {
            return response()->json('forbiden');
        }
    }
}
