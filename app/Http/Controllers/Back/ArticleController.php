<?php

namespace App\Http\Controllers\Back;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Articles;
use App\Repositories\ArticlesRepository;
use App\Models\File;
use Illuminate\Support\Facades\Storage;
use Intervention\Image\Facades\Image as InterventionImage;
use Gate;

class ArticleController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('back.article.index', ['articles' => (new ArticlesRepository())->getAll()]);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function my()
    {
        return view('back.article.index', ['articles' => (new ArticlesRepository())->getAllByAuteur(isGuest()->id)]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        if (Gate::allows('gate-staff')) {
            return view('back.article.create', ['article' => null]);
        } else {
            return redirect()->route('admin.back.home')->with('messageError', __('common.autorisation_page'));
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, Articles $article)
    {
        if (Gate::allows('gate-staff')) {
            $articleRepo = new ArticlesRepository();
            $article = $articleRepo->save($request, $article);
            return redirect()->route('admin.article.edit', $article)->with('messageSuccess', __('common.message_maj'));
        } else {
            return redirect()->route('admin.back.home')->with('messageError', __('common.autorisation_page'));
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Articles $article)
    {
        if (Gate::allows('gate-staff')) {
            return view('back.article.edit', [
                'article' => $article
            ]);
        } else {
            return redirect()->route('admin.back.home')->with('messageError', __('common.autorisation_page'));
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Articles $article)
    {
        //@todo parent
        if (Gate::allows('gate-staff')) {
            $article->texte = $request->get('nicarea');
            $article->texte = str_replace(array('<doc', '|left>', '|right>', '|center>'), array('[doc', '/]', '/]', '/]',), $request->get('nicarea'));
            $article->texte = preg_replace_callback(
                    '`\[doc(.*)\/]`isU', function ($idImg) {
                $image = (new File())->find($idImg[1]);
                if ($image->mode === 'document') {
                    return '<a href="' . url(DISK_PATH_DOCS . $image->fichier) . '" title="' . $image->titre . '">' . $image->titre . '</a>';
                } elseif ($image->mode === 'image') {
                    return '<img src="' . url(DISK_PATH_IMG_STD . $image->fichier) . '" title="' . $image->titre . '" alt="' . $image->desc . '"/>';
                } else {
                    return '<a href="' . url(DISK_PATH_DOCS . $image->fichier) . '" title="' . $image->titre . '">' . $image->titre . '</a>';
                }

            }, $article->texte
            );
            $article->id_rubrique = $request->get('rubrique');
            $article->titre = $request->get('titre');
            $article->maj = date('Y-m-d H:i:s');
            $article->save();
            return redirect()->route('article.edit', $article->id_article)->with('messageSuccess', __('common.message_maj'));
        } else {
            return redirect()->route('admin.back.home')->with('messageError', __('common.autorisation_page'));
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($article)
    {
        if (Gate::allows('gate-admin')) {
            return response()->json(Articles::destroy($article));
        } else {
            return response()->json('forbiden');
        }
    }

    /**
     * Ajoute un fichier à une rubrique
     *
     * @param  \Illuminate\Http\Requests  $request
     * @return \Illuminate\Http\Response
     * @todo virer les @ nécessaire sur wsl
     */
    public function addFile(Request $request) {
        if (Gate::allows('gate-staff')) {
            foreach ($request->file() as $input => $requestFile) {
                $idArticle = trim(strstr($input, '_'), '_');

                $diskStorage = DISK_DRIVER_DOCS;
                // Création des miniatures
                if (exif_imagetype($requestFile->getPathname()) !== false) {
                    $diskStorage = DISK_DRIVER_IMG_FULL;

                    // Save image
                    $path = @Storage::disk($diskStorage)->putFile('', $requestFile);

                    $image = InterventionImage::make($requestFile)->widen(IMG_MAX_SIZE_STD);
                    @Storage::disk(DISK_DRIVER_IMG_STD)->put($path, $image->encode());
                    $image = InterventionImage::make($requestFile)->widen(IMG_MAX_SIZE_TH);
                    @Storage::disk(DISK_DRIVER_IMG_TH)->put($path, $image->encode());
                } else {
                    $path = @Storage::disk($diskStorage)->putFile('', $requestFile);
                }
                if (@Storage::disk($diskStorage)->exists($path)) {
                    // Save in base
                    return json_encode(
                            (new \App\Repositories\FileRepository())->addFile($requestFile, $path, $idArticle, 'article')
                    );
                } else {
                    return json_encode(false);
                }
            }
            return json_encode(false);
        } else {
            return response()->json('forbiden');
        }
    }
}
