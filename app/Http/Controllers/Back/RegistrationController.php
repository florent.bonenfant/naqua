<?php

namespace App\Http\Controllers\Back;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Repositories\RegistrationRepository;
use App\Models\Registration;

class RegistrationController extends Controller {

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index() {
        return view('back.registration.index', ['registration' => (new RegistrationRepository())->getAll()]);
    }

    public function export(\Symfony\Component\HttpFoundation\Request $requete) {
        $content = "id;Civilite;Nom;Prénom;Date Naissance;Ville Naissance;Dept Naissance;Nationalité;Profession;Adresse;Code postal;Ville;Tel Dom;Tel Buro;Tel Mobile;Fax;email;Nb plongée;Date Der Brevet;Niv Plongeur;Niv Prepa;Niv Nitrox;Prepa Nitrox;Niv encadr;Prepa encadre;Nom en cas d'accident;Tel accident;Lien accident;Adresse accident;Allergie aspirine;Service Club;Remarques;Intérêt Nage libre;Intérêt Apnée;Intérêt Palmes;Intérêt Perfect;Intérêt Secourisme;Reg intérieur;1er cotise;Ancienneté;Date d'inscription;Chèque;Date certificat\r\n";
        foreach($requete->all() as $id => $valide) {
            if ('id_' == substr($id, 0, 3)) {
                $inscrit = Registration::find(substr($id, 3))->toArray();
                $inscrit['adresse'] = str_replace("\r\n", ', ', $inscrit['adresse']);
                $inscrit['adresseaccident'] = str_replace("\r\n", ', ', $inscrit['adresseaccident']);
                $inscrit['serviceclub'] = str_replace("\r\n", ', ', $inscrit['serviceclub']);
                $inscrit['remarques'] = str_replace("\r\n", ', ', $inscrit['remarques']);
                unset($inscrit['id_auteur']);
                unset($inscrit['certificats']);
                $content .= implode(';', $inscrit) . "\r\n";
            } else {
                continue;
            }
        }
        return response()->downloadCsv($content, "inscriptions-" . date('d.m.Y') . ".csv");
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create() {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request) {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id) {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id) {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id) {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id) {
        //
    }

}
