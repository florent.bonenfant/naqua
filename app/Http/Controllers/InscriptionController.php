<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Controllers\Controller;
use App\Http\Requests\InscriptionRequest;
use Illuminate\Support\Facades\Storage;
use App\Models\Category;
use App\Models\File;
use App\Models\FileLink;
use App\Models\Registration;
use App\Repositories\RegistrationRepository;
use Illuminate\Support\Carbon;
use Auth;

class InscriptionController extends Controller
{
    /**
     * Ajoute un inscription sur le site
     *
     * @param  \Illuminate\Http\Requests  $request
     * @param  \Illuminate\Models\Registration  $repository
     */
    public function store(InscriptionRequest $request, Registration $repository)
    {
        $path = null;
        foreach ($request->file() as $input => $requestFile) {
            if (env('APP_DEBUG')) {
                $path = @Storage::disk(DISK_DRIVER_CERTIFICATS)->putFile(date('Y'), $requestFile);
            } else {
                $path = Storage::disk(DISK_DRIVER_CERTIFICATS)->putFile(date('Y'), $requestFile);
            }
        }

        if (!$request->input('rgpdaccident')) {
            $request->merge([
                'nomaccident' => null,
                'telaccident' => null,
                'lienaccident' => null,
                'adresseaccident' => null,
            ]);
        }
        $request->merge([
            'id_auteur' => isGuest()->id,
            'certificats' => $path,
            'datenaissance' => Carbon::createFromFormat('d/m/Y', $request->input('datenaissance'))->format('Y-m-d'),
            'dernierbrevet' => Carbon::createFromFormat('d/m/Y', $request->input('dernierbrevet'))->format('Y-m-d'),
            ]);
        $repository->create($request->all());
        return redirect()->route('accueil.index')->with('ok', __ ('L\'inscription a bien été enregistrée'));
    }

    /**
     * Affichage du formulaire pour l'inscription d'un utilisateur identifié
     */
    public function create()
    {
        if (isGuest()) {
            $inscription = (new RegistrationRepository())->getLastRegistrationUser();
            return view('front.forms.inscript', ['lastRegistration' => $inscription]);
        } else {
            return redirect('/login');
        }
    }

    private function conversionDate($date)
    {
    }
}
