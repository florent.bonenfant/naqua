<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests\ContactRequest;
use App\Mail\ContactCheckMail;
use App\Mail\ContactEnvoi;
use Illuminate\Support\Facades\Mail;
use App\Models\Contact;
use App\Models\Meta;
use App\Repositories\ContactRepository;
use Illuminate\Support\Str;

class ContactController extends Controller
{

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index() {
        $this->create();
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create() {
        return view('front.contact.index');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\ContactRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function store(ContactRequest $request, Contact $contact) {
        $contact->nom = $request->input('nom');
        $contact->prenom = $request->input('prenom');
        $contact->email = $request->input('email');
        $contact->telephone = $request->input('telephone');
        $contact->message = $request->input('question');
        $contact->token = Str::random(30);
        $contact->save();

        if ((new ContactRepository())->checkIfMailActive($contact)) {
            // Envoi du mail au staff
            $this->send($contact);
            return view('front.contact.check', ['contact' => $contact, 'dejaEnvoye' => 0]);
            // return view('front.contact.index')->with('messageSuccess', __('contact.message_mail_envoye'));

        } else {
            // Envoi d'un mail de confirmation à l'utilisateur
            Mail::to($contact->email)
                ->send(new ContactCheckMail($contact));
                return redirect()->route('accueil.index')->with('messageSuccess', __('contact.message_verification_mail'));
        }
    }

    /**
     * On envoi le message de l'utilisateur si le token correspond au message
     *
     * @param     Contact    $contact    [$contact description]
     * @param     [string]     $token      [$token description]
     *
     * @return    [void]                 [return description]
     */
    public function validation(Contact $contact, $token) {
        if ($contact->token === $token) {
            if ($this->send($contact)) {
                // envoi en cours
                return view('front.contact.check', ['contact' => $contact, 'dejaEnvoye' => 0]);
            } else {
                // déja envoyé
                return view('front.contact.check', ['contact' => $contact, 'dejaEnvoye' => 1]);
            }
        } else {
            return redirect()->route('accueil.index')->with('messageError', __('contact.autorisation_page'));
        }
    }


    /**
     * Envoi le mail au staff si ça n'a pas déjà été fait et passe le message à validé
     *
     * @param     Contact    $contact    [$contact description]
     *
     * @return    [bool]                 [return description]
     */
    public function send(Contact $contact) {
        if (!$contact->valid) {
            $contact->valid = 1;
            $contact->save();
            Mail::to(Meta::getMeta(Meta::META_TYPE_CONFIG, 'mail_contact'))
                ->send(new ContactEnvoi($contact));
            return true;
        }
        return false;
    }
}
