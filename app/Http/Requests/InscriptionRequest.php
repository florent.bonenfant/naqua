<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class InscriptionRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        if (isGuest()) {
            return true;
        }
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return $rules = [
            // coordonnees
            'civilite' => 'string|between:0,4',
            'nom' => 'required|string|between:2,200',
            'prenom' => 'required|string|between:2,200',
            'datenaissance' => 'string|between:0,10',
            'villenaiss' => 'required|string|between:0,200',
            'deptnaiss' => 'required|between:0,10',
            'nationalite' => 'required|string|between:0,100',
            'profession' => 'string|between:0,200',
            'adresse' => 'required|string',
            'codepostal' => 'required|integer|between:0,99999',
            'ville' => 'required|string|between:0,200',
            'teldom' => 'between:0,20',
            'telburo' => 'between:0,20',
            'telmobile' => 'required|between:0,20',
            'fax' => 'between:0,20',
            'email' => 'required|email|between:0,200',
            // plongees
            'nbdive' => 'required|integer|between:0,1000000',
            'ville' => 'required|string|between:0,200',
            'dernierbrevet' => 'string|between:0,10',
            //----
            'nivdiver' => 'required|string|between:0,8',
            'nivprepa' => 'string|between:0,11',
            'nivnitrox' => 'string|between:0,8',
            'prepanitrox' => 'string|between:0,8',
            'nivencadr' => 'string|between:0,2',
            'prepaencadr' => 'string|between:0,2',
            //accident
            // 'nomaccident' => 'required|string|between:0,200',
            // 'telaccident' => 'required|between:0,20',
            // 'lienaccident' => 'required|string|between:0,200',
            // 'adresseaccident' => 'required|string',
            'rgpdaccident' => 'required|integer|between:0,1',
            'allergieaspirine' => 'required|integer|between:0,1',
            //autres infos
            'anneepi' => 'integer|nullable',
            'anneescotisation' => 'string',
            'serviceclub' => 'string|nullable',
            'remarques' => 'string|nullable',
            //cours vendredi
            'interetnagelibre' => 'integer|between:0,1',
            'interetapnee' => 'integer|between:0,1',
            'interetpalmes' => 'integer|between:0,1',
            'interetperfect' => 'integer|between:0,1',
            'interetsecourisme' => 'integer|between:0,1',
            // certificat
            'certificat' => 'file',
            'reglementok' => 'bail|required',
        ];
    }
}
