<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class EventRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        if (isStaff()) {
            return true;
        }
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return $rules = [
            'titre' => 'required|string|between:3,255',
            'description' => 'string|nullable',
            'date_debut' => 'required|string|between:10,19',
            'date_fin' => 'required|string|between:10,19',
            'lieu' => 'required|string|between:2,255',
            'adresse' => 'string|nullable',
            'places' => 'integer|between:0,999|nullable',
            'couleur' => 'string|between:0,50|nullable',
            'url' => 'url|between:0,255|nullable',
        ];
    }
}
