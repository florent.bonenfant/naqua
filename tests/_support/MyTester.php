<?php


/**
 * Inherited Methods
 * @method void wantToTest($text)
 * @method void wantTo($text)
 * @method void execute($callable)
 * @method void expectTo($prediction)
 * @method void expect($prediction)
 * @method void amGoingTo($argumentation)
 * @method void am($role)
 * @method void lookForwardTo($achieveValue)
 * @method void comment($description)
 * @method \Codeception\Lib\Friend haveFriend($name, $actorClass = NULL)
 *
 * @SuppressWarnings(PHPMD)
*/
class MyTester extends \Codeception\Actor
{
    use _generated\MyTesterActions;

   /**
    * Define custom actions here
    */
    
   /**
     * @Given Un premier test
     */
    public function unPremierTest()
    {
        // throw new \Codeception\Exception\Incomplete("Step `Un premier test` is not defined");
    }

   /**
    * @Given Je suis sur la page :arg1
    */
    public function jeSuisSurLaPage($arg1)
    {
        // throw new \Codeception\Exception\Incomplete("Step `Je suis sur la page :arg1` is not defined");
    }
}
