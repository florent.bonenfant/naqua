<?php

/*
 *  Liste les constantes de l'application
 *  echo config('constants.your-returned-const');
 *
 *  @author  Florent Bonenfant <social1@niweb.eu>
 *  @version 1.0
 */

 // Dossiers
const DISK_PATH_PUBLIC              = 'app/public';
const DISK_PATH_UI                  = 'ui' . DIRECTORY_SEPARATOR;
const DISK_PATH_UPLOAD              = 'upload' . DIRECTORY_SEPARATOR;
const DISK_PATH_DOCS                = 'documents' . DIRECTORY_SEPARATOR;
const DISK_PATH_ARCHIVES            = 'archives' . DIRECTORY_SEPARATOR;
const DISK_PATH_IMG                 = 'images' . DIRECTORY_SEPARATOR;
const DISK_PATH_CERTIFICATS         = 'certificats' . DIRECTORY_SEPARATOR;
const DISK_PATH_AVATARS             = DISK_PATH_IMG . 'users' . DIRECTORY_SEPARATOR;
const DISK_PATH_IMG_FULL            = DISK_PATH_IMG . 'full' . DIRECTORY_SEPARATOR;
const DISK_PATH_IMG_STD             = DISK_PATH_IMG . 'std' . DIRECTORY_SEPARATOR;
const DISK_PATH_IMG_TH              = DISK_PATH_IMG . 'th' . DIRECTORY_SEPARATOR;

const DISK_DRIVER_PUBLIC              = 'public';
const DISK_DRIVER_UPLOAD              = 'upload';
const DISK_DRIVER_DOCS                = 'documents';
const DISK_DRIVER_ARCHIVES            = 'archives';
const DISK_DRIVER_CERTIFICATS         = 'certificats';
const DISK_DRIVER_IMG                 = 'images';
const DISK_DRIVER_IMG_FULL            = 'img_full';
const DISK_DRIVER_IMG_STD             = 'img_std';
const DISK_DRIVER_IMG_TH              = 'img_thumbs';

// Models
const URLS_TYPE_ARTICLE             = 'article';
const URLS_TYPE_AUTEUR              = 'auteur';
const URLS_TYPE_RUBRIQUE            = 'rubrique';


// ACL
const IS_GOD                        = 15;
const IS_ADMIN                      = 10;
const IS_STAFF                      = 8;
const IS_AUTHOR                     = 6;
const IS_DIVER                      = 2;
const IS_GUEST                      = 1;

// Galerie
const GALERIE_IMAGE_LARGEUR         = 650;

// Dimension par défaut des images
const IMG_MAX_SIZE_STD              = 500;
const IMG_MAX_SIZE_TH               = 250;

return [
    'your-returned-const' => 'Your returned constant value!'
];
