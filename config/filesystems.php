<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Default Filesystem Disk
    |--------------------------------------------------------------------------
    |
    | Here you may specify the default filesystem disk that should be used
    | by the framework. The "local" disk, as well as a variety of cloud
    | based disks are available to your application. Just store away!
    |
    */

    'default' => env('FILESYSTEM_DRIVER', 'local'),

    /*
    |--------------------------------------------------------------------------
    | Default Cloud Filesystem Disk
    |--------------------------------------------------------------------------
    |
    | Many applications store files both locally and in the cloud. For this
    | reason, you may specify a default "cloud" driver here. This driver
    | will be bound as the Cloud disk implementation in the container.
    |
    */

    'cloud' => env('FILESYSTEM_CLOUD', 's3'),

    /*
    |--------------------------------------------------------------------------
    | Filesystem Disks
    |--------------------------------------------------------------------------
    |
    | Here you may configure as many filesystem "disks" as you wish, and you
    | may even configure multiple disks of the same driver. Defaults have
    | been setup for each driver as an example of the required options.
    |
    | Supported Drivers: "local", "ftp", "sftp", "s3", "rackspace"
    |
    */

    'disks' => [

        'local' => [
            'driver' => 'local',
            'root' => storage_path('app'),
        ],

        DISK_DRIVER_PUBLIC => [
            'driver' => 'local',
            'root' => storage_path(DISK_PATH_PUBLIC),
            'url' => env('APP_URL').'/storage',
            'visibility' => 'public',
        ],

        's3' => [
            'driver' => 's3',
            'key' => env('AWS_ACCESS_KEY_ID'),
            'secret' => env('AWS_SECRET_ACCESS_KEY'),
            'region' => env('AWS_DEFAULT_REGION'),
            'bucket' => env('AWS_BUCKET'),
            'url' => env('AWS_URL'),
        ],

        DISK_DRIVER_ARCHIVES => [
            'driver' => 'local',
            'root' => public_path(DISK_PATH_ARCHIVES),
            'visibility' => 'public',
        ],
        DISK_DRIVER_UPLOAD => [
            'driver' => 'local',
            'root' => public_path(DISK_PATH_UPLOAD),
            'visibility' => 'private',
        ],
        DISK_DRIVER_DOCS => [
            'driver' => 'local',
            'root' => public_path(DISK_PATH_DOCS),
            'visibility' => 'public',
        ],
        DISK_DRIVER_CERTIFICATS => [
            'driver' => 'local',
            'root' => public_path(DISK_PATH_CERTIFICATS),
            'visibility' => 'public',
        ],

        DISK_DRIVER_IMG => [
            'driver' => 'local',
            'root' => public_path(DISK_PATH_IMG),
            'visibility' => 'public',
        ],
        DISK_DRIVER_IMG_FULL => [
            'driver' => 'local',
            'root' => public_path(DISK_PATH_IMG_FULL),
            'visibility' => 'public',
        ],
        DISK_DRIVER_IMG_STD => [
            'driver' => 'local',
            'root' => public_path(DISK_PATH_IMG_STD),
            'visibility' => 'public',
        ],
        DISK_DRIVER_IMG_TH => [
            'driver' => 'local',
            'root' => public_path(DISK_PATH_IMG_TH),
            'visibility' => 'public',
        ],

    ],

];
