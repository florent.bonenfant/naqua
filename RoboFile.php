<?php

use Symfony\Component\Finder\Finder;
use Robo\Result;
use Robo\Collection\CollectionBuilder;
use Robo\Task\Composer;
use Symfony\Component\Yaml\Yaml;

require "vendor/autoload.php";

class RoboFile extends \Globalis\Robo\Tasks {


    // config ok (voir <##APP_DEBUG##> <##DB_URL##>)
    // create:files ok
    // compile:css ok (voir pour bootstrap)
    // min:css ok
    // min:js ok (sur dossiers scripts)
    const PATH_INSTALL          = __DIR__;

    private $pathInstall        = self::PATH_INSTALL;
    private $pathConfigRobo     = self::PATH_INSTALL . '/.config';
    private $buildDirectory     = self::PATH_INSTALL . '/.config/build';
    private $pathPublic         = self::PATH_INSTALL . '/public';
    private $pathToCompil       = self::PATH_INSTALL . '/resources/assets';

    private $configVariables;
// sources => resources/assets /js /sass
    public function install() {
        $this->io()->title('Configuration du projet');
        // $this->config();
        $this->io()->section('Création des dossiers');
        if ($this->createFiles()->wasSuccessful()) {
            $this->io()->note('Création des fichiers réussi');
        } else {
            $this->io()->error('Echec lors de la création des fichiers');
        }
        // $this->io()->section('Installation de bootstrap');
        // if ($this->installBootstrap()->wasSuccessful()) {
        //     $this->io()->note('Installation de bootstrap OK');
        // } else {
        //     $this->io()->error('Echec de l\'installation de bootstrap');
        // }
        $this->io()->section('Compilation de bootstrap');
        if ($this->compileCss()) {
            $this->io()->note('Compilation css términé');
        } else {
            $this->io()->error('Echec de la compilation css');
        }
        if ($this->minCss()) {
            $this->io()->note('Minification css términé');
        } else {
            $this->io()->error('Echec de la minification css');
        }
        if ($this->minJs()) {
            $this->io()->note('Minification js términé');
        } else {
            $this->io()->error('Echec de la minification js');
        }


        $this->migrateUp();
        $this->key();
    }


    /**
     * Compilation et minification css
     *
     * @return    [type]    [return description]
     */
    public function compileCss() {
        $this->compilationCss();
        return $this->minCss();
    }

    /**
     * Execution des minifications
     *
     * @return    [type]    [return description]
     */
    public function minJs() {
        // front
        $this->minificationJs('plugins/fullcalendar.js');
        $this->minificationJs('plugins/jquery.fancybox.js');
        $this->minificationJs('plugins/jssor.slider.js');
        $this->minificationJs('plugins/moment.js');
        $this->minificationJs('plugins/daterangepicker.js');
        $this->minificationJs('plugins/jquery.inputmask.js');

        // back
        $this->minificationJs('plugins/adminlte.js');
        $this->minificationJs('plugins/Chart.js');
        $this->minificationJs('plugins/fastclick.js');
        $this->minificationJs('plugins/jquery.slimscroll.js');
        $this->minificationJs('plugins/jquery.sparkline.js');
        $this->minificationJs('plugins/nicEdit.js');
        $this->minificationJs('plugins/sweetalert2.all.js');
        $this->minificationJs('plugins/dataTables.bootstrap.js');
        $this->minificationJs('plugins/jquery.dataTables.js');
        $this->minificationJs('plugins/bootstrap-colorpicker.js');
        $this->minificationJs('bootstrap.js');
        $this->minificationJs('2.1.4.jQuery.js');
        $this->minificationJs('plugins/jquery-ui.js');
        return $this->minificationJs()->wasSuccessful();
    }

    /**
     * Execution des minifications
     *
     * @return    [type]    [return description]
     */
    public function minCss() {
        // front
        $this->minificationCss('js/fullcalendar.css');
        $this->minificationCss('js/fullcalendar.print.css');
        $this->minificationCss('js/jquery.fancybox.css');

        // back
        $this->minificationCss('js/_all-skins.css');
        $this->minificationCss('js/AdminLTE.css');
        $this->minificationCss('js/sweetalert2.css');
        $this->minificationCss('js/bootstrap3.css');
        $this->minificationCss('js/dataTables.bootstrap.css');
        $this->minificationCss('js/jquery.dataTables.css');
        $this->minificationCss('js/bootstrap-colorpicker.css');
        $this->minificationCss('js/daterangepicker.css');

        return $this->minificationCss()->wasSuccessful();
    }

    /**
     * Création et copie des différents fichiers
     *
     * @return Robo\Result
     */
    public function createFiles() {
        $collection = $this->collectionBuilder();
        $collection->taskFilesystemStack()
                ->mkdir($this->pathToCompil)
                ->mkdir($this->pathToCompil . '/fonts')
                ->mkdir($this->pathToCompil . '/images')
                ->mkdir($this->pathToCompil . '/scripts')
                ->mkdir($this->pathToCompil . '/styles')
                ->mkdir($this->pathPublic)
                ->mkdir($this->pathPublic . '/css')
                ->mkdir($this->pathPublic . '/scripts')
                ->mkdir($this->pathPublic . '/fonts')
                ->mkdir($this->pathPublic . '/js')

                ->mkdir($this->pathInstall . '/storage')
                ->mkdir($this->pathInstall . '/storage/framework')
                ->mkdir($this->pathInstall . '/storage/framework/cache', 0765)
                ->mkdir($this->pathInstall . '/storage/framework/views', 0765)
                ->mkdir($this->pathInstall . '/storage/framework/test', 0765)
                ->mkdir($this->pathInstall . '/storage/framework/cache/data', 0765)
                ->mkdir($this->pathInstall . '/storage/framework/sessions', 0765)
                ->run();

        // if (file_exists($this->pathInstall . '/application/logs')) {
        //     $collection->taskCopyDir([$this->pathInstall . '/application/logs' => $this->pathInstall . 'application/data/logs']);
        //     $collection->taskDeleteDir($this->pathInstall . '/application/logs');
        // }

        return $collection->run();
    }

    /**
     * Installation de bootstrap
     * Commande robo install:bootstrap
     * Nécessite l'installation de de Node & NPM & npm install -g bower
     *
     * @return Robo\Result
     */
    public function installBootstrap() {
        $collection = $this->collectionBuilder();
        $collection->taskExec('bower')
                ->arg('install');

        // $collection->taskCopyDir([
        //     $this->pathToCompil . '/bootstrap-sass/assets/stylesheets' => $this->pathPublic . '/assets/styles/',
        //     $this->pathToCompil . '/bootstrap-sass/assets/javascripts' => $this->pathInstall . '/public/dist/scripts/',
        //     $this->pathToCompil . '/bootstrap-sass/assets/fonts' => $this->pathInstall . '/public/dist/fonts/',
        // ]);

        $collection->taskExec('cp')
                ->arg($this->pathToCompil . '/bootstrap-sass/assets/javascripts/bootstrap.js')
                ->arg($this->pathToCompil . '/scripts/bootstrap.js');
        return $collection->run();
    }

    /**
     * Création d'une migration
     * @param string $table
     *
     * @return Robo\Result
     */
    public function migrateCreate($table) {
        $collection = $this->collectionBuilder();
        $collection->taskExec('php')
                ->arg('artisan')
                ->arg('make:migration')
                ->arg($table);

        return $collection->run();
    }

    /**
     * Migration des tichiers vers la base de données
     *
     * @return Robo\Result
     */
    public function migrateUp() {
        $collection = $this->collectionBuilder();
        $collection->taskExec('php')
                ->arg('artisan')
                ->arg('migrate');

        return $collection->run();
    }

    /**
     * Création de la clé de sécurité
     *
     * @return Robo\Result
     */
    public function key() {
        $collection = $this->collectionBuilder();
        $collection->taskExec('php')
                ->arg('artisan')
                ->arg('key:generate');

        return $collection->run();
    }


    /**
     * Annule toutes les migrations
     *
     * @param     [bool]    $all    par défaut false     *
     * @return Robo\Result
     */
    public function migrateDown($all = false) {
        $collection = $this->collectionBuilder();

        if ($all) {
            $collection->taskExec('php')
            ->arg('artisan')
            ->arg('migrate:reset')
            ;
        } else {
            $collection->taskExec('php')
            ->arg('artisan')
            ->arg('migrate:rollback')
            ;
        }
        return $collection->run();
    }

    /**
     * Rejoue l'ensemble des migrations
     *
     * @return Robo\Result
     */
    public function migrateRefresh() {
        $collection = $this->collectionBuilder();

            $collection->taskExec('php')
            ->arg('artisan')
            ->arg('migrate:refresh')
            ;
        return $collection->run();
    }

    public function installApp() {
        $this->loadConfig();
        $this->buildApp(__DIR__);
    }

    /**
     * Configure le projet en cours
     *
     * @return void
     */
    public function config() {
        $this->configVariables = $this->taskConfiguration()
                ->initConfig($this->getProperties('config'))
                ->initLocal($this->getProperties('local'))
                ->initSettings($this->getProperties('settings'))
                ->configFilePath($this->pathConfigRobo . '/config/my.config')
                ->force(true)
                ->run()
                ->getData();
        // Install project
        $this->installApp();
    }

    /**
     * Compilation de boostrap et minification
     *
     * @return Robo\Result
     */
    private function compilationCss() {
        $collection = $this->collectionBuilder();
        $collection->taskFilesystemStack()
                ->mkdir($this->pathPublic . '/css')
                ->mkdir($this->pathPublic . '/images')
                ->touch($this->pathPublic . '/css/.gitignore')
                ->touch($this->pathPublic . '/css/index.html')
                ->touch($this->pathPublic . '/images/.gitignore')
                ->touch($this->pathPublic . '/images/index.html')
                // Bootstrap
                ->taskScss([
                    $this->pathToCompil . '/bootstrap-sass/assets/stylesheets/_bootstrap.scss' => $this->pathToCompil . '/styles/bootstrap.css'
                ])
                ->importDir($this->pathToCompil . '/bootstrap-sass/assets/stylesheets/')
                // Site
                ->taskScss([
                    $this->pathToCompil . '/sass/app.scss' => $this->pathToCompil . '/styles/styles.css'
                ])
                ->setImportPaths($this->pathToCompil . '/sass/')
                ->taskImageMinify($this->pathToCompil . '/images/*')
                    ->to($this->pathPublic . '/images/')
        ;
        return $collection->run();
    }

  /**
     * Scan le repertoire à la recherche de fichier css ou js
     * @param string $folder
     * @return string|false
     */
    private function appendFile($folder = 'styles') {
        if (!file_exists($this->pathToCompil . "/$folder/")) {
            return false;
        }

        $files = scandir($this->pathToCompil . "/$folder/");

        array_shift($files);
        array_shift($files);
        $script = '';
        if ($files > 0) {
            foreach ($files as $file) {
                $fileExt = substr($file, strrpos($file, '.'));
                if ($fileExt === '.css' || $fileExt === '.js') {
                    $script .= file_get_contents($this->pathToCompil . "/$folder/" . $file) . PHP_EOL;
                }
            }
            return $script;
        } else {
            $this->io()->note('Aucun fichier trouvé dans : ' . $this->pathToCompil . "/$folder/");
            return false;
        }
    }

    /**
     * Compression des fichiers CSS trouvés
     * Recherche automatique dans integration/assets/styles
     *
     * @param string $file = null
     *
     * @return Robo\Result
     */
    private function minificationCss($file = null) {
        $collection = $this->collectionBuilder();
        if ($file === null) {
            if (false !== $style = $this->appendFile('styles')) {
                $style .= $this->appendFile('/styles/parpage');
                $collection->taskMinify($style)
                        ->to($this->pathPublic . '/css/styles.min.css');
            }
        } else {
            $collection->taskMinify($this->pathToCompil . '/styles/' . $file)
                    ->to($this->pathPublic . '/css/' . str_replace('.css', '.min.css', substr($file, strpos($file, '/'))));
        }

        return $collection->run();
    }

    /**
     * Compression des fichiers JS trouvés
     * Recherche automatique dans integration/assets/scripts
     *
     * @param string $file = null
     *
     * @return Robo\Result
     */
    private function minificationJs($file = null) {
        $collection = $this->collectionBuilder();
        if ($file === null) {
            if (false !== $scripts = $this->appendFile('scripts')) {
                $collection->taskMinify($scripts)
                        ->to($this->pathPublic . '/js/main.min.js');
            }
        } else {
            $collection->taskMinify($this->pathToCompil . '/scripts/' . $file)
                    ->to($this->pathPublic . '/js/' . str_replace('.js', '.min.js', substr($file, strpos($file, '/'))));
        }

        return $collection->run();
    }

    /**
     * Retourne les propriétés de configurations
     *
     * @param  string $type
     *
     * @return array
     */
    private function getProperties($type) {
        if (!isset($this->properties)) {
            $this->properties = include $this->pathConfigRobo . '/config/properties.php';
        }
        return $this->properties[$type];
    }

    private function buildApp($appPath) {
        $this->taskCopyReplaceDir([$this->buildDirectory => $appPath])
                ->from(array_keys($this->configVariables))
                ->to($this->configVariables)
                ->startDelimiter('<##')
                ->endDelimiter('##>')
                ->dirPermissions(0755)
                ->filePermissions(0644)
                ->run();
    }

    private function loadConfig() {
        $this->configVariables = $this->taskConfiguration()
                ->initConfig($this->getProperties('config'))
                ->initLocal($this->getProperties('local'))
                ->initSettings($this->getProperties('settings'))
                ->configFilePath($this->pathConfigRobo . '/config/my.config')
                ->run()
                ->getData();
    }

    /**
     * Mess detector.
     *
     * Run the PHP Mess Detector on a file or directory.
     *
     * @param string $file A file or directory to analyze.
     * @param string $type A report format.
     *
     * @return void
     */
    public function mess($file = 'src/', $type = 'text') {
        $this->taskExec("./vendor/bin/phpmd")
                ->args([$file, $type, 'design,unusedcode'])
                ->run();
    }

    /**
     * Code sniffer.
     *
     * Run the PHP Codesniffer on a file or directory.
     *
     * @param string $file A file or directory to analyze.
     * @option $autofix Whether to run the automatic fixer or not.
     * @option $strict Show warnings as well as errors.
     *    Default is to show only errors.
     *
     * @return void
     */
    public function sniff(
    $file = 'src/', $options = [
        'autofix' => false,
        'strict' => false,
    ]
    ) {
        $strict = $options['strict'] ? '' : '-n';
        $result = $this->taskExec("./vendor/bin/phpcs --standard=PSR2 {$strict} {$file}")->run();
        if (!$result->wasSuccessful()) {
            if (!$options['autofix']) {
                $options['autofix'] = $this->confirm('Would you like to run phpcbf to fix the reported errors?');
            }
            if ($options['autofix']) {
                $result = $this->taskExec("./vendor/bin/phpcbf --standard=PSR2 {$file}")->run();
            }
        }
        return $result;
    }

    /**
     * Build the Robo phar executable.
     */
    public function pharBuild($composerPath = null) {
        // Create a collection builder to hold the temporary
        // directory until the pack phar task runs.
        $collection = $this->collectionBuilder();
        $workDir = $collection->tmpDir();
        $roboBuildDir = "$workDir/robo";
        // Before we run `composer install`, we will remove the dev
        // dependencies that we only use in the unit tests.  Any dev dependency
        // that is in the 'suggested' section is used by a core task;
        // we will include all of those in the phar.
        $devProjectsToRemove = $this->devDependenciesToRemoveFromPhar();
        $depProjectsToAdd = $this->dependenciesToAddFromPhar();
        // We need to create our work dir and run `composer install`
        // before we prepare the pack phar task, so create a separate
        // collection builder to do this step in.
        $prepTasks = $this->collectionBuilder();
        $preparationResult = $prepTasks
                ->taskFilesystemStack()
                ->mkdir($workDir)
                ->taskRsync()
                ->fromPath(__DIR__ . '/')
                ->toPath($roboBuildDir)
                ->recursive()
                ->exclude(
                        [
                            'vendor/',
                            '.idea/',
                            'docs/',
                            'composer.phar',
                            'composer.lock',
                            'robo.phar',
                        ]
                )
                ->taskComposerRemove($composerPath)
                ->dir($roboBuildDir)
                ->dev()
                ->noUpdate()
                ->printOutput(false)
                ->args($devProjectsToRemove)
                ->taskExec($composerPath . ' require ')
                ->dir($roboBuildDir)
                ->option('--no-update')
                ->printOutput(false)
                ->args($depProjectsToAdd)
                ->taskComposerInstall($composerPath)
                ->dir($roboBuildDir)
                ->printOutput(false)
                ->run();
        // Exit if the preparation step failed
        if (!$preparationResult->wasSuccessful()) {
            return $preparationResult;
        }
        // Decide which files we're going to pack
        $files = Finder::create()->ignoreVCS(true)
                ->files()
                ->name('*.php')
                ->name('*.json')
                ->name('LICENSE')
                ->name('*.exe') // for 1symfony/console/Resources/bin/hiddeninput.exe
                ->name('GeneratedWrapper.tmpl')
                ->path('src')
                ->path('vendor')
                ->notPath('/vendor\/.*\/[Tt]est[s?]\//')
                ->in($roboBuildDir);
        // Build the phar
        return $collection
                        ->taskPackPhar('robo.phar')
                        ->addFiles($files)
                        ->executable('vendor/bin/robo')
                        ->taskFilesystemStack()
                        ->chmod('robo.phar', 0777)
                        ->run();
    }

    /**
     * The phar:build command removes the project requirements from the
     * 'require-dev' section that are not in the 'suggest' section.
     *
     * @return array
     */
    protected function devDependenciesToRemoveFromPhar() {
        $composerInfo = (array) json_decode(file_get_contents(__DIR__ . '/composer.json'));
        $devDependencies = array_keys((array) $composerInfo['require-dev']);
        $suggestedProjects = [];
        if (isset($composerInfo['suggest'])) {
            $suggestedProjects = array_keys((array) $composerInfo['suggest']);
        }
        return array_diff($devDependencies, $suggestedProjects);
    }

    /**
     * The phar:build command removes the project requirements from the
     * 'require-dev' section that are not in the 'suggest' section.
     *
     * @return array
     */
    protected function dependenciesToAddFromPhar() {
        $composerInfo = (array) json_decode(file_get_contents(__DIR__ . '/vendor/consolidation/robo/composer.json'));
        $devDependencies = array_keys((array) $composerInfo['require-dev']);
        $suggestedProjects = [];
        if (isset($composerInfo['suggest'])) {
            $suggestedProjects = array_keys((array) $composerInfo['suggest']);
        }
        return array_intersect($devDependencies, $suggestedProjects);
    }



    //------------------------------------------------------------

    public function tt($name = null, $options = ['group' => null, 'no-fail-fast' => false, 'report' => null, 'debug' => false])
    {
        // Build test env
        // $this->loadTestConfig();

        $collection = $this->collectionBuilder();


        // $defaultConfig = function () {
        //     $this->loadConfig();
        //     $this->buildApp(__DIR__);
        // };
        // $collection->rollbackCode($defaultConfig);
        // $collection->addTask($this->buildAppTask(__DIR__))
        //     ->addTask($this->migrateUpTask()->option('--quiet'))
        //     ->addTask($this->taskDeleteDir([
        //             __DIR__ . '/tests/_output',
        //     ]));

        $tests = ['tt'];
        // if ($name) {
        //     $tests = [$name];
        // } else {
        //     $tests = [
        //         'webservice',
        //         'Generic',
        //         'DemandePec',
        //         'Convention',
        //         'Salarie',
        //         'Collecte',
        //     ];
        // }

        foreach ($tests as $name) {
            $testCollection = $this->collectionBuilder();
            // $testCollection->addTask($this->seedRunTask()->option('-q'));
            $testTask = $this->taskCodecept();
            $testTask->test($name);
            if ($options['group']) {
                $testTask->group($options['group']);
            }

            if ($options['report']) {
                if ($options['report'] === 'xml') {
                    $report_name = 'report_'.$name.'.xml';
                    $testTask->xml($report_name);
                } else {
                    $report_name = 'report_'.$name.'.html';
                    $testTask->html($report_name);
                }
            }

            if ($options['debug'] !== false) {
                $testTask->debug();
            }

            if ($options['no-fail-fast'] === false) {
                $testTask->option('fail-fast');
            }

            $testCollection->addTask($testTask);

            if ($options['report']) {
                $testCollection->addTask(
                    $this->taskExec('cp')
                        ->arg(__DIR__.'/tests/_output/'.$report_name)
                        ->arg(__DIR__.'/reports/'.$report_name)
                );
            }
            $collection->addTask($testCollection);
        }
        return $collection;
    }




    /**
     * Start tests
     *
     * @param  testName $name  The test name
     *
     * @option string group group
     * @option string report html / xml
     */
    public function tests($name = null, $options = ['group' => null, 'no-fail-fast' => false, 'report' => null, 'debug' => false])
    {
        // Build test env
        $this->loadTestConfig();

        $collection = $this->collectionBuilder();


        $defaultConfig = function () {
            $this->loadConfig();
            $this->buildApp(__DIR__);
        };
        $collection->rollbackCode($defaultConfig);
        $collection->addTask($this->buildAppTask(__DIR__))
            ->addTask($this->migrateUpTask()->option('--quiet'))
            ->addTask($this->taskDeleteDir([
                    __DIR__ . '/tests/_output',
            ]));


        if ($name) {
            $tests = [$name];
        } else {
            $tests = [
                'My',
                'Feature',
            ];
        }

        foreach ($tests as $name) {
            $testCollection = $this->collectionBuilder();
            $testCollection->addTask($this->seedRunTask()->option('-q'));
            $testTask = $this->taskCodecept();
            $testTask->test($name);
            if ($options['group']) {
                $testTask->group($options['group']);
            }

            if ($options['report']) {
                if ($options['report'] === 'xml') {
                    $report_name = 'report_'.$name.'.xml';
                    $testTask->xml($report_name);
                } else {
                    $report_name = 'report_'.$name.'.html';
                    $testTask->html($report_name);
                }
            }

            if ($options['debug'] !== false) {
                $testTask->debug();
            }

            if ($options['no-fail-fast'] === false) {
                $testTask->option('fail-fast');
            }

            $testCollection->addTask($testTask);

            if ($options['report']) {
                $testCollection->addTask(
                    $this->taskExec('cp')
                        ->arg(__DIR__.'/tests/_output/'.$report_name)
                        ->arg(__DIR__.'/reports/'.$report_name)
                );
            }
            $collection->addTask($testCollection);
        }
        return $collection;
    }


    private function loadTestConfig()
    {
        $config = $this->getProperties('config');
        $config['WEBDRIVER_HOST'] = [
            'question' => 'Webdriver Host'
        ];
        $config['WEBDRIVER_PORT'] = [
            'question' => 'Webdriver Port',
            'default' => 9515
        ];
        $config['WEBSERVICE_HTTP'] = [
            'question' => 'Webservice HTTP path'
        ];

        $this->configVariables = $this->taskConfiguration()
         ->initConfig($config)
         ->initLocal($this->getProperties('local'))
         ->initSettings($this->getProperties('settings'))
        //  ->configFilePath($this->configDirectory . 'test.config')
         ->run()
         ->getData();

        $this->postLoadConfig();
    }


    private function installDependencies($appPath)
    {
        // Install composer dependencies
        $task = $this->taskComposerInstall()
            ->workingDir($appPath)
            ->preferDist();
        if ($this->configVariables['ENV'] == 'prod') {
            $task->noDev()
                ->optimizeAutoloader();
        }
        $task->run();
    }

}