<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Langue pour les contacts
    |--------------------------------------------------------------------------
    |
    |
    */
    // interface
    'envoyer'                       => 'Envoyer',



    // validation
    'message_mail_envoye'           => 'Votre message à été envoyé à notre équipe.',
    'message_verification_mail'     => 'Vous allez recevoir un mail afin de confirmer l\'envoi de votre message.',

    // question
    'question_suppression'          => 'Voulez vous supprimer les informations de ce message ?',

    // erreur
    'erreur_mail_deja_envoye'       => 'Votre message est déjà parvenu à notre équipe',

    // autorisation
    'autorisation_page'             => 'Aucune page n\'a été trouvé',

];
