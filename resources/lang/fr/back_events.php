<?php

return [

    // index
    'title'                         => [
        'index'         => 'Liste des évenements',
        'edit'          => 'Edition d\'un évenemnt',
        'create'        => 'Création d\'un évenemnt',
    ],

    'events_titre'                  => 'Titre',
    'events_inscriptions'           => 'Inscriptions',
    'events_lieu'                   => 'Lieu',
    'events_date_debut'             => 'Date de début',
    'events_date_fin'               => 'Date de fin',
    'events_action'                 => 'Action',

    // form
    'form'                          => [
        'titre'                     => 'Titre de l\'évenement *',
        'description'               => 'Description',
        'debut'                     => 'Début de l\'évenement *',
        'fin'                       => 'Fin de l\'évenement *',
        'lieu'                      => 'Lieu de l\'évenement *',
        'adresse'                   => 'Adresse de l\'évenement',
        'places'                    => 'Nb de places',
        'couleur'                   => 'Couleur sur le calendrier',
        'lien'                      => 'Lien pour des informations supplémentaire',
    ],

];
