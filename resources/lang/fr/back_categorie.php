<?php

return [

    // index
    'titre'                         => [
        'index'         => 'Liste des catégories',
        'edit'          => 'Edition de la catégorie : :titre',
        'create'        => 'Création de la catégorie',
        'show'          => 'Catégorie : :titre',
    ],

    // index
    'list'                          => [
        'etat'                          => 'Etat',
        'categorie'                     => 'Catégorie',
        'liste'                         => 'Liste des articles liés à la catégorie',
        'ajouter_categorie'             => 'Ajouter une nouvelle catégorie à la racine',
    ],

    // form
    'form'                          => [
        'rubrique'                     => 'Rubrique',
        'titre'                        => 'Titre',
        'contenu'                      => 'Contenu',
    ],

];
