<?php

return [

    // index
    'titre'                         => [
        'index'         => 'Liste des articles',
        'edit'          => 'Edition de l\'article : :titre',
        'create'        => 'Création de l\'article',
        'mes_articles'  => 'Liste des articles',
    ],

    // index
    'list'                          => [
        'article'                       => 'Article',
        'categorie'                     => 'Catégorie',
        'auteur'                        => 'Auteur',
        'date'                          => 'Date',
        'titre'                         => 'Titre',
        'contenu'                       => 'Contenu',
    ],

    // form
    'form'                          => [
        'rubrique'                     => 'Rubrique',
        'titre'                        => 'Titre',
        'contenu'                      => 'Contenu',
    ],

];
