<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Langue communes
    |--------------------------------------------------------------------------
    |
    |
    */
    // interface
    'ajouter'                       => 'Ajouter',
    'enregistrer'                   => 'Enregistrer',
    'champs_obligatoires'           => '(* champs obligatoires)',


    // validation
    'message_enregistrement'        => 'L\'enregistrement a été effectué avec succès',
    'message_maj'                   => 'L\'enregistrement a été mis à jour avec succès',
    'message_suppression'           => 'Enregistrement supprimé.',

    // question
    'question_suppression'          => 'Voulez vous supprimer le fichier ?',
    'question_confirmation'         => 'Confirmation ?',

    // erreur
    'erreur_enregistrement'         => 'Echec de l\'enregistrement.',
    'erreur_detection'              => 'Aucun enregistrement trouvé.',

    // autorisation
    'autorisation_page'             => 'Vous n\'êtes pas autorisé à accéder à cette page',
    'autorisation_page_front'       => 'La page demandée n\'a pas été trouvée.',
    'autorisation_action'           => 'Vous n\'êtes pas autorisé à effectuer cette action',

    // exception
    'exception_type_inconnu'        => 'Le type indiqué est inconnu.',
    'exception_url_inconnu'         => 'Impossible de trouver l\'url demandée ":url".',
    'exception_ajax_serveur'        => 'Problème de communication avec le serveur.',

];
