<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Menus
    |--------------------------------------------------------------------------
    |
    */

    // Left
    'left_menu_title_back'         => 'Aqua13 | Administration',

    'left_menu_online'             => 'En lgne',
    'left_menu_offline'            => 'Hors lgne',
    'left_menu_events'             => 'Evenements',
    'left_menu_category'           => 'Rubriques',
    'left_menu_article'            => 'Articles',
    'left_menu_my_article'         => 'Mes articles',
    'left_menu_doc'                => 'Documents',
    'left_menu_pictures'           => 'Galerie photos',
    'left_menu_registers'          => 'Inscriptions',
    'left_menu_divers'             => 'Palanquée',

    // Top

    // Breadcrumb
    'ariane'                        => [
        'home'                  => 'Home',
        'events'                => 'Evenements',
        'category'              => 'Rubriques',
        'article'               => 'Articles',
        'doc'                   => 'Documents',
        'register'              => 'Inscriptions',
        'action'                => [
                'create'    => 'Création',
                'edit'      => 'Edition',
                'delete'    => 'Suppression',
                'index'     => 'Liste',
        ],
    ],

    'actions'                   => 'Actions',

];
