<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'init_utilisateur_sujet' => 'Vos identifiants sur Aqua13Plongée',
    'contact_envoi_message_staff' => 'Question provenant du site Aqua13Plongée',
    'contact_verification_mail' => 'Vérification de votre courriel',

];
