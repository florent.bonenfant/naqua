@extends('front.layouts.app')

@section('content')

    <div id="edito">
        <h1>{{ $edito->titre }}</h1>
        {!! $edito->texte !!}
    </div>

    <div id="calendrier">
      <div id="calendar" ></div>
    </div>

    <div id="lastPic">

        <div class="galerieVign">

            <div class="pic1"><a href="Estartit-2009.html"><img src='local/cache-vignettes/L75xH100/IMG_2007-819e4.jpg' width='75' height='100' style='height:100px;width:75px;' /></a></div>

            <div class="vignLabel">
                <h1>GALERIE PHOTOS</h1>
                Estartit 2009<br/>
                30/08/2010<br/>
            </div> <!-- fin vignLabel -->

        </div> <!-- fin galerieVign -->
    </div> <!-- fin lastpic -->

@endsection

@section('js')
<!-- fullCalendar 2.2.5 -->
<script src="{{ asset('js/moment.min.js')}}"></script>
<script src="{{ asset('js/fullcalendar.min.js')}}"></script>


<script>
$(function () {

/* initialize the external events
-----------------------------------------------------------------*/
function ini_events(ele) {
  ele.each(function () {

    // create an Event Object (http://arshaw.com/fullcalendar/docs/event_data/Event_Object/)
    // it doesn't need to have a start or end
    var eventObject = {
      title: $.trim($(this).text()) // use the element's text as the event title
    };

    // store the Event Object in the DOM element so we can get to it later
    $(this).data('eventObject', eventObject);

    // make the event draggable using jQuery UI
    $(this).draggable({
      zIndex: 1070,
      revert: true, // will cause the event to go back to its
      revertDuration: 0  //  original position after the drag
    });

  });
}
ini_events($('#external-events div.external-event'));

  /* initialize the calendar
  -----------------------------------------------------------------*/

  $('#calendar').fullCalendar({
  header: {
    left: 'prev,next today',
    center: 'title',
    right: 'month,agendaWeek,agendaDay'
  },
  buttonText: {
    today: 'today',
    month: 'month',
    week: 'week',
    day: 'day'
  },
  height: 380,
  // contentHeight: 380,
  //Random default events
  events: [
    @foreach($evenements as $evenement)
    {
      title: '{{ $evenement->titre }}',
      start: new Date('{{ $evenement->date_debut }}'),
      end: new Date('{{ $evenement->date_fin }}'),
      url: '{{ $evenement->url }}',
      backgroundColor: '{{ $evenement->couleur }}',
      borderColor: "#a3a3a3 "
    },
    @endforeach
  ],
  eventClick: function(event) {
    if (event.url) {
        window.open(event.url, "_blank");
        return false;
    }
  },
  editable: false,
  droppable: false, // this allows things to be dropped onto the calendar !!!
  drop: function (date, allDay) { // this function is called when something is dropped

    // retrieve the dropped element's stored Event Object
    var originalEventObject = $(this).data('eventObject');

    // we need to copy it, so that multiple events don't have a reference to the same object
    var copiedEventObject = $.extend({}, originalEventObject);

    // assign it the date that was reported
    copiedEventObject.start = date;
    copiedEventObject.allDay = allDay;
    copiedEventObject.backgroundColor = $(this).css("background-color");
    copiedEventObject.borderColor = $(this).css("border-color");

    // render the event on the calendar
    // the last `true` argument determines if the event "sticks" (http://arshaw.com/fullcalendar/docs/event_rendering/renderEvent/)
    $('#calendar').fullCalendar('renderEvent', copiedEventObject, true);

    // is the "remove after drop" checkbox checked?
    if ($('#drop-remove').is(':checked')) {
      // if so, remove the element from the "Draggable Events" list
      $(this).remove();
    }

  }
});

  /* ADDING EVENTS */
  var currColor = "#3c8dbc"; //Red by default
  //Color chooser button
  var colorChooser = $("#color-chooser-btn");
  $("#color-chooser > li > a").click(function (e) {
    e.preventDefault();
    //Save color
    currColor = $(this).css("color");
    //Add color effect to button
    $('#add-new-event').css({"background-color": currColor, "border-color": currColor});
  });
  $("#add-new-event").click(function (e) {
    e.preventDefault();
    //Get value and make sure it is not null
    var val = $("#new-event").val();
    if (val.length == 0) {
      return;
    }

    //Create events
    var event = $("<div />");
    event.css({"background-color": currColor, "border-color": currColor, "color": "#fff"}).addClass("external-event");
    event.html(val);
    $('#external-events').prepend(event);

    //Add draggable funtionality
    ini_events(event);

    //Remove event from text input
    $("#new-event").val("");
  });
});
</script>
@endsection

@section('css')

<link href="{{ asset('css/fullcalendar.min.css') }}" rel="stylesheet" media="projection, screen, tv" >
<link href="{{ asset('css/fullcalendar.print.min.css') }}" rel="stylesheet" media="print" >
  <style>
  #calendar .fc-week {
    min-height:3em;
  }
  #calendar .fc-day-grid-container {
    overflow:hidden;
  }
  #calendar .fc-toolbar {
    margin-bottom: 0;
  }
  #calendar {
    background-color: white;
    color:black;
    height:auto;
  }</style>
@endsection
