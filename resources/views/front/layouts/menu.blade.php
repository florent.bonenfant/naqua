<?php

/* 
 *  
 *  
 *  @author  Florent Bonenfant <social1@niweb.eu>
 *  @version 1.0
 */

use App\Repositories\CategoryRepository; 
$category = (new CategoryRepository())->getAll();
?>


<ul class="menuN1">
    @foreach($category as $categorie)
    @if($categorie->id_rubrique == $categorie->id_secteur)
        <li class="menuN1">
            <span class="menuN1">{{ $categorie->titre }}</span>
            <ul class="menuN2">
                @foreach($category as $ssCategorie)
                    @if($ssCategorie->id_parent == $categorie->id_rubrique)
                    <?php $url = (is_object(getUrlByTypeAndId(URLS_TYPE_RUBRIQUE, $ssCategorie->id_rubrique)) ? getUrlByTypeAndId(URLS_TYPE_RUBRIQUE, $ssCategorie->id_rubrique)->url : '') ?>
                    <li class="{{ (!empty($url) && strpos($_SERVER['REQUEST_URI'], $url) !== false ? 'menu1_on' : '' ) }}" >
                        <a class="menuN2" href="{{ $url }}" >{{ $ssCategorie->titre }}</a>
                    </li>
                    @endif
                @endforeach
            </ul>
        </li>
    @endif
    @endforeach
</ul>

