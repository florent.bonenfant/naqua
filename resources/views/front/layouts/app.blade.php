<!DOCTYPE html>
<html lang="fr">
    <head>
        <meta name="robots" content="all"/>
        <meta name="description" content="Club Aqua13, club de plongée parisien au : 5 Place Paul Verlaine, 75013 Paris"/>
        <meta name="keywords" lang="fr-ch" content="club plongée, club de plongée, club de plongée paris, paris 13, 75013, plongée 75013, piscine paris"/>
        <meta name="category" content="Environnement"/>
        <meta name="distribution" content="global"/>
        <meta name="revisit-after" content="7 days"/>
        <meta name="author" lang="fr-ch" content="Florent Bonenfant"/>
        <meta name="copyright" content="Florent Bonenfant"/>
        <meta name="identifier-url" content="http://www.aqua13plongee.fr"/>
        <meta name="expires" content="never"/>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
        <meta http-equiv="Content-Language" content="fr-FR"/>
        <meta http-equiv="Content-Style-Type" content="text/css"/>
        <link href="{{ asset('css/styles.min.css') }}" rel="stylesheet" media="projection, screen, tv" >
        @yield('css')
        <!--[if lt IE 7.]>
            <link rel="stylesheet" href="{{ asset('css/ie6fix.css') }}" type="text/css" media="all" />
        <![endif]-->
        <title>Aqua13 Plongée</title>
    </head>
    <body>

        <div id="page">
            <div id="logoSite">
                <a href="http://aqua13plongee.fr"><img src="../ui/fr/Logo_aqua13.png" alt="Aqua13 plongée" width="100" height="120" class="spip_logos" border='0' /></a>
            </div>

            <div id="barreCnx">
                <img src="../ui/fr/barre_hautG.png" style="float: left;" />
                <div id="identification" class="barreTop">
                    <div class='formulaire_spip formulaire_login'>
                        <br class='bugajaxie' />

                        @if (!isGuest())
                        <form method="POST" action="{{ route('login') }}" aria-label="{{ __('Login') }}" id='formulaire_login' >
                            {{ csrf_field() }}

                            <ul>
                                <li class="editer_login obligatoire">
                                    <input id="email" type="email" class="text input-sm{{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" value="{{ old('email') }}" required autofocus>
                                </li>
                                <li class="editer_password obligatoire">
                                    <input id="password" type="password" class="text input-sm{{ $errors->has('password') ? ' is-invalid' : '' }}" name="password" required>
                                </li>
                                <li class="editer_remember">
                                    <label class="form-check-label" for="remember">Se souvenir</label>
                                    <input class="form-check-input" type="checkbox" name="remember" id="remember" {{ old('remember') ? 'checked' : '' }}>
                                    &nbsp;
                                </li>
                                <li>
                                    <input type="submit" class="submit input-sm" value="Login" />
                                </li>
                            </ul>
                        </form>
                        @else
                            Membre : {{ isGuest()->name }} Mon compte <a href="{{ route('logout') }} ">Déconnection</a>
                        @endif
                    </div>
                </div>
                <div id="" class="barreTop"><a href="{{ route('register') }}">Enregistrement&nbsp;</a></div>
            </div>

            <div id="main">
                <div id="menu">
                    @include('front.layouts.menu')
                </div>
                <div id="sommaireMain">
{{-- @todo Ajuster template pour les messages flashs --}}
                    <div class="content-wrapper">
                        @if (session('messageSuccess') && session('messageError') == '')
                            <div class="alert alert-success alert-dismissable">
                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                            <h4> <i class="icon fa fa-check"></i> Infos</h4>
                            {{ session('messageSuccess') }}
                            </div>

                        @endif
                        @if (session('messageError'))
                            <div class="alert alert-danger alert-dismissable">
                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                            <h4><i class="icon fa fa-ban"></i> Alert!</h4>
                            {{ session('messageError') }}
                            </div>
                        @endif

                        @yield('content')
                    </div>
                </div>
            </div> <!-- fin main -->
        </div> <!-- fin page -->
        <div id="pied">
        Aqua 13 Plong&eacute;e - <a href="Mentions-legales.html">Mentions légales</a> - <a href="{{ route('contact.show') }}">Contactez nous</a> - Design : <a href="http://www.katelia.com">Katelia</a> - R&eacute;alisation : <a href="http://www.gaiaservice.fr">Gaiaservice</a>
        </div>

{{-- @todo ajouter les style et plugins de facon à les compiler automatiquement --}}
        <script src="{{ asset('js/main.min.js') }}" type="text/javascript"></script>
        <script type="text/javascript">
            jQuery(document).ready(function() {
            // $("a.tooltip").simpletooltip({});
                // $("#galerie").transition();
            });
            function showContent(id) {
                $.post("spip.php?page=sommairedyn", { rub: id}, function(data){
                    $("#sommaireMain").html(data);
                });
            }
            var gaJsHost = (("https:" == document.location.protocol) ? "https://ssl." : "http://www.");
            document.write(unescape("%3Cscript src='" + gaJsHost + "google-analytics.com/ga.js' type='text/javascript'%3E%3C/script%3E"));
            try {
                        var pageTracker = _gat._getTracker("UA-10597555-1");
                pageTracker._trackPageview();
            }                        catch(err) {}
        </script>

        <script src="{{ asset('js/plugins/jQuery/jQuery-2.1.4.min.js') }}"></script>
        @yield('js')

 <!--                            <script type="text/javascript" src="http://www.aqua13plongee.fr/prive/javascript/md5.js"></script>

            // <script type='text/javascript'>/*<    ![CDATA[*/
            // var alea_actuel='';
            // var alea_futur='';
            // var login='';
            // var page_auteur = 'http://www.aqua13plongee.fr/spip.php?page=informerauteur';
            //         var informe_auteur_en_cours = false;
            //         var attente_informe = 0;

                    // (function($){
            // affiche_login_secure();
            // $('#var_login').change(actualise_auteur);
            // $('form#formulaire_login').submit(login_submit);
            // }(jQuery));

            // /*]]>*/</script>

                            // <!-- Fin header du Couteau Suisse -            ->

            <script type="text/javascript">

            jQuery(document).ready(function()	{
            // ajoute les barres d'outils markitup
            function barrebouilles(){
            // si c'est un appel de previsu markitup, faut pas relancer
            // on attrappe donc uniquement les textarea qui n'ont pas deja la classe markItUpEditor
            jQuery('.formulaire_forum textarea[name=texte]                                    :not(.markItUpEditor)').markItUp(barre_outils_forum, {lang:'fr'});
            jQuery('textarea.textarea_forum:not(.markItUpEditor)').markItUp(barre_outils_forum,{lang:'fr'});
            jQuery('.formulaire_spip textarea[name=texte]:not(.markItUpEditor)')
                    .markItUp(barre_outils_edition,{lang:'fr'})
                    .previsu_spip({
                    previewParserPath:'http://www.aqua13plongee.fr/spip.php?page=porte_plume_preview',
                    textEditer:'&Eacute;diter',
                    textVoir:'Voir'});
            }
            barrebouilles();
            onAj                axLoad(barrebouilles);
            
            });           	
        </script> -->	

        <script type="text/javascript">
            var cs_prive = window.location.pathname.match(/\/ecrire\/$/) != null;
            function soft_scroller_init(){
                if (typeof jQuery.localScroll == "function")
                jQuery.localScroll({hash:true})
            }
            var cs_init = function(){
                soft_scroller_init.apply(this);
            }
            if (typeof onAjaxLoad == 'function')onAjaxLoad(cs_init);
            if (window.jQuery){
                var cs_sel_jQuery = typeof jQuery(document).selector == 'undefined'?'@':'';
                var cs_CookiePlugin = "http://aqua13plongee.fr/prive/javascript/jquery.cookie.js";
                jQuery(document).ready(function(){
                cs_init.apply(document);
                });
            }
        </script>
    </body>
</html>