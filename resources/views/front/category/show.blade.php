<?php
/*
 *  @author  Florent Bonenfant <social1@niweb.eu>
 *  @version 1.0
 */
?>
@extends('front.layouts.app')

@section('content')
<div id="chemin">{{ $category->titre }}</div>

{!! ($category->texte) !!}


<?php
if ($pos = strpos($category->texte, '<formulaire|')) {
        preg_match_all(
            '`<formulaire\|(.*)>`isU',
            $category->texte,
            $retour
);
?>
    @include('front.forms.' . $retour[1][0])
<?php
}
?>
@endsection