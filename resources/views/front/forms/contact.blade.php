
<form action='{{ route('contact.store') }}' method='post' enctype="multipart/form-data" class="formcontact">
	{{ csrf_field() }}

    <fieldset>
            <legend>Informations</legend>

			<div class="form-group {{ $errors->has('nom') ? 'has-error' : '' }}">
				<label class="col-sm-3 col-form-label text-md-right" for="nom">Nom</label>
				<div class="col-md-9">
					<input type="text" class="form-control" id="nom" name="nom" value="{{ old('nom') }}">
					<span class="help-block">{!! $errors->first('nom', '<div class="invalid-feedback">:message</div>') !!}</span>
				</div>
			</div>
			<div class="form-group {{ $errors->has('prenom') ? 'has-error' : '' }}">
				<label class="col-sm-3 col-form-label text-md-right" for="prenom">Prénom *</label>
				<div class="col-md-9">
					<input type="text" class="form-control" id="prenom" name="prenom" value="{{ old('prenom') }}">
					<span class="help-block">{!! $errors->first('prenom', '<div class="invalid-feedback">:message</div>') !!}</span>
				</div>
			</div>
			<div class="form-group {{ $errors->has('telephone') ? 'has-error' : '' }}">
					<label class="col-sm-3 col-form-label text-md-right" for="telephone">Téléphone domicile</label>
					<div class="col-md-9">
						<input type="text" class="form-control" id="telephone" name="telephone" value="{{ old('telephone') }}">
						<span class="help-block">{!! $errors->first('telephone', '<div class="invalid-feedback">:message</div>') !!}</span>
					</div>
			</div>
			<div class="form-group {{ $errors->has('email') ? 'has-error' : '' }}">
					<label class="col-sm-3 col-form-label text-md-right" for="email">Email *</label>
					<div class="col-md-9">
						<input type="text" class="form-control" id="email" name="email" value="{{ old('email') }}">
						<span class="help-block">{!! $errors->first('email', '<div class="invalid-feedback">:message</div>') !!}</span>
					</div>
			</div>

            <div class="form-group {{ $errors->has('question') ? 'has-error' : '' }}">
					<label class="col-sm-3 col-form-label text-md-right" for="question">Question *</label>
					<div class="col-md-9">
						<textarea type="text" class="form-control" id="question" name="question" >{{ old('question') }}</textarea>
						<span class="help-block">{!! $errors->first('question', '<div class="invalid-feedback">:message</div>') !!}</span>
					</div>
			</div>
    </fieldset>

	<br/>

    <!-- @todo mettre en place un captcha ou vérification mail client -->
    <input type='submit' name='ok' value='Envoyer' class="submit"/><br/><br/>
        (* champs obligatoires)

</form>