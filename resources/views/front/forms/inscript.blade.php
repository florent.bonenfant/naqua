@extends('front.layouts.app')

@section('content')
{{-- <div id="sommaireMain"> --}}

		<div id="chemin">Le Club Aqua 13 - Adhésion</div>
		<h4 class="spip">Demande d’adhésion</h4>

<p> Bulletin réservé aux anciens adhérents et aux adhérents préinscrits</p>

<dl class="spip_document_330 spip_documents spip_documents_right" style="float:right;">
	<dt><a href="documents/Reglement_interieur_2013-2.pdf" title="PDF - 84.4 ko" type="application/pdf" class="spip_ancre"><img src="squelettes/vignettes/pdf.png" alt="PDF - 84.4 ko" width="26" height="26"></a></dt>
</dl>

<p>Merci de compléter ce formulaire avec le plus grand soin, de bien lire le <a href="documents/Reglement_interieur_2013-2.pdf" class="spip_in">règlement intérieur du club.</a>
<br>
<br>
<br>Après validation du formulaire, remettre <strong>une photocopie</strong> de votre certificat médical et votre règlement à la secrétaire ou l’une de ses adjointes (Alexandra, Marine, Anne) avant le <strong>2 octobre 2018</strong>
<br>
<br>Envoyez votre photo, afin d’accéder à l’espace membre du site internet  à secretariat.aqua13plongee[@]gmail.com
<br>
<br></p>

@if ($errors->any())
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif

<form action='{{ route('inscription.store') }}' method='post' enctype="multipart/form-data" class="forminscription">
	{{ csrf_field() }}

    <fieldset>
            <legend>Coordonnées</legend>

			<div class="form-group {{ $errors->has('civilite') ? 'has-error' : '' }}">
				<label class="col-sm-3 col-form-label text-md-right" for="civilite">Civilité *</label>
				  	<label>
						<input type="radio" name="civilite" value="Mr" {{ (old('civilite') ?? $lastRegistration->civilite) === 'Mr' ? 'checked' : '' }}>
						Monsieur&nbsp;&nbsp;
				  	</label>
				  	<label>
						<input type="radio" name="civilite" value="Mme" {{ (old('civilite') ?? $lastRegistration->civilite) === 'Mme' ? 'checked' : '' }}>
						Madame&nbsp;&nbsp;
				  	</label>
				  	<label>
						<input type="radio" name="civilite" value="Mlle" {{ (old('civilite') ?? $lastRegistration->civilite) === 'Mlle' ? 'checked' : '' }}>
						Mademoiselle
				  	</label>
				<span class="help-block">{!! $errors->first('civilite', '<div class="invalid-feedback">:message</div>') !!}</span>
			</div>
			<div class="form-group {{ $errors->has('nom') ? 'has-error' : '' }}">
				<label class="col-sm-3 col-form-label text-md-right" for="nom">Nom *</label>
				<div class="col-md-9">
					<input type="text" class="form-control" id="nom" name="nom" value="{{ old('nom') ?? $lastRegistration->nom }}">
					<span class="help-block">{!! $errors->first('nom', '<div class="invalid-feedback">:message</div>') !!}</span>
				</div>
			</div>
			<div class="form-group {{ $errors->has('prenom') ? 'has-error' : '' }}">
				<label class="col-sm-3 col-form-label text-md-right" for="prenom">Prénom *</label>
				<div class="col-md-9">
					<input type="text" class="form-control" id="prenom" name="prenom" value="{{ old('prenom') ?? $lastRegistration->prenom }}">
					<span class="help-block">{!! $errors->first('prenom', '<div class="invalid-feedback">:message</div>') !!}</span>
				</div>
			</div>
			<div class="form-group {{ $errors->has('datenaissance') ? 'has-error' : '' }}">
				<label class="col-sm-3 col-form-label text-md-right" for="datenaissance">Date de naissance *</label>
				<div class="col-md-9">
						<input type='text' name='datenaissance' value="{{ old('datenaissance') ?? Carbon\Carbon::createFromFormat('Y-m-d', $lastRegistration->datenaissance)->format('d/m/Y') }}" class="form-control" placeholder="jj/mm/aaaa"/>
					<span class="help-block">{!! $errors->first('datenaissance', '<div class="invalid-feedback">:message</div>') !!}</span>
				</div>
			</div>
			<div class="form-group {{ $errors->has('villenaiss') ? 'has-error' : '' }}">
					<label class="col-sm-3 col-form-label text-md-right" for="villenaiss">Ville de naissance *</label>
					<div class="col-md-9">
						<input type="text" class="form-control" id="villenaiss" name="villenaiss" value="{{ old('villenaiss') ?? $lastRegistration->villenaiss }}">
						<span class="help-block">{!! $errors->first('villenaiss', '<div class="invalid-feedback">:message</div>') !!}</span>
					</div>
			</div>
			<div class="form-group {{ $errors->has('deptnaiss') ? 'has-error' : '' }}">
					<label class="col-sm-3 col-form-label text-md-right" for="deptnaiss">Département *</label>
					<div class="col-md-9">
						<input type="text" class="form-control" id="deptnaiss" name="deptnaiss" value="{{ old('deptnaiss') ?? $lastRegistration->deptnaiss }}">
						<span class="help-block">{!! $errors->first('deptnaiss', '<div class="invalid-feedback">:message</div>') !!}</span>
					</div>
			</div>
			<div class="form-group {{ $errors->has('nationalite') ? 'has-error' : '' }}">
					<label class="col-sm-3 col-form-label text-md-right" for="nationalite">Nationalité *</label>
					<div class="col-md-9">
						<input type="text" class="form-control" id="nationalite" name="nationalite" value="{{ old('nationalite') ?? $lastRegistration->nationalite }}">
						<span class="help-block">{!! $errors->first('nationalite', '<div class="invalid-feedback">:message</div>') !!}</span>
					</div>
			</div>
			<div class="form-group {{ $errors->has('profession') ? 'has-error' : '' }}">
					<label class="col-sm-3 col-form-label text-md-right" for="profession">Profession *</label>
					<div class="col-md-9">
						<input type="text" class="form-control" id="profession" name="profession" value="{{ old('profession') ?? $lastRegistration->profession }}">
						<span class="help-block">{!! $errors->first('profession', '<div class="invalid-feedback">:message</div>') !!}</span>
					</div>
			</div>
			<div class="form-group {{ $errors->has('adresse') ? 'has-error' : '' }}">
					<label class="col-sm-3 col-form-label text-md-right" for="adresse">Adresse *</label>
					<div class="col-md-9">
						<textarea type="text" class="form-control" id="adresse" name="adresse" >{{ old('adresse') ?? $lastRegistration->adresse }}</textarea>
						<span class="help-block">{!! $errors->first('adresse', '<div class="invalid-feedback">:message</div>') !!}</span>
					</div>
			</div>
			<div class="form-group {{ $errors->has('codepostal') ? 'has-error' : '' }}">
					<label class="col-sm-3 col-form-label text-md-right" for="codepostal">Code postal *</label>
					<div class="col-md-9">
							<input type="text" class="form-control" id="codepostal" name="codepostal" value="{{ old('codepostal') ?? $lastRegistration->codepostal }}">
						<span class="help-block">{!! $errors->first('codepostal', '<div class="invalid-feedback">:message</div>') !!}</span>
					</div>
			</div>
			<div class="form-group {{ $errors->has('ville') ? 'has-error' : '' }}">
					<label class="col-sm-3 col-form-label text-md-right" for="ville">Ville *</label>
					<div class="col-md-9">
						<input type="text" class="form-control" id="ville" name="ville" value="{{ old('ville') ?? $lastRegistration->ville }}">
						<span class="help-block">{!! $errors->first('ville', '<div class="invalid-feedback">:message</div>') !!}</span>
					</div>
			</div>
			<div class="form-group {{ $errors->has('teldom') ? 'has-error' : '' }}">
					<label class="col-sm-3 col-form-label text-md-right" for="teldom">Téléphone domicile *</label>
					<div class="col-md-9">
						<input type="text" class="form-control" id="teldom" name="teldom" value="{{ old('teldom') ?? $lastRegistration->teldom }}">
						<span class="help-block">{!! $errors->first('teldom', '<div class="invalid-feedback">:message</div>') !!}</span>
					</div>
			</div>
			<div class="form-group {{ $errors->has('telburo') ? 'has-error' : '' }}">
					<label class="col-sm-3 col-form-label text-md-right" for="telburo">Téléphone bureau *</label>
					<div class="col-md-9">
						<input type="text" class="form-control" id="telburo" name="telburo" value="{{ old('telburo') ?? $lastRegistration->telburo }}">
						<span class="help-block">{!! $errors->first('telburo', '<div class="invalid-feedback">:message</div>') !!}</span>
					</div>
			</div>
			<div class="form-group {{ $errors->has('telmobile') ? 'has-error' : '' }}">
					<label class="col-sm-3 col-form-label text-md-right" for="telmobile">Téléphone portable *</label>
					<div class="col-md-9">
						<input type="text" class="form-control" id="telmobile" name="telmobile" value="{{ old('telmobile') ?? $lastRegistration->telmobile }}">
						<span class="help-block">{!! $errors->first('telmobile', '<div class="invalid-feedback">:message</div>') !!}</span>
					</div>
			</div>
			<div class="form-group {{ $errors->has('fax') ? 'has-error' : '' }}">
					<label class="col-sm-3 col-form-label text-md-right" for="fax">Fax</label>
					<div class="col-md-9">
						<input type="text" class="form-control" id="fax" name="fax" value="{{ old('fax') ?? $lastRegistration->fax }}">
						<span class="help-block">{!! $errors->first('fax', '<div class="invalid-feedback">:message</div>') !!}</span>
					</div>
			</div>
			<div class="form-group {{ $errors->has('email') ? 'has-error' : '' }}">
					<label class="col-sm-3 col-form-label text-md-right" for="email">Email *</label>
					<div class="col-md-9">
						<input type="text" class="form-control" id="email" name="email" value="{{ old('email') ?? $lastRegistration->email }}">
						<span class="help-block">{!! $errors->first('email', '<div class="invalid-feedback">:message</div>') !!}</span>
					</div>
			</div>
    </fieldset>



	<fieldset>
		<legend>Plongées</legend>

		<div class="form-group {{ $errors->has('nbdive') ? 'has-error' : '' }}">
			<label class="col-sm-3 col-form-label text-md-right" for="nbdive">Nombre de plongées *</label>
			<div class="col-md-9">
				<input type="text" class="form-control" id="nbdive" name="nbdive" value="{{ old('nbdive') }}">
				<span class="help-block">{!! $errors->first('nbdive', '<div class="invalid-feedback">:message</div>') !!}</span>
			</div>
		</div>

		<div class="form-group {{ $errors->has('dernierbrevet') ? 'has-error' : '' }}">
			<label class="col-sm-3 col-form-label text-md-right" for="dernierbrevet">Date d'obtention du dernier brevet *</label>
			<div class="col-md-9">
					<input type='text' name='dernierbrevet' value="{{ old('dernierbrevet') ?? Carbon\Carbon::createFromFormat('Y-m-d', $lastRegistration->dernierbrevet)->format('d/m/Y') }}" class="form-control" placeholder="jj/mm/aaaa"/>
				<span class="help-block">{!! $errors->first('dernierbrevet', '<div class="invalid-feedback">:message</div>') !!}</span>
			</div>
		</div>

		{{-- Niveau du plongeur --}}
		<div class="form-group {{ $errors->has('nivdiver') ? 'has-error' : '' }}">
				<label class="col-sm-3 col-form-label text-md-right" for="nivdiver">Niveau de plongeur *</label>
				  	<label>
						<input type="radio" name="nivdiver" value="Débutant" {{ (old('nivdiver') ?? $lastRegistration->nivdiver) === 'Débutant' ? 'checked' : '' }}>
						Débutant&nbsp;&nbsp;
				  	</label>
				  	<label>
						<input type="radio" name="nivdiver" value="N1" {{ (old('nivdiver') ?? $lastRegistration->nivdiver) === 'N1' ? 'checked' : '' }}>
						N1&nbsp;&nbsp;
				  	</label>
				  	<label>
						<input type="radio" name="nivdiver" value="N2" {{ (old('nivdiver') ?? $lastRegistration->nivdiver) === 'N2' ? 'checked' : '' }}>
						N2&nbsp;&nbsp;
				  	</label>
				  	<label>
						<input type="radio" name="nivdiver" value="N3" {{ (old('nivdiver') ?? $lastRegistration->nivdiver) === 'N3' ? 'checked' : '' }}>
						N3&nbsp;&nbsp;
				  	</label>
				  	<label>
						<input type="radio" name="nivdiver" value="N4" {{ (old('nivdiver') ?? $lastRegistration->nivdiver) === 'N4' ? 'checked' : '' }}>
						N4
				  	</label>
				<span class="help-block">{!! $errors->first('nivdiver', '<div class="invalid-feedback">:message</div>') !!}</span>
			</div>

		{{-- Préparation brevet --}}
		<div class="form-group {{ $errors->has('nivprepa') ? 'has-error' : '' }}">
				<label class="col-sm-3 col-form-label text-md-right" for="nivprepa">Quel brevet souhaitez vous préparer cette année *</label>
				  	<label>
						<input type="radio" name="nivprepa" value="Aucun" {{ old('nivprepa') === 'Aucun' ? 'checked' : '' }}>
						Aucun&nbsp;&nbsp;
				  	</label>
				  	<label>
						<input type="radio" name="nivprepa" value="N1" {{ old('nivprepa') === 'N1' ? 'checked' : '' }}>
						N1&nbsp;&nbsp;
				  	</label>
				  	<label>
						<input type="radio" name="nivprepa" value="N2" {{ old('nivprepa') === 'N2' ? 'checked' : '' }}>
						N2&nbsp;&nbsp;
				  	</label>
				  	<label>
						<input type="radio" name="nivprepa" value="N3" {{ old('nivprepa') === 'N3' ? 'checked' : '' }}>
						N3&nbsp;&nbsp;
				  	</label>
				  	<label>
						<input type="radio" name="nivprepa" value="N4" {{ old('nivprepa') === 'N4' ? 'checked' : '' }}>
						N4
				  	</label>
				  	<label>
						<input type="radio" name="nivprepa" value="initiateur" {{ old('nivprepa') === 'initiateur' ? 'checked' : '' }}>
						Initiateur
				  	</label>
				<span class="help-block">{!! $errors->first('nivprepa', '<div class="invalid-feedback">:message</div>') !!}</span>
			</div>
			<div style="clear: both;"></div>

			{{-- Niveau de plongeur Nitrox --}}
			<div class="form-group {{ $errors->has('nivnitrox') ? 'has-error' : '' }}">
				<label class="col-sm-3 col-form-label text-md-right" for="nivnitrox">Niveau de plongeur Nitrox</label>
				  	<label>
						<input type="radio" name="nivnitrox" value="base" {{ (old('nivnitrox') ?? $lastRegistration->nivnitrox) === 'base' ? 'checked' : '' }}>
						Base&nbsp;&nbsp;
				  	</label>
				  	<label>
						<input type="radio" name="nivnitrox" value="confirme" {{ (old('nivnitrox') ?? $lastRegistration->nivnitrox) === 'confirme' ? 'checked' : '' }}>
						Confirmé&nbsp;&nbsp;
				  	</label>
				<span class="help-block">{!! $errors->first('nivnitrox', '<div class="invalid-feedback">:message</div>') !!}</span>
			</div>
			<div style="clear: both;"></div>

			{{-- Préparation niveau Nitrox --}}
			<div class="form-group {{ $errors->has('prepanitrox') ? 'has-error' : '' }}">
				<label class="col-sm-3 col-form-label text-md-right" for="civilite">Souhaitez vous préparer l'un de ces niveaux et lequel</label>
				  	<label>
						<input type="radio" name="prepanitrox" value="base" {{ old('prepanitrox') === 'base' ? 'checked' : '' }}>
						Base&nbsp;&nbsp;
				  	</label>
				  	<label>
						<input type="radio" name="prepanitrox" value="confirme" {{ old('prepanitrox') === 'confirme' ? 'checked' : '' }}>
						Confirmé&nbsp;&nbsp;
				  	</label>
				<span class="help-block">{!! $errors->first('prepanitrox', '<div class="invalid-feedback">:message</div>') !!}</span>
			</div>
			<div style="clear: both;"></div>

		{{-- Niveau d'encadrement --}}
		<div class="form-group {{ $errors->has('nivencadr') ? 'has-error' : '' }}">
			<label class="col-sm-3 col-form-label text-md-right" for="nivencadr">Niveau d'encadrement</label>
			  <label>
				<input type="radio" name="nivencadr" value="E1" {{ (old('nivencadr') ?? $lastRegistration->nivencadr) === 'E1' ? 'checked' : '' }}>
				E1&nbsp;&nbsp;
			  </label>
			  <label>
				<input type="radio" name="nivencadr" value="E2" {{ (old('nivencadr') ?? $lastRegistration->nivencadr) === 'E2' ? 'checked' : '' }}>
				E2&nbsp;&nbsp;
			  </label>
			  <label>
				<input type="radio" name="nivencadr" value="E3" {{ (old('nivencadr') ?? $lastRegistration->nivencadr) === 'E3' ? 'checked' : '' }}>
				E3&nbsp;&nbsp;
			  </label>
			  <label>
				<input type="radio" name="nivencadr" value="E4" {{ (old('nivencadr') ?? $lastRegistration->nivencadr) === 'E4' ? 'checked' : '' }}>
				E4
			  </label>
			<span class="help-block">{!! $errors->first('nivencadr', '<div class="invalid-feedback">:message</div>') !!}</span>
		</div>
		<div style="clear: both;"></div>

		{{-- Entrainement niveeau --}}
		<div class="form-group {{ $errors->has('prepaencadr') ? 'has-error' : '' }}">
				<label class="col-sm-3 col-form-label text-md-right" for="prepaencadr">Quel groupe souhaitez vous entraîner cette année</label>
				  	<label>
						<input type="radio" name="prepaencadr" value="N1" {{ old('prepaencadr') === 'N1' ? 'checked' : '' }}>
						N1&nbsp;&nbsp;
				  	</label>
				  	<label>
						<input type="radio" name="prepaencadr" value="N2" {{ old('prepaencadr') === 'N2' ? 'checked' : '' }}>
						N2&nbsp;&nbsp;
				  	</label>
				  	<label>
						<input type="radio" name="prepaencadr" value="N3" {{ old('prepaencadr') === 'N3' ? 'checked' : '' }}>
						N3&nbsp;&nbsp;
				  	</label>
				<span class="help-block">{!! $errors->first('prepaencadr', '<div class="invalid-feedback">:message</div>') !!}</span>
			</div>

	</fieldset>


	<fieldset>
		<legend>Personne à prevenir en cas d'accident (fortement recommandé)</legend>
		<div class="form-group {{ $errors->has('nomaccident') ? 'has-error' : '' }}">
			<label class="col-sm-3 col-form-label text-md-right" for="nomaccident">Nom</label>
			<div class="col-md-9">
				<input type="text" class="form-control" id="nomaccident" name="nomaccident" value="{{ old('nomaccident') }}">
				<span class="help-block">{!! $errors->first('nomaccident', '<div class="invalid-feedback">:message</div>') !!}</span>
			</div>
		</div>
		<div class="form-group {{ $errors->has('telaccident') ? 'has-error' : '' }}">
			<label class="col-sm-3 col-form-label text-md-right" for="telaccident">Tél</label>
			<div class="col-md-9">
				<input type="text" class="form-control" id="telaccident" name="telaccident" value="{{ old('telaccident') }}">
				<span class="help-block">{!! $errors->first('telaccident', '<div class="invalid-feedback">:message</div>') !!}</span>
			</div>
		</div>
		<div class="form-group {{ $errors->has('lienaccident') ? 'has-error' : '' }}">
			<label class="col-sm-3 col-form-label text-md-right" for="lienaccident">Précisez le lien de parenté ou ami</label>
			<div class="col-md-9">
				<input type="text" class="form-control" id="lienaccident" name="lienaccident" value="{{ old('lienaccident') }}">
				<span class="help-block">{!! $errors->first('lienaccident', '<div class="invalid-feedback">:message</div>') !!}</span>
			</div>
		</div>
		<div class="form-group {{ $errors->has('adresseaccident') ? 'has-error' : '' }}">
			<label class="col-sm-3 col-form-label text-md-right" for="adresseaccident">Adresse</label>
			<div class="col-md-9">
				<textarea type="text" class="form-control" id="adresseaccident" name="adresseaccident" >{{ old('adresseaccident') }}</textarea>
				<span class="help-block">{!! $errors->first('adresseaccident', '<div class="invalid-feedback">:message</div>') !!}</span>
			</div>
		</div>

		<div class="form-group {{ $errors->has('rgpdaccident') ? 'has-error' : '' }}">
			<label class="col-sm-3 col-form-label text-md-right" for="rgpdaccident">Cette personne m'a autorisé à fournir ces informations *</label>
			  	<label>
					<input type="radio" name="rgpdaccident" value="1" {{ (old('rgpdaccident') ?? $lastRegistration->rgpdaccident) === '1' ? 'checked' : '' }}>
					Oui&nbsp;&nbsp;
			  	</label>
			  	<label>
					<input type="radio" name="rgpdaccident" value="0" {{ (old('rgpdaccident') ?? $lastRegistration->rgpdaccident) === '0' ? 'checked' : '' }}>
					Non&nbsp;&nbsp;
			  	</label>
			<span class="help-block">{!! $errors->first('civilite', '<div class="invalid-feedback">:message</div>') !!}</span>
		</div>

		<div style="clear: both;"></div>
		{{-- Allergie --}}
		<div class="form-group {{ $errors->has('prepaencadr') ? 'has-error' : '' }}">
			<label class="col-sm-3 col-form-label text-md-right" for="prepaencadr">Allergie à l'aspirine *</label>
			  <label>
				<input type="radio" name="allergieaspirine" value="1" {{ (old('allergieaspirine') ?? $lastRegistration->allergieaspirine) === '1' ? 'checked' : '' }}>
				Oui&nbsp;&nbsp;
			  </label>
			  <label>
				<input type="radio" name="allergieaspirine" value="0" {{ (old('allergieaspirine') ?? $lastRegistration->allergieaspirine) === '0' ? 'checked' : '' }}>
				Non&nbsp;&nbsp;
			  </label>
			<span class="help-block">{!! $errors->first('allergieaspirine', '<div class="invalid-feedback">:message</div>') !!}</span>
		</div>

	</fieldset>
	<br/>
	<fieldset>
		<legend>Autres informations</legend>

		<div class="form-group {{ $errors->has('anneepi') ? 'has-error' : '' }}">
			<label class="col-sm-6 col-form-label text-md-right" for="anneepi">Année de la première inscription à AQUA13 Plongée *</label>
			<div class="col-md-6">
				<input type="text" class="form-control" id="anneepi" name="anneepi" value="{{ old('anneepi') ?? $lastRegistration->anneepi }}">
				<span class="help-block">{!! $errors->first('anneepi', '<div class="invalid-feedback">:message</div>') !!}</span>
			</div>
		</div>
		<div class="form-group {{ $errors->has('anneescotisation') ? 'has-error' : '' }}">
			<label class="col-sm-6 col-form-label text-md-right" for="anneescotisation">Nombre d'années de cotisation *</label>
			<div class="col-md-6">
				<input type="text" class="form-control" id="anneescotisation" name="anneescotisation" value="{{ old('anneescotisation') }}">
				<span class="help-block">{!! $errors->first('anneescotisation', '<div class="invalid-feedback">:message</div>') !!}</span>
			</div>
		</div>
		<div class="form-group {{ $errors->has('serviceclub') ? 'has-error' : '' }}">
			<label class="col-sm-3 col-form-label text-md-right" for="serviceclub">Services que vous pouvez rendre au club ou à ses membres *</label>
			<div class="col-md-9">
				<textarea type="text" class="form-control" id="serviceclub" name="serviceclub" >{{ old('serviceclub') }}</textarea>
				<span class="help-block">{!! $errors->first('serviceclub', '<div class="invalid-feedback">:message</div>') !!}</span>
			</div>
		</div>
		<div class="form-group {{ $errors->has('remarques') ? 'has-error' : '' }}">
			<label class="col-sm-3 col-form-label text-md-right" for="remarques">Autres remarques ou suggestions</label>
			<div class="col-md-9">
				<textarea type="text" class="form-control" id="remarques" name="remarques" >{{ old('remarques') }}</textarea>
				<span class="help-block">{!! $errors->first('remarques', '<div class="invalid-feedback">:message</div>') !!}</span>
			</div>
		</div>



		<label class="col-sm-3 col-form-label text-md-right" >Sur le créneau du vendredi soir, êtes vous intéressés par</label><br/>
		<div class="col-sm-4">
			<div class="checkbox">
				<label>
					<input type="checkbox" name="interetnagelibre" value="1" {{ old('interetnagelibre') ? 'checked' : '' }}>
					Nage libre
				</label>
			</div>
			<div class="checkbox">
				<label>
					<input type="checkbox" name="interetapnee" value="1" {{ old('interetapnee') ? 'checked' : '' }}>
					Apnée
				</label>
			</div>
			<div class="checkbox">
				<label>
					<input type="checkbox" name="interetpalmes" value="1" {{ old('interetpalmes') ? 'checked' : '' }}>
					Nage avec palmes (Monopalme)
				</label>
			</div>
		</div>
		<div class="col-sm-4">
			<div class="checkbox">
				<label>
					<input type="checkbox" name="interetperfect" value="1" {{ old('interetperfect') ? 'checked' : '' }}>
					Perfectionnement
				</label>
			</div>
			<div class="checkbox">
				<label>
					<input type="checkbox" name="interetsecourisme" value="1" {{ old('interetsecourisme') ? 'checked' : '' }}>
					Secourisme
				</label>
			</div>
		</div>
		<div style="clear: both;"></div>

		<div class="form-group {{ $errors->has('certificat') ? 'has-error' : '' }}">
			<label class="col-sm-3 col-form-label text-md-right" for="certificat">Certificat Médicale (jpg, pdf)</label>
			<div class="col-md-9">
				<input type="file" id="certificat" name="certificat" accept="image/*,application/pdf">
				<span class="help-block">{!! $errors->first('certificat', '<div class="invalid-feedback">:message</div>') !!}</span>
			</div>
		</div>

	</fieldset>
	<div class="form-group {{ $errors->has('reglementok') ? 'has-error' : '' }}">
			<label class="col-sm-3 col-form-label text-md-right" for="reglementok"></label>
			  <label>
				<input type="radio" name="reglementok" value="1">
				J'ai lu et reconnais le règlement intérieur du club aqua13 Plongée, ainsi que les mentions légales.
				J'accepte que les données saisies dans ce formulaire soient utilisées pour la gestion de votre dossier au sein du club.
			  </label>
			<span class="help-block">{!! $errors->first('reglementok', '<div class="invalid-feedback">:message</div>') !!}</span>
	</div>
	<br/>
    <input type='submit' name='ok' value='Envoyer' class="submit"/><br/><br/>
        (* champs obligatoires)

</form>



@endsection

@section('js')
<!-- AdminLTE App -->
{{-- <script src="{{ asset('js/app.min.js') }}"></script> --}}
<script src="{{ asset('js/daterangepicker.min.js') }}"></script>
<script src="{{ asset('js/jquery.inputmask.min.js') }}"></script>
{{-- <script src="{{ asset('js/plugins/datepicker/bootstrap-datepicker.js') }}"></script> --}}
<script>
	$( function() {
		$( "input[name='datenaissance'], input[name='dernierbrevet']" ).inputmask({ "mask": "99/99/9999" });
		$( "input[name='teldom'], input[name='telburo'], input[name='telmobile'], input[name='fax'], input[name='telaccident']" ).inputmask({ "mask": "99 99 99 99 99" });
		$( "input[name='rgpdaccident']" ).on('click', function (e) {
                if ($(this).val() == 0) {
					$( "input[name='nomaccident']" ).val('');
					$( "input[name='telaccident']" ).val('');
					$( "input[name='lienaccident']" ).val('');
					$( "textarea[name='adresseaccident']" ).val('');
                }
        })
	} );
</script>
@endsection