<?php
use App\Services\Galerie\Galerie;
/*
 *  @author  Florent Bonenfant <social1@niweb.eu>
 *  @version 1.0
 */

// dd($article);
?>
@extends('front.layouts.app')

@section('content')
<div id="chemin">{{ $article->titre }}</div>

<?php
        $article->texte = preg_replace_callback(
                '`\<galerie(.*)\>`isU',
            function ($id) {
                return Galerie::getHtml($id[1]);

            },
            $article->texte
        );

?>
{!! $article->texte !!}
@endsection
@include('front.layouts.galerie')
@section('css')
        <style>
            .galerie_photos li {
                list-style: none;
                display: inline;
            }

            .galerie_photos img {
                width: 200px;
            }
        </style>
@endsection
