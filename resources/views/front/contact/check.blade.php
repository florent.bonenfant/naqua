@extends('front.layouts.app')

@section('content')

<div id="chemin">Contact</div>

@if ($dejaEnvoye)
    {{ __('contact.erreur_mail_deja_envoye') }}
@else
    {{ __('contact.message_mail_envoye') }}
@endif

@endsection