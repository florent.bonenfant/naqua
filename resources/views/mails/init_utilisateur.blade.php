<!DOCTYPE html>
<html lang="fr">
  <head>
    <meta charset="utf-8">
  </head>
  <body>
    <h2>Salut à toi {{ ucfirst($user->prenom) }},</h2>
    <p>Tu reçois ce mail, car le site à subi certaines évolutions entrainant la réinitialisation de ton mot de passe, pour des raisons de sécurités nous avons créé un nouveau mot de passe aléatoire crypter que nous vous invitons à réinitialiser en utilisant l'adresse mail : {{ $user->email }}.
Afin de te faciliter la tâche tu peux suivre l'hyperlien suivant : <a href="{{ route('password.request') }}" target="_blank">{{ route('password.request') }}</a>.<br />
Si tu n'as pas confiance je t'invite à rejoindre ton site de plongée préféré en suivant les conseils suivants :</p>
    <ul>
      <li>Dirige-toi en haut à droite du site</li>
      <li>Clique sur le bouton "Login"</li>
      <li>Clique maintenant sur le bouton "Forgot Your Password"</li>
      <li>Et te voici arrivé à l'url indiquée ci-dessus</li>
    </ul>
      <p>Tu rentres ton mail et tu cliques sur le bouton tu recevras ainsi un mail avec l'ensemble des instructions nécessaires.<br /><br />
      Je te souhaite de bonnes bulles.
      </p>
  </body>
</html>








