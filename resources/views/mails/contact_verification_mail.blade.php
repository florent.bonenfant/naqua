<!DOCTYPE html>
<html lang="fr">
  <head>
    <meta charset="utf-8">
  </head>
  <body>
    <h2>Bonjour {{ $user->prenom ?? $user->nom }},</h2>
    <p>Ceci est un mail afin de vérifier que vous n'êtes pas un robot qui a pour hobbie l'envoi de spam.</p>
    <p>Afin de finaliser l'envoi de votre message à notre équipe merci de bien vouloir cliquer sur le lien ci-dessous.</p>
    <br />
    <p>Valider l'envoi du message : <a href="{{ route('contact.check', [$user->contact_id, $user->token]) }}">{{ route('contact.check', [$user->contact_id, $user->token]) }}</a></p>

    <p>Ceci est un courriel automatique merci de ne pas y répondre.</p>
    <p>L'équipe d'Aqua13 Plongée</p>
  </body>
</html>








