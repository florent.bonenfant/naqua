<?php
/*
 *  
 *  
 *  @author  Florent Bonenfant <social1@niweb.eu>
 *  @version 1.0
 */

?>
@extends('back.layouts.app')

@section('content')

<section class="content-header">
    <h1>
        Liste des documents
    </h1>
    <ol class="breadcrumb">
        <li><a href="{{ route('admin.back.home')}}"><i class="fa fa-dashboard"></i> {{__('back_common.ariane.home')}}</a></li>
        <li>{{__('back_common.ariane.doc')}}</li>
    </ol>
</section>

<!-- Main content -->
<section class="content">
    <div class="row">
        <div class="col-xs-12">

            <div class="box">
                <table id="documents" class="table table-bordered table-hover">
                    <thead>
                        <tr>
                            <th>#</th>
                            <th>Titre</th>
                            <th>Type</th>
                            <th>Poids</th>
                            <th>Date</th>
                            <th></th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($documents as $document)
                        <tr>
                            <td>{{ $document->id_document }}</td>
                            <td><a href="{{asset('documents/'.$document->fichier)}}" target="_blank">@if($document->titre != '') {{ $document->titre }} @else {{ substr(strstr($document->fichier, '/'),1)}} @endif</a></td>
                            <td>{{ $document->mode }}</td>
                            <td>{{ number_format($document->taille / 1024,2) }} ko</td>
                            <td>{{ \Carbon\Carbon::parse($document->maj)->format('d/m/Y H:i') }}</td>
                            <td>
                                @can('gate-author')
                                    <button class="btn btn-danger btn-sm" value="{{ $document->id_document }}"><i class="fa fa-trash-o"></i></button>
                                @endcan
                            </td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
                <!--                <div class="box-header">
                                    <h3 class="box-title">Liste des articles</h3>
                                </div> /.box-header -->
                <div class="box-body">

                </div><!-- /.box -->
            </div><!-- /.box -->

        </div><!-- /.col -->
    </div><!-- /.row -->
</section><!-- /.content -->



@endsection

@section('js')

<!-- DataTables -->
<script src="{{ asset('js/jquery.dataTables.min.js') }}"></script>
<script src="{{ asset('js/dataTables.bootstrap.min.js') }}"></script>
<!-- SlimScroll -->
<script src="{{ asset('js/jquery.slimscroll.min.js') }}"></script>

<script>
$(function () {
//$("#example1").DataTable();
    $('#documents').DataTable({
        "paging": true,
        "lengthChange": false,
        "searching": true,
        "ordering": false,
        "info": true,
        "autoWidth": true,
        "pageLength": 15,
//        "order": [[4, "desc"]],

        "language":
                {
                    "sProcessing": "Traitement en cours...",
                    "sSearch": "Rechercher&nbsp;:",
                    "sLengthMenu": "Afficher _MENU_ &eacute;l&eacute;ments",
                    "sInfo": "Affichage de l'&eacute;l&eacute;ment _START_ &agrave; _END_ sur _TOTAL_ &eacute;l&eacute;ments",
                    "sInfoEmpty": "Affichage de l'&eacute;l&eacute;ment 0 &agrave; 0 sur 0 &eacute;l&eacute;ment",
                    "sInfoFiltered": "(filtr&eacute; de _MAX_ &eacute;l&eacute;ments au total)",
                    "sInfoPostFix": "",
                    "sLoadingRecords": "Chargement en cours...",
                    "sZeroRecords": "Aucun &eacute;l&eacute;ment &agrave; afficher",
                    "sEmptyTable": "Aucune donn&eacute;e disponible dans le tableau",
                    "oPaginate": {
                        "sFirst": "Premier",
                        "sPrevious": "Pr&eacute;c&eacute;dent",
                        "sNext": "Suivant",
                        "sLast": "Dernier"
                    },
                    "oAria": {
                        "sSortAscending": ": activer pour trier la colonne par ordre croissant",
                        "sSortDescending": ": activer pour trier la colonne par ordre d&eacute;croissant"
                    }
                }

    });

    $('button.btn-danger').click(function(e) {
        swalConfirmDelete("{{ route('admin.file.index') }}" + "/" + $(this).attr('value'), e);
    });

});
</script>

@endsection
@section('css')
<!-- DataTables -->
<link rel="stylesheet" href="{{ asset('css/dataTables.bootstrap.min.css') }}">
@endsection
