<?php

/*
 *  @author  Florent Bonenfant <social1@niweb.eu>
 *  @version 1.0
 */

 $user = isAuthor();
?>
  <!DOCTYPE html>
  <html>

  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>{{ __('back_common.left_menu_title_back')}}</title>
    <!-- Tell the browser to be responsive to screen width -->
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <!-- Bootstrap 3.3.5 -->

    {{-- <link href="{{ asset('css/styles.min.css') }}" rel="stylesheet" media="projection, screen, tv" > --}}
    <link rel="stylesheet" href="{{ asset('css/bootstrap.min.css') }}">
    <!-- Font Awesome -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">
    <!-- Ionicons -->
    <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">

    <link href="{{ asset('css/sweetalert2.min.css') }}" rel="stylesheet">
    <!-- jvectormap -->
    <!--<link rel="stylesheet" href="{{ asset('js/plugins/jvectormap/jquery-jvectormap-1.2.2.css') }}">-->
    <!-- Theme style -->
    <link rel="stylesheet" href="{{ asset('css/AdminLTE.min.css') }}">
    <!-- AdminLTE Skins. Choose a skin from the css/skins
         folder instead of downloading all of them to reduce the load. -->
    <!--<link rel="stylesheet" href="{{ asset('css/skins/skin-red.min.css')}}">-->
    <link rel="stylesheet" href="{{ asset('css/_all-skins.min.css')}}"> @yield('css')
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
        <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
  </head>

  <body class="hold-transition skin-blue sidebar-mini">
    <div class="wrapper">

      <header class="main-header">

        <!-- Logo -->
      <a href="{{ route('accueil.index')}}" class="logo">
          <!-- mini logo for sidebar mini 50x50 pixels -->
          <span class="logo-mini"><b>A</b>13</span>
          <!-- logo for regular state and mobile devices -->
          <span class="logo-lg"><b>Aqua 13</b></span>
        </a>

        <!-- Header Navbar: style can be found in header.less -->
        <nav class="navbar navbar-static-top" role="navigation">
          <!-- Sidebar toggle button-->
          <a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
            <span class="sr-only">Toggle navigation</span>
          </a>
          <!-- Navbar Right Menu -->
          <div class="navbar-custom-menu">
            <ul class="nav navbar-nav">
              <!-- Messages: style can be found in dropdown.less-->
              {{-- <li class="dropdown messages-menu">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                  <i class="fa fa-envelope-o"></i>
                  <span class="label label-success">4</span>
                </a>
                <ul class="dropdown-menu">
                  <li class="header">You have 4 messages</li>
                  <li>
                    <!-- inner menu: contains the actual data -->
                    <ul class="menu">
                      <li>
                        <!-- start message -->
                        <a href="#">
                          <div class="pull-left">
                            <img src="{{ asset('ui/user2-160x160.jpg') }}" class="img-circle" alt="User Image">
                          </div>
                          <h4>
                            Support Team
                            <small><i class="fa fa-clock-o"></i> 5 mins</small>
                          </h4>
                          <p>Why not buy a new awesome theme?</p>
                        </a>
                      </li>
                      <!-- end message -->
                      <li>
                        <a href="#">
                          <div class="pull-left">
                            <img src="{{ asset('ui/user3-128x128.jpg') }}" class="img-circle" alt="User Image">
                          </div>
                          <h4>
                            AdminLTE Design Team
                            <small><i class="fa fa-clock-o"></i> 2 hours</small>
                          </h4>
                          <p>Why not buy a new awesome theme?</p>
                        </a>
                      </li>
                      <li>
                        <a href="#">
                          <div class="pull-left">
                            <img src="{{ asset('ui/user4-128x128.jpg') }}" class="img-circle" alt="User Image">
                          </div>
                          <h4>
                            Developers
                            <small><i class="fa fa-clock-o"></i> Today</small>
                          </h4>
                          <p>Why not buy a new awesome theme?</p>
                        </a>
                      </li>
                      <li>
                        <a href="#">
                          <div class="pull-left">
                            <img src="{{ asset('ui/user3-128x128.jpg') }}" class="img-circle" alt="User Image">
                          </div>
                          <h4>
                            Sales Department
                            <small><i class="fa fa-clock-o"></i> Yesterday</small>
                          </h4>
                          <p>Why not buy a new awesome theme?</p>
                        </a>
                      </li>
                      <li>
                        <a href="#">
                          <div class="pull-left">
                            <img src="{{ asset('ui/user4-128x128.jpg') }}" class="img-circle" alt="User Image">
                          </div>
                          <h4>
                            Reviewers
                            <small><i class="fa fa-clock-o"></i> 2 days</small>
                          </h4>
                          <p>Why not buy a new awesome theme?</p>
                        </a>
                      </li>
                    </ul>
                  </li>
                  <li class="footer"><a href="#">See All Messages</a></li>
                </ul>
              </li> --}}
              <!-- Notifications: style can be found in dropdown.less -->
              {{-- <li class="dropdown notifications-menu">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                  <i class="fa fa-bell-o"></i>
                  <span class="label label-warning">10</span>
                </a>
                <ul class="dropdown-menu">
                  <li class="header">You have 10 notifications</li>
                  <li>
                    <!-- inner menu: contains the actual data -->
                    <ul class="menu">
                      <li>
                        <a href="#">
                          <i class="fa fa-users text-aqua"></i> 5 new members joined today
                        </a>
                      </li>
                      <li>
                        <a href="#">
                          <i class="fa fa-warning text-yellow"></i> Very long description here that may not fit into the page and may cause design problems
                        </a>
                      </li>
                      <li>
                        <a href="#">
                          <i class="fa fa-users text-red"></i> 5 new members joined
                        </a>
                      </li>
                      <li>
                        <a href="#">
                          <i class="fa fa-shopping-cart text-green"></i> 25 sales made
                        </a>
                      </li>
                      <li>
                        <a href="#">
                          <i class="fa fa-user text-red"></i> You changed your username
                        </a>
                      </li>
                    </ul>
                  </li>
                  <li class="footer"><a href="#">View all</a></li>
                </ul>
              </li> --}}
              <!-- Tasks: style can be found in dropdown.less -->
              {{-- <li class="dropdown tasks-menu">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                  <i class="fa fa-flag-o"></i>
                  <span class="label label-danger">9</span>
                </a>
                <ul class="dropdown-menu">
                  <li class="header">You have 9 tasks</li>
                  <li>
                    <!-- inner menu: contains the actual data -->
                    <ul class="menu">
                      <li>
                        <!-- Task item -->
                        <a href="#">
                          <h3>
                            Design some buttons
                            <small class="pull-right">20%</small>
                          </h3>
                          <div class="progress xs">
                            <div class="progress-bar progress-bar-aqua" style="width: 20%" role="progressbar" aria-valuenow="20" aria-valuemin="0" aria-valuemax="100">
                              <span class="sr-only">20% Complete</span>
                            </div>
                          </div>
                        </a>
                      </li>
                      <!-- end task item -->
                      <li>
                        <!-- Task item -->
                        <a href="#">
                          <h3>
                            Create a nice theme
                            <small class="pull-right">40%</small>
                          </h3>
                          <div class="progress xs">
                            <div class="progress-bar progress-bar-green" style="width: 40%" role="progressbar" aria-valuenow="20" aria-valuemin="0" aria-valuemax="100">
                              <span class="sr-only">40% Complete</span>
                            </div>
                          </div>
                        </a>
                      </li>
                      <!-- end task item -->
                      <li>
                        <!-- Task item -->
                        <a href="#">
                          <h3>
                            Some task I need to do
                            <small class="pull-right">60%</small>
                          </h3>
                          <div class="progress xs">
                            <div class="progress-bar progress-bar-red" style="width: 60%" role="progressbar" aria-valuenow="20" aria-valuemin="0" aria-valuemax="100">
                              <span class="sr-only">60% Complete</span>
                            </div>
                          </div>
                        </a>
                      </li>
                      <!-- end task item -->
                      <li>
                        <!-- Task item -->
                        <a href="#">
                          <h3>
                            Make beautiful transitions
                            <small class="pull-right">80%</small>
                          </h3>
                          <div class="progress xs">
                            <div class="progress-bar progress-bar-yellow" style="width: 80%" role="progressbar" aria-valuenow="20" aria-valuemin="0"
                              aria-valuemax="100">
                              <span class="sr-only">80% Complete</span>
                            </div>
                          </div>
                        </a>
                      </li>
                      <!-- end task item -->
                    </ul>
                  </li>
                  <li class="footer">
                    <a href="#">View all tasks</a>
                  </li>
                </ul>
              </li> --}}
              <!-- User Account: style can be found in dropdown.less -->
              <li class="dropdown user user-menu">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                  <img src="{{ asset(DISK_PATH_AVATARS . 'auton'. $user->id.'.jpg') }}" class="user-image" alt="Votre avatar">
                <span class="hidden-xs">{{  $user->name }}</span>
                </a>
                <ul class="dropdown-menu">
                  <!-- User image -->
                  <li class="user-header">
                    <img src="{{ asset(DISK_PATH_AVATARS . 'auton'. $user->id.'.jpg') }}" class="img-circle" alt="User Image">
                    <p>
                      {{ $user->name }}
                    </p>
                  </li>
                  <!-- Menu Body -->
                  {{-- <li class="user-body">
                    <div class="col-xs-4 text-center">
                      <a href="#">Followers</a>
                    </div>
                    <div class="col-xs-4 text-center">
                      <a href="#">Sales</a>
                    </div>
                    <div class="col-xs-4 text-center">
                      <a href="#">Friends</a>
                    </div>
                  </li> --}}
                  <!-- Menu Footer-->
                  <li class="user-footer">
                    // @todo création d'un profil user
                    {{-- <div class="pull-left">
                      <a href="#" class="btn btn-default btn-flat">Profile</a>
                    </div> --}}
                    <div class="pull-right">
                    <a href="{{ route('logout') }}" class="btn btn-default btn-flat">{{ __('back_common.left_menu_offline')}}</a>
                    </div>
                  </li>
                </ul>
              </li>
              <!-- Control Sidebar Toggle Button -->
              <li>
                <a href="#" data-toggle="control-sidebar"><i class="fa fa-gears"></i></a>
              </li>
            </ul>
          </div>

        </nav>
      </header>
      <!-- Left side column. contains the logo and sidebar -->
      <aside class="main-sidebar">
        <!-- sidebar: style can be found in sidebar.less -->
        <section class="sidebar">
          <!-- Sidebar user panel -->
          <div class="user-panel">
            <div class="pull-left image">
              <img src="{{ asset(DISK_PATH_AVATARS . 'auton'. $user->id.'.jpg') }}" class="img-circle" alt="Avatar">
            </div>
            <div class="pull-left info">
              <p>{{ $user->name }}</p>
              <a href="#"><i class="fa fa-circle text-success"></i> {{ __('back_common.left_menu_online')}}</a>
            </div>
          </div>
          <!-- search form -->
          {{-- <form action="#" method="get" class="sidebar-form">
            <div class="input-group">
              <input type="text" name="q" class="form-control" placeholder="Search...">
              <span class="input-group-btn">
                <button type="submit" name="search" id="search-btn" class="btn btn-flat"><i class="fa fa-search"></i></button>
              </span>
            </div>
          </form> --}}
          <!-- /.search form -->
          <!-- sidebar menu: : style can be found in sidebar.less -->
          <ul class="sidebar-menu">
            <li class="header">Administration du site</li>
            {{-- <li class="active treeview">
              <a href="#">
                <i class="fa fa-dashboard"></i> <span>Dashboard</span>
              </a>

            </li> --}}
            <li>
              <a href="pages/widgets.html">
                <a href="{{ route('admin.evenement.index') }}"><i class="fa fa-plane"></i>  {{ __('back_common.left_menu_events')}}</a>
              </a>
            </li>
            <li class="treeview">
              <a href="#">
                <i class="fa fa-folder-open-o"></i> <span> {{ __('back_common.left_menu_category')}}</span> <i class="fa fa-angle-left pull-right"></i>
              </a>
              <ul class="treeview-menu">
                <li><a href="{{ route('admin.category.index') }}"><i class="fa fa-folder-o"></i> {{ __('back_common.left_menu_category')}}</a></li>
                <li class="active"><a href="{{ route('admin.article.my') }}"><i class="fa fa-edit"></i> {{ __('back_common.left_menu_my_article')}}</a></li>
                <li class="active"><a href="{{ route('admin.article.index') }}"><i class="fa fa-edit"></i> {{ __('back_common.left_menu_article')}}</a></li>
              </ul>
            </li>
            <li>
              <a href="{{ route('admin.file.index') }}">
                <i class="fa fa-th"></i> <span>{{ __('back_common.left_menu_doc')}}</span>
              </a>
            </li>
            {{-- <li>
              <a href="pages/widgets.html">
                <i class="fa fa-comments-o"></i> <span>Forum ?</span> <small class="label pull-right bg-green">Voir si implémenté</small>
              </a>
            </li> --}}
            <li>
              <a href="pages/widgets.html">
                <s><i class="fa fa-image"></i> <span>{{ __('back_common.left_menu_pictures')}}</span></s>
              </a>
            </li>
            <li>
              <a href="{{ route('admin.registration.index') }}">
                <i class="fa fa-male"></i> <span>{{ __('back_common.left_menu_registers')}}</span>
              </a>
            </li>
            <li class="treeview">
              <a href="#">
                <s><i class="fa fa-group"></i> <span>{{ __('back_common.left_menu_divers')}}</span></s>
              </a>
            </li>
            {{-- <li class="header">LABELS</li>
            <li><a href="#"><i class="fa fa-circle-o text-red"></i> <span>Important</span></a></li>
            <li><a href="#"><i class="fa fa-circle-o text-yellow"></i> <span>Warning</span></a></li>
            <li><a href="#"><i class="fa fa-circle-o text-aqua"></i> <span>Information</span></a></li> --}}
          </ul>
        </section>
        <!-- /.sidebar -->
      </aside>

      <!-- Content Wrapper. Contains page content -->
      <div class="content-wrapper">
        @if (session('messageSuccess') && session('messageError') == '')
        <div class="alert alert-success alert-dismissable">
          <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
          <h4> <i class="icon fa fa-check"></i> Infos</h4>
          {{ session('messageSuccess') }}
        </div>

        @endif @if (session('messageError'))
        <div class="alert alert-danger alert-dismissable">
          <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
          <h4><i class="icon fa fa-ban"></i> Alert!</h4>
          {{ session('messageError') }}
        </div>

        @endif @yield('content')


      </div>
      <!-- /.content-wrapper -->

      <footer class="main-footer">
        <div class="pull-right hidden-xs">
          <b>Version</b> 2.3.0
        </div>
        <strong>Copyright &copy; 2014-2015 <a href="http://almsaeedstudio.com">Almsaeed Studio</a>.</strong> All rights reserved.
      </footer>


      <!-- Add the sidebar's background. This div must be placed
           immediately after the control sidebar -->
      <div class="control-sidebar-bg"></div>

    </div>
    <!-- ./wrapper -->


    <!-- jQuery 2.1.4 -->
    <script src="{{ asset('js/2.1.4.jQuery.min.js') }}"></script>
    {{-- <script src="{{ asset('js/plugins/jQuery/jQuery-2.1.4.min.js') }}"></script> --}}

    <!-- Bootstrap 3.3.5 -->
    <script src="{{ asset('js/bootstrap.min.js') }}"></script>
    <!-- FastClick -->
    <script src="{{ asset('js/fastclick.min.js') }}"></script>
    {{-- <script src="{{ asset('js/plugins/fastclick/fastclick.min.js') }}"></script> --}}
    <!-- AdminLTE App -->
    <script src="{{ asset('js/adminlte.min.js') }}"></script>
    <!-- Sparkline -->
    <script src="{{ asset('js/jquery.sparkline.min.js') }}"></script>
    {{-- <script src="{{ asset('js/plugins/sparkline/jquery.sparkline.min.js') }}"></script> --}}
    <!-- jvectormap -->
    <!--    <script src="{{ asset('js/plugins/jvectormap/jquery-jvectormap-1.2.2.min.js') }}"></script>
    <script src="{{ asset('js/plugins/jvectormap/jquery-jvectormap-world-mill-en.js') }}"></script>-->

    <!-- SlimScroll 1.3.0 -->
    <script src="{{ asset('js/jquery.slimscroll.min.js') }}"></script>
    {{-- <script src="{{ asset('js/plugins/slimScroll/jquery.slimscroll.min.js') }}"></script> --}}
    <!-- ChartJS 1.0.1 -->
    <script src="{{ asset('js/Chart.min.js') }}"></script>
    {{-- <script src="{{ asset('js/plugins/chartjs/Chart.min.js') }}"></script> --}}
    <script src="{{ asset('js/nicEdit.min.js') }}"></script>
    <script src="{{ asset('js/sweetalert2.all.min.js') }}"></script>
    <!-- AdminLTE dashboard demo (This is only for demo purposes) -->

    <!--<script src="{{ asset('js/dashboard2.js') }}"></script>-->
    <!-- AdminLTE for demo purposes -->

    {{-- <script src="{{ asset('js/demo.js') }}"></script> --}}
    <script>
      var swalConfirmDelete = function(url, event) {
        swal({
                    title: "{{ __('common.question_confirmation') }}",
                    text: "{{ __('common.question_suppression') }}",
                    icon: "warning",
                    buttons: true,
                    dangerMode: true,
                })
                        .then((willDelete) => {
                            if (willDelete) {
                                let icon = "info";
                                $.ajax({
                                    url: url,
                                    headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
                                    type: 'DELETE',
                                    dataType: "json",
                                    data: {id: $(this).attr('value')},

                                    success: function (data, textStatus, jqXHR) {
                                        let msg = "";
                                        if(data) {
                                            msg = "{{ __('common.message_suppression') }}";
                                            icon = "success"
                                            $( event.target ).closest( "tr" ).remove();
                                        } else {
                                            msg = "{{ __('common.erreur_detection') }}";
                                            icon = "warning";
                                        }
                                        swal(msg, {
                                            icon: icon,
                                        });
                                    },
                                    error: function (jqXHR, textStatus, errorThrown) {
                                        swal("{{ __('common.exception_ajax_serveur') }}", {
                                            icon: "error",
                                        })
                                    }

                                }).done(function (data) { })
                                  .fail(function (data) { });
                                ;
                            }
                        });

                      event.preventDefault();
    }
    </script>
    @yield('js')
  </body>

  </html>