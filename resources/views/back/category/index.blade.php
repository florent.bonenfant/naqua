<?php
/*
 *  
 *  
 *  @author  Florent Bonenfant <social1@niweb.eu>
 *  @version 1.0
 */
?>
@extends('back.layouts.app')

@section('content')

<section class="content-header">
    <h1>
        {{__('back_categorie.titre.index')}}
    </h1>
    <ol class="breadcrumb">
        <li><a href="{{ route('admin.back.home')}}"><i class="fa fa-dashboard"></i> {{__('back_common.ariane.home')}}</a></li>
        <li>{{__('back_common.ariane.category')}}</li>
    </ol>
</section>

<!-- Main content -->
<section class="content">
    <div class="row">
        <div class="col-xs-12">

            <div class="box">
                <!--                <div class="box-header">
                                    <h3 class="box-title">Liste des articles</h3>
                                </div> /.box-header -->
                <div class="box-body">
                    <?php
                    $couleurs = ['primary', 'info', 'success', 'warning', 'danger', 'teal',];
                    $couleurId = 0;
                    ?>
                    @foreach($category as $categorie)
                    @if($categorie->id_rubrique == $categorie->id_secteur)
                    <div class="col-md-4">
                        <!-- DIRECT CHAT PRIMARY -->
                        <div class="box box-{!! $couleurs[$couleurId] !!} direct-chat direct-chat-primary" >
                            <div class="box-header with-border bg-{!! $couleurs[$couleurId] !!}">
                                <h3 class="box-title">{{ $categorie->titre }}</h3>
                                <!--<h5 class="widget-user-desc">{{ $categorie->texte }}</h5>-->
                                <div class="box-tools pull-right">
                                    <!--<span data-toggle="tooltip" title="" class="badge bg-gray" data-original-title="3 New Messages">3</span>-->
                                    <button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                                    <button class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
                                </div>
                            </div><!-- /.box-header -->

                            <div class="box-body" style="min-height:275px;">
                                <table class="table table-striped">
                                    <thead>
                                    <th>#</th>
                                    <th>{{__('back_categorie.list.categorie')}}</th>
                                    <th>{{__('back_categorie.list.etat')}}</th>
                                    <th>{{__('back_common.action')}}</th>
                                    </thead>
                                    <tbody>
                                        @foreach($category as $ssCategorie)
                                            @if($ssCategorie->id_parent == $categorie->id_rubrique)
                                            <tr>
                                                <td>{{$ssCategorie->id_rubrique}}</td>
                                                <td><a href="{{ route('admin.category.edit', $ssCategorie->id_rubrique) }}" title="{{$ssCategorie->titre}}">{{$ssCategorie->titre}}</a></td>
                                                <td>{{$ssCategorie->statut}}</td>
                                                <td>
                                                    <a href="{{ route('admin.category.edit', $ssCategorie->id_rubrique) }}" target="_self"><button class="btn btn-default btn-sm"><i class="fa fa-pencil"></i></button></a>
                                                    @can('gate-admin')
                                                        <button class="btn btn-danger btn-sm" value="{{ $ssCategorie->id_rubrique }}"><i class="fa fa-trash-o"></i></button>
                                                    @endcan
                                                </td>
                                            </tr>
                                            @endif
                                        @endforeach
                                    </tbody>
                                </table>
                            </div><!-- /.box-body -->
                            <div class="box-footer">
                                <form action="{{ route('admin.category.addChildren') }}" method="post">
                                    {{ csrf_field() }}
                                    <div class="input-group">
                                        <input name="category_{{$categorie->id_rubrique}}" placeholder="Nouvelle sous catégorie" class="form-control" type="text">
                                        <span class="input-group-btn">
                                            <button type="submit" class="btn btn-{!! $couleurs[$couleurId]; $couleurId = ($couleurId === count($couleurs) - 1 ? 0 : $couleurId + 1); !!} btn-flat">Enregistrer</button>
                                        </span>
                                    </div>
                                </form>
                            </div><!-- /.box-footer-->
                        </div><!--/.direct-chat -->
                    </div><!-- /.col -->

{{-- {{ next($couleurs) }} --}}
                    @endif
                    @endforeach



<!--                    <table id="articles" class="table table-bordered table-hover">
                        <thead>
                            <tr>
                                <th>#</th>
                                <th>Catégorie</th>
                                <th>Parent</th>
                                <th>Etat</th>
                                <th>Date</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($category as $categorie)
                            <tr>
                                <td>{{ $categorie->id_rubrique }}</td>
                                <td>{{ $categorie->titre }}</td>
                                <td>{{ $categorie->id_parent }}</td>
                                <td>{{ $categorie->statut }}</td>
                                <td>{-{ $article->date }}</td>
                            </tr>
                            @endforeach
                            </tfoot>
                    </table>-->
                </div><!-- /.box-body -->
                @can('gate-admin')
                <div class="box-body">
                    <form action="{{ route('admin.category.create') }}" method="post">
                        {{ csrf_field() }}
                        <label>{{__('back_categorie.list.ajouter_categorie')}}</label>
                        <div class="input-group">
                            <input name="category_racine" placeholder="Nouvelle catégorie" class="form-control" type="text">
                            <span class="input-group-btn">
                                <button type="submit" class="btn btn-primary btn-flat" type="submit">{{__('common.enregistrer')}}</button>
                            </span>
                        </div>
                    </form>
                </div><!-- /.box -->
                @endcan
            </div><!-- /.box -->

        </div><!-- /.col -->
    </div><!-- /.row -->
</section><!-- /.content -->



@endsection

@section('js')

<!-- DataTables -->
<script src="{{ asset('js/jquery.dataTables.min.js') }}"></script>
<script src="{{ asset('js/dataTables.bootstrap.min.js') }}"></script>
<!-- SlimScroll -->
<script src="{{ asset('js/jquery.slimscroll.min.js') }}"></script>

<script>
$(function () {
//$("#example1").DataTable();
    $('#articles').DataTable({
        "paging": true,
        "lengthChange": false,
        "searching": true,
        "ordering": true,
        "info": true,
        "autoWidth": true,
        "pageLength": 15,
        "order": [[4, "desc"]],
//    "lengthMenu": [ [20, 35, 50, -1], [20, 35, 50, "All"] ]

        "language":
                {
                    "sProcessing": "Traitement en cours...",
                    "sSearch": "Rechercher&nbsp;:",
                    "sLengthMenu": "Afficher _MENU_ &eacute;l&eacute;ments",
                    "sInfo": "Affichage de l'&eacute;l&eacute;ment _START_ &agrave; _END_ sur _TOTAL_ &eacute;l&eacute;ments",
                    "sInfoEmpty": "Affichage de l'&eacute;l&eacute;ment 0 &agrave; 0 sur 0 &eacute;l&eacute;ment",
                    "sInfoFiltered": "(filtr&eacute; de _MAX_ &eacute;l&eacute;ments au total)",
                    "sInfoPostFix": "",
                    "sLoadingRecords": "Chargement en cours...",
                    "sZeroRecords": "Aucun &eacute;l&eacute;ment &agrave; afficher",
                    "sEmptyTable": "Aucune donn&eacute;e disponible dans le tableau",
                    "oPaginate": {
                        "sFirst": "Premier",
                        "sPrevious": "Pr&eacute;c&eacute;dent",
                        "sNext": "Suivant",
                        "sLast": "Dernier"
                    },
                    "oAria": {
                        "sSortAscending": ": activer pour trier la colonne par ordre croissant",
                        "sSortDescending": ": activer pour trier la colonne par ordre d&eacute;croissant"
                    }
                }

    });
    $('button.btn-danger').click(function(e) {
        swalConfirmDelete("{{ route('admin.category.index') }}" + "/" + $(this).attr('value'), e);
    });
});
</script>

@endsection
@section('css')
<!-- DataTables -->
<link rel="stylesheet" href="{{ asset('css/dataTables.bootstrap.min.css') }}">
@endsection
