<?php
use App\Models\Urls;
/*
 *  @author  Florent Bonenfant <social1@niweb.eu>
 *  @version 1.0
 */
?>
@extends('back.layouts.app')

@section('content')

<section class="content-header">
    <h1>
        {{__('back_categorie.titre.edit', ['titre' => $category->titre])}}
    </h1>
    <ol class="breadcrumb">
        <li><a href="{{ route('admin.back.home')}}"><i class="fa fa-dashboard"></i> {{__('back_common.ariane.home')}}</a></li>
        <li><a href="{{ route('admin.category.index')}}">{{__('back_common.ariane.category')}}</a></li>
        <li class="active">{{__('back_common.ariane.article')}}</li>
    </ol>
</section>

<!-- Main content -->
<section class="content">
    <div class="row">
        <div class="col-xs-12">

            <div class="box">
                <!--                <div class="box-header">
                                    <h3 class="box-title">Liste des articles</h3>
                                </div> /.box-header -->
                <div class="box-body">




                    <div class="col-md-3">
                        <div class="box box-info direct-chat direct-chat-info" >
                            <div class="box-header with-border bg-info">
                                <h3 class="box-title">{{__('back_common.ariane.doc')}}</h3>
                                <!--<h5 class="widget-user-desc">{-{ $categorie->texte }}</h5>-->
                                <div class="box-tools pull-right">
                                    <!--<span data-toggle="tooltip" title="" class="badge bg-gray" data-original-title="3 New Messages">3</span>-->
                                    <button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                                    <button class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
                                </div>
                            </div><!-- /.box-header -->

                            <div class="box-body">
                                <table class="table table-striped">
                                    <thead>
                                    <!--<th>Fichiers</th>-->
<!--                                    <th>Catégorie</th>
                                    <th>Etat</th>
                                    <th>Actions</th>-->
                                    </thead>
                                    <tbody id="insertFileAjax">
                                        @foreach($category->FilesOfCategory as $file)
                                        <tr value="{{ $file->id_document }}" data_id="{{ $file->id_document }}">
                                            <td class="col-md-2"><i class="fa fa-fw 
                                                                    @if ($file->extension == 'pdf')
                                                                    fa-file-pdf-o
                                                                    @elseif ($file->extension == 'zip')
                                                                    fa-file-archive-o
                                                                    @else
                                                                    fa-file-image-o
                                                                    @endif
                                                                    "></i></td>
                                            <td  class="col-md-8">{{ $file->titre }}</td>
                                            <td  class="col-md-2"><i value="{{ $file->id_document }}" class="fa fa-fw fa-trash-o"> </i></td>
            <!--                                <td>{-{ $category->titre }}</td>
                                            <td>{-{ $article->nom }}</td>
                                            <td>{-{ $article->date }}</td>-->
                                        </tr>
                                        @endforeach

                                    </tbody>
                                </table>
                            </div><!-- /.box-body -->
                            <div class="box-footer">
                                <form action="{{ route('admin.category.addFile') }}" method="post" id="addFile" >
                                    {{ csrf_field() }}
                                    <!--<div class="input-group">-->
                                    <input name="category_{{$category->id_rubrique}}" type="file" accept=".jpg, .jpeg, .png, .pdf, .doc, .xls">
                                    <br/>
                                    <span class="input-group-btn">
                                        <button type="button" name="submitFile" id="submitFile" class="btn btn-info btn-flat pull-">{{__('common.enregistrer')}}</button>
                                    </span>
                                    <!--</div>-->
                                </form>
                            </div><!-- /.box-footer-->
                        </div><!--/.direct-chat -->
                    </div><!--/.direct-chat -->


                    <form action="{{ route('admin.category.update', $category->id_rubrique)}}" method="post">
                            {{ method_field('PUT') }}
                            {{ csrf_field() }}
                    <div class="col-md-9">
                        <div class="box box-info direct-chat direct-chat-info" >
                            <div class="box-header with-border bg-info">
                                <h3 class="box-title col-md-6 "><input class="col-md-12 form-control" id="titre" name="titre" value="{{  $category->titre }}"></h3>
                                <!--<h5 class="widget-user-desc">{-{ $categorie->texte }}</h5>-->
                                <div class="box-tools pull-right">
                                    <!--<span data-toggle="tooltip" title="" class="badge bg-gray" data-original-title="3 New Messages">3</span>-->
                                    <button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                                    <button class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
                                </div>
                            </div><!-- /.box-header -->

                            <div class="box-body">
                                    {{-- <label class="col-md-3">Titre</label><input class="col-md-9 form-control" id="titre" name="titre" value="{{  $category->titre }}"> --}}
                                    {{-- <label class="col-md-3">Corps</label> --}}
                                    <textarea class="col-md-12"  id="nicarea" name="nicarea">{{  $category->texte }}</textarea>
                                    <span class="input-group-btn ">
                                        <button type="submit" class="btn btn-info btn-flat pull-right">{{__('common.enregistrer')}}</button>
                                    </span>
                            </div><!-- /.box-body -->

                        </div><!--/.direct-chat -->
                    </div>

                </form>





                    <div class="col-md-12">
                        <div class="box box-info direct-chat direct-chat-info" >
                            <div class="box-header with-border bg-info">
                                <h3 class="box-title">{{__('back_categorie.list.liste')}}</h3>
                                <!--<h5 class="widget-user-desc">{-{ $categorie->texte }}</h5>-->
                                <div class="box-tools pull-right">
                                    <!--<span data-toggle="tooltip" title="" class="badge bg-gray" data-original-title="3 New Messages">3</span>-->
                                    <button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                                    <button class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
                                </div>
                            </div><!-- /.box-header -->

                            <div class="box-body">
                                <table id="articles" class="table table-bordered table-hover">
                                    <thead>
                                        <tr>
                                            <th>#</th>
                                            <th>{{__('back_article.list.article')}}</th>
                                            <th>{{__('back_article.list.categorie')}}</th>
                                            <th>{{__('back_article.list.auteur')}}</th>
                                            <th>{{__('back_article.list.date')}}</th>
                                            <th></th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @foreach($category->ArticlesOfCategory as $article)
                                        <tr>
                                        <?php
                                            $url = (new Urls)->getByTypeAndId('article', $article->id_article);
                                            if (is_object($url)) {
                                                $url = url($url->url);
                                            } else {
                                                $url = url('#');
                                            }
                                        ?>
                                            <td>{{ $article->id_article }}</td>
                                            <td><a href="{{ $url }}" target="_blank" alt="{{ $article->titre }}" title="{{ $article->titre }}">{{ $article->titre }}</a></td>
                                            <td>{{ $category->titre }}</td>
                                            <td>{{ $article->nom }}</td>
                                            <td>{{ $article->date }}</td>
                                            <td>
                                                @can('gate-admin')
                                                    <button class="btn btn-danger btn-sm" value="{{ $article->id_article }}"><i class="fa fa-trash-o"></i></button>
                                                @endcan
                                            </td>
                                        </tr>
                                        @endforeach
                                    </tbody>
                                </table>

                            </div><!-- /.box-body -->

                        </div><!--/.direct-chat -->
                    </div>
                </div><!-- /.box-body -->
            </div><!-- /.box -->
        </div><!-- /.col -->
    </div><!-- /.row -->
</section><!-- /.content -->
@endsection

@section('js')

<!-- SlimScroll -->
<!--<script src="{{ asset('js/jquery.slimscroll.min.js') }}"></script>-->
<script src="{{ asset('js/jquery-ui.min.js') }}"></script>
<script>

bkLib.onDomLoaded(function () {
    new nicEditor({
        iconsPath: '{{ asset("images/nicEditorIcons.gif") }}',
        buttonList: ['save', 'bold', 'italic', 'underline', 'left', 'center', 'right', 'justify', 'ol', 'ul', 'indent', 'outdent', 'fontSize', 'fontFormat', 'image', 'upload', 'link', 'unlink', 'forecolor', 'bgcolor', 'xhtml'],
    }).panelInstance('nicarea')
            ;
});


function addToCursorPosition(textareaId, value) {
    var editor = nicEditors.findEditor(textareaId);
    var range = editor.getRng();
    var editorField = editor.selElm();
    var start = range.startOffset;
    var end = range.endOffset;
    if (editorField.nodeValue != null) {
        editorField.nodeValue = editorField.nodeValue.substring(0, start) +
                value +
                editorField.nodeValue.substring(end, editorField.nodeValue.length);
    } else {
        var content = nicEditors.findEditor(textareaId).getContent().split("<br>");
        var linesCount = 0;
        var before = "";
        var after = "";
        for (var i = 0; i < content.length; i++) {
            if (linesCount < start) {
                before += content[i] + "<br>";
            } else {
                after += content[i] + "<br>";
            }
            linesCount++;
            if (content[i] != "") {
                linesCount++;
            }
        }
        nicEditors.findEditor(textareaId).setContent(before + value + after);
    }

}

$(function () {


    $("#insertFileAjax>tr").draggable({
        appendTo: '.content', //This is just a higher level DOM element
        revert: true,
        cursor: 'pointer',
        zIndex: 1500, // Make sure draggable drags above everything else
        containment: 'DOM',
        helper: 'clone' //Clone it while dragging (keep original intact)
    });


    $("#insertFileAjax")
            .on('click', "tr", function (e) {
                if (typeof (e.target.attributes[0]) == 'undefined') {
                    addToCursorPosition('nicarea', '[doc' + $(this).attr('data_id') + '/]');
                    e.preventDefault();
                } else if (e.target.attributes[0].nodeValue !== 'fa fa-fw fa-trash-o') {
                    addToCursorPosition('nicarea', '[doc' + $(this).attr('data_id') + '/]');
                    e.preventDefault();
                }
            })
            .on('click', "i.fa-trash-o", function (e) {
                swal({
                    title: "Confirmation ?",
                    text: "Voulez vous supprimer le fichier ?",
                    icon: "warning",
                    buttons: true,
                    dangerMode: true,
                })
                        .then((willDelete) => {
                            if (willDelete) {
                                $.ajax({
                                    url: '<?php echo route('admin.category.delFile', $category->id_rubrique) ?>',
                                    headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
                                    type: 'POST',
                                    dataType: "json",
                                    data: {id: $(this).attr('value')},

                                    success: function (data, textStatus, jqXHR) {
                                        swal("Fichier supprimé !", {
                                            icon: "success",
                                        });
                                        e.currentTarget.remove();
                                    },
                                    error: function (jqXHR, textStatus, errorThrown) {
                                        swal("Il semble y avoir une erreur sur le serveur, veuillez réessayer plus tard...", {
                                            icon: "error",
                                        })
                                    }

                                }).done(function (data) { })
                                  .fail(function (data) { });
                                ;
                            }
                        });

                e.preventDefault();
            });


    $("button[name='submitFile']").click(function (e) {

        var myForm = document.getElementById('addFile');
        formData = new FormData(myForm);

        swal({
            title: "Attente...",
            text: "Enregistrement en cours",
            icon: "warning",
            buttons: false,
            dangerMode: false,
        })
        $.ajax({
            url: '<?php echo route('admin.category.addFile') ?>',
            headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
            type: 'POST',
            processData: false,
            contentType: false,
            dataType: "json",
            data: formData,

            success: function (data, textStatus, jqXHR) {
                //   $(location).attr('href',"<?php //echo route(Route::current()->parameterNames[0] . '.index')     ?>");
                swal("Abracadabra! le fichier est sur le serveur !", {
                    icon: "success",
                });
                $("#insertFileAjax").append('<tr draggable="true" class="ui-draggable ui-draggable-handle" data_id="' + data.id_document + '" value="' + data.id_document + '">\n\
                            <td><i class="fa fa-fw ' + ((data.extension == 'pdf') ? 'fa-file-pdf-o' : (data.extension != 'zip') ? 'fa-file-image-o' : 'fa-file-archive-o ') + '"></i></td>\n\
                            <td>' + data.titre + '</td>\n\
                        </tr>');
                $("input[name*='category_']").val("");

            },
            error: function (jqXHR, textStatus, errorThrown) {
                swal("Il semble y avoir une erreur sur le serveur, veuillez réessayer plus tard...", {
                    icon: "error",
                })
            }
        });
    });

    @can('gate-admin')
        $('button.btn-danger').click(function(e) {
            swalConfirmDelete("{{ route('admin.article.index') }}" + "/" + $(this).attr('value'), e);
        });
    @endcan
});
</script>

@endsection
@section('css')
<!-- DataTables -->
<link rel="stylesheet" href="{{ asset('css/dataTables.bootstrap.min.css') }}">
@endsection
