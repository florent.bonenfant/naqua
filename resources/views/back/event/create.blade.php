@extends('back.layouts.app')

@section('content')

<section class="content-header">
    <h1>
            {{__('back_events.title.create')}}
    </h1>
    <ol class="breadcrumb">
        <li><a href="{{ route('admin.back.home')}}"><i class="fa fa-dashboard"></i> {{__('back_common.ariane.home')}}</a></li>
        <li><a href="{{ route('admin.evenement.index')}}">{{__('back_common.ariane.events')}}</a></li>
        <li class="active">{{__('back_common.ariane.action.create')}}</li>
    </ol>
</section>

<section class="content">
@if ($errors->any())
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif
<div class="row">
<form action='{{ route('admin.evenement.store') }}' method='post' enctype="multipart/form-data">
		{{ csrf_field() }}
			@include('back.event.form')
		</form>
	</div>
	</section>



@endsection

@include('back.event.plugins')