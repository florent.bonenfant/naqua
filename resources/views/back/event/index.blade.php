<?php
/*
 *  @author  Florent Bonenfant <social1@niweb.eu>
 *  @version 1.0
 */
?>
@extends('back.layouts.app')

@section('content')

<section class="content-header">
    <h1>
            {{__('back_events.title.index')}}
    </h1>
    <ol class="breadcrumb">
        <li><a href="{{ route('admin.back.home')}}"><i class="fa fa-dashboard"></i> {{__('back_common.ariane.home')}}</a></li>
        <li class="active">{{__('back_common.ariane.events')}}</li>
    </ol>
</section>

<!-- Main content -->
<section class="content">
    <div class="row">
        <div class="col-xs-12">

            <div class="box">

                <div class="box-body">
                        <p class="pull-right">
                            @can('gate-staff')
                                <a href="{{ route('admin.evenement.create')}}" class="btn btn-info btn-flat">
                                    <i class="fa fa-plus"></i> {{ __('common.ajouter')}}
                                </a>
                            @endcan
                        </p>
                    <table id="evenements" class="table table-bordered table-hover">
                        <thead>
                            <tr>
                                <th>#</th>
                                <th>{{__('back_events.events_titre')}}</th>
                                <th>{{__('back_events.events_inscriptions')}}</th>
                                <th>{{__('back_events.events_lieu')}}</th>
                                <th>{{__('back_events.events_date_debut')}}</th>
                                <th>{{__('back_events.events_date_fin')}}</th>
                                <th>{{__('back_events.events_action')}}</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($evenements as $event)
                            <tr>
                                <td>{{ $event->id_evenement }}</td>
                                <td>{{ $event->titre }}</td>
                                <td>{{ $event->inscription }} / {{ $event->places }}</td>
                                <td>{{ $event->lieu }}</td>
                                <td>{{ \Carbon\Carbon::parse($event->date_debut)->format('d/m/Y H:i') }}</td>
                                <td>{{ \Carbon\Carbon::parse($event->date_fin)->format('d/m/Y H:i') }}</td>
                                <td>
                                    <a href="{{ route('admin.evenement.edit', $event->id_evenement) }}" target="_self"><button class="btn btn-default btn-sm"><i class="fa fa-pencil"></i></button></a>
                                    <button class="btn btn-danger btn-sm" value="{{ $event->id_evenement }}"><i class="fa fa-trash-o"></i></button>
                                </td>
                            </tr>
                            @endforeach
                            </tfoot>
                    </table>
                </div><!-- /.box-body -->
            </div><!-- /.box -->
        </div><!-- /.col -->
    </div><!-- /.row -->
</section><!-- /.content -->


@endsection

@section('js')
<script src="{{ asset('js/jquery-ui.min.js') }}"></script>
<!-- DataTables -->
<script src="{{ asset('js/jquery.dataTables.min.js') }}"></script>
<script src="{{ asset('js/dataTables.bootstrap.min.js') }}"></script>
<!-- SlimScroll -->
<script src="{{ asset('js/jquery.slimscroll.min.js') }}"></script>

<script>
$(function () {
//$("#example1").DataTable();
    $('#evenements').DataTable({
        "paging": true,
        "lengthChange": false,
        "searching": true,
        "ordering": true,
        "info": true,
        "autoWidth": true,
        "pageLength": 15,
        "order": [[1, "desc"]],

        "language":
                {
                    "sProcessing": "Traitement en cours...",
                    "sSearch": "Rechercher&nbsp;:",
                    "sLengthMenu": "Afficher _MENU_ &eacute;l&eacute;ments",
                    "sInfo": "Affichage de l'&eacute;l&eacute;ment _START_ &agrave; _END_ sur _TOTAL_ &eacute;l&eacute;ments",
                    "sInfoEmpty": "Affichage de l'&eacute;l&eacute;ment 0 &agrave; 0 sur 0 &eacute;l&eacute;ment",
                    "sInfoFiltered": "(filtr&eacute; de _MAX_ &eacute;l&eacute;ments au total)",
                    "sInfoPostFix": "",
                    "sLoadingRecords": "Chargement en cours...",
                    "sZeroRecords": "Aucun &eacute;l&eacute;ment &agrave; afficher",
                    "sEmptyTable": "Aucune donn&eacute;e disponible dans le tableau",
                    "oPaginate": {
                        "sFirst": "Premier",
                        "sPrevious": "Pr&eacute;c&eacute;dent",
                        "sNext": "Suivant",
                        "sLast": "Dernier"
                    },
                    "oAria": {
                        "sSortAscending": ": activer pour trier la colonne par ordre croissant",
                        "sSortDescending": ": activer pour trier la colonne par ordre d&eacute;croissant"
                    }
                }

    });

    $('button.btn-danger').click(function(e) {
        swalConfirmDelete("{{ route('admin.evenement.index') }}" + "/" + $(this).attr('value'), e);
    });

});
</script>

@endsection
@section('css')
<!-- DataTables -->
<link rel="stylesheet" href="{{ asset('css/dataTables.bootstrap.min.css') }}">
@endsection
{{-- {{ route('admin.evenement.destroy', $event->id_evenement) }} --}}