
				<div class="box">
					<div class="box-body">
						<div class="col-xs-12">
							<div class="box box-primary">

								<div class="form-group {{ $errors->has('titre') ? 'has-error' : '' }}">
									<label class="col-sm-3 col-form-label text-md-right" for="titre">{{__('back_events.form.titre')}}</label>
									<div class="col-md-9">
										<div class="input-group date">
											<div class="input-group-addon">
												<i class="fa fa-flag-o"></i>
											</div>
											<input type="text" class="form-control" id="titre" name="titre" value="{{ old('titre') ?? $event->titre ?? '' }}" />
											<span class="help-block">{!! $errors->first('titre', '<div class="invalid-feedback">:message</div>') !!}</span>
										</div>
									</div>
								</div>
								<div class="form-group {{ $errors->has('descriptif') ? 'has-error' : '' }}">
									<label class="col-sm-3 col-form-label text-md-right" for="descriptif">{{__('back_events.form.description')}}</label>
									<div class="col-md-9">
										<textarea type="text" class="form-control" id="descriptif" name="descriptif">{{ old('descriptif') ?? $event->descriptif ?? '' }}</textarea>
										<span class="help-block">{!! $errors->first('descriptif', '<div class="invalid-feedback">:message</div>') !!}</span>
									</div>
								</div>

								<div class="form-group {{ $errors->has('date_debut') ? 'has-error' : '' }}">
									<label class="col-sm-3 col-form-label text-md-right" for="date_debut">{{__('back_events.form.debut')}}</label>
									<div class="col-md-9">
										<div class="input-group date">
											<div class="input-group-addon">
												<i class="fa fa-calendar"></i>
											</div>
											<input type="text" class="form-control" id="date_debut" name="date_debut" value="{{ old('date_debut') ?? (empty($event->date_debut) ?'': Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $event->date_debut)->format('d/m/Y H:i')) }}" placeholder="Clique sur l'icone pour afficher le calendrier">
											<span class="help-block">{!! $errors->first('date_debut', '<div class="invalid-feedback">:message</div>') !!}</span>
										</div>
									</div>
								</div>

								<div class="form-group {{ $errors->has('date_fin') ? 'has-error' : '' }}">
									<label class="col-sm-3 col-form-label text-md-right" for="date_fin">{{__('back_events.form.fin')}}</label>
									<div class="col-md-9">
										<div class="input-group date">
											<div class="input-group-addon">
												<i class="fa fa-calendar"></i>
											</div>
											<input type="text" class="form-control" id="date_fin" name="date_fin" value="{{ old('date_fin') ?? (empty($event->date_fin) ?'': Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $event->date_fin)->format('d/m/Y H:i')) }}" placeholder="jj/mm/aaaa 23:59">
											<span class="help-block">{!! $errors->first('date_fin', '<div class="invalid-feedback">:message</div>') !!}</span>
										</div>
									</div>
								</div>

								<div class="form-group {{ $errors->has('lieu') ? 'has-error' : '' }}">
									<label class="col-sm-3 col-form-label text-md-right" for="lieu">{{__('back_events.form.lieu')}}</label>
									<div class="col-md-9">
										<div class="input-group date">
											<div class="input-group-addon">
												<i class="fa fa-map-signs"></i>
											</div>
											<input type="text" class="form-control pull-right" id="lieu" name="lieu" value="{{ old('lieu') ?? $event->lieu ?? '' }}">
											<span class="help-block">{!! $errors->first('lieu', '<div class="invalid-feedback">:message</div>') !!}</span>
										</div>
									</div>
								</div>

								<div class="form-group {{ $errors->has('adresse') ? 'has-error' : '' }}">
									<label class="col-sm-3 col-form-label text-md-right" for="adresse">{{__('back_events.form.adresse')}}</label>
									<div class="col-md-9">
										<textarea type="text" class="form-control" id="adresse" name="adresse">{{ old('adresse') ?? $event->adresse ?? '' }}</textarea>
										<span class="help-block">{!! $errors->first('adresse', '<div class="invalid-feedback">:message</div>') !!}</span>
									</div>
								</div>

								<div class="form-group {{ $errors->has('places') ? 'has-error' : '' }}">
									<label class="col-sm-3 col-form-label text-md-right" for="places">{{__('back_events.form.places')}}</label>
									<div class="col-md-9">
										<div class="input-group date">
											<div class="input-group-addon">
												<i class="fa fa-th-list"></i>
											</div>
											<input type="text" class="form-control" id="places" name="places" value="{{ old('places') ?? $event->places ?? '' }}">
											<span class="help-block">{!! $errors->first('places', '<div class="invalid-feedback">:message</div>') !!}</span>
										</div>
									</div>
								</div>

								<div class="form-group {{ $errors->has('couleur') ? 'has-error' : '' }}">
									<label class="col-sm-3 col-form-label text-md-right" for="couleur">{{__('back_events.form.couleur')}}</label>
									<div class="col-md-9">
										<div class="input-group date">
											<div class="input-group-addon">
												<i class="fa fa-delicious"></i>
											</div>
											<input type="text" class="form-control" id="couleur" name="couleur" value="{{ old('couleur') ?? $event->couleur ?? '' }}">
											<span class="help-block">{!! $errors->first('couleur', '<div class="invalid-feedback">:message</div>') !!}</span>
										</div>
									</div>
								</div>

								<div class="form-group {{ $errors->has('url') ? 'has-error' : '' }}">
									<label class="col-sm-3 col-form-label text-md-right" for="url">{{__('back_events.form.lien')}}</label>
									<div class="col-md-9">
										<div class="input-group date">
											<div class="input-group-addon">
												<i class="fa fa-globe"></i>
											</div>
											<input type="text" class="form-control" id="url" name="url" value="{{ old('url') ?? $event->url ?? '' }}">
											<span class="help-block">{!! $errors->first('url', '<div class="invalid-feedback">:message</div>') !!}</span>
										</div>
									</div>
								</div>

							<input type='submit' name='ok' value='Enregistrer' class="btn btn-primary pull-right" /><br/><br/> {{__('common.champs_obligatoires')}}
						</div>
					</div>
				</div>
			</div>
