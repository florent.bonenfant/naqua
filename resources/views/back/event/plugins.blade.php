@section('css')
<link href="{{ asset('css/bootstrap-colorpicker.min.css') }}" rel="stylesheet" media="projection, screen, tv">
<link href="{{ asset('css/daterangepicker.min.css') }}" rel="stylesheet" media="projection, screen, tv">
<style>
	.form-group {
		margin: 2px 0;
		height:35px;
		clear:both;
	}
	i.fa {
		width:30px;
	}
	</style>
@endsection
@section('js')
<script src="{{ asset('js/moment.min.js') }}"></script>
<script src="{{ asset('js/daterangepicker.min.js') }}"></script>
<script src="{{ asset('js/bootstrap-colorpicker.min.js') }}"></script>

<script>
	$( function() {
		// daterangepicker
		$('.fa-calendar').daterangepicker({
			"timePicker": true,
			"timePicker24Hour": true,
			"timePickerIncrement": 15,
			"hour": 15,
			"startDate": "{{ (isset($event->date_debut) ? Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $event->date_debut)->format('m/d/Y H:i') : date('m/d/Y H:i')) }}",
			"endDate": "{{ (isset($event->date_fin) ? Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $event->date_fin)->format('m/d/Y H:i') : Carbon\Carbon::now()->addDays(4)->format('m/d/Y H:i')) }}"
		}, function(start, end, label) {
			$('#date_debut').val(start.format('DD/MM/YYYY HH:mm'));
			$('#date_fin').val(end.format('DD/MM/YYYY HH:mm'));
		});

		//Colorpicker
    $('#couleur').colorpicker()

	} );
</script>
@endsection