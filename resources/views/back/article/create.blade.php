<?php
/*
 *  @author  Florent Bonenfant <social1@niweb.eu>
 *  @version 1.0
 */

 $categories = (new \App\Repositories\CategoryRepository)->getAll();

?>
@extends('back.layouts.app')

@section('content')

<section class="content-header">
    <h1>
            {{__('back_article.titre.create')}}
    </h1>
    <ol class="breadcrumb">
        <li><a href="{{ route('admin.back.home')}}"><i class="fa fa-dashboard"></i> {{__('back_common.ariane.home')}}</a></li>
        <li><a href="{{ route('admin.article.index')}}">{{__('back_common.ariane.article')}}</a></li>
    </ol>
</section>


<!-- Main content -->
<section class="content">
    <div class="row">
        <div class="col-xs-12">

            <div class="box">
                <!--                <div class="box-header">
                                    <h3 class="box-title">Liste des articles</h3>
                                </div> /.box-header -->
                <div class="box-body">


                    {{-- <div class="col-md-3">
                        <div class="box box-info direct-chat direct-chat-info" >
                            <div class="box-header with-border bg-info">
                                <h3 class="box-title">Documents</h3>
                                <!--<h5 class="widget-user-desc">{-{ $categorie->texte }}</h5>-->
                                <div class="box-tools pull-right">
                                    <!--<span data-toggle="tooltip" title="" class="badge bg-gray" data-original-title="3 New Messages">3</span>-->
                                    <button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                                    <button class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
                                </div>
                            </div><!-- /.box-header -->

                        </div><!--/.direct-chat -->
                    </div><!--/.direct-chat --> --}}
                    <div class="col-md-12">
                    <form action="{{ route('admin.article.store') }}" method="post">
                            {{ csrf_field() }}
                @include('back.article.form')

            </form>
        </div>


                </div><!-- /.box-body -->
            </div><!-- /.box -->
        </div><!-- /.col -->
    </div><!-- /.row -->
</section><!-- /.content -->
@endsection

@section('js')

<!-- SlimScroll -->
<!--<script src="{{ asset('js/jquery.slimscroll.min.js') }}"></script>-->
<script src="{{ asset('js/jquery-ui.min.js') }}"></script>
<script>

bkLib.onDomLoaded(function () {
    new nicEditor({
        iconsPath: '{{ asset("images/nicEditorIcons.gif") }}',
        buttonList: ['save', 'bold', 'italic', 'underline', 'left', 'center', 'right', 'justify', 'ol', 'ul', 'indent', 'outdent', 'fontSize', 'fontFormat', 'image', 'upload', 'link', 'unlink', 'forecolor', 'bgcolor', 'xhtml'],
    }).panelInstance('nicarea')
            ;
});


function addToCursorPosition(textareaId, value) {
    var editor = nicEditors.findEditor(textareaId);
    var range = editor.getRng();
    var editorField = editor.selElm();
    var start = range.startOffset;
    var end = range.endOffset;
    if (editorField.nodeValue != null) {
        editorField.nodeValue = editorField.nodeValue.substring(0, start) +
                value +
                editorField.nodeValue.substring(end, editorField.nodeValue.length);
    } else {
        var content = nicEditors.findEditor(textareaId).getContent().split("<br>");
        var linesCount = 0;
        var before = "";
        var after = "";
        for (var i = 0; i < content.length; i++) {
            if (linesCount < start) {
                before += content[i] + "<br>";
            } else {
                after += content[i] + "<br>";
            }
            linesCount++;
            if (content[i] != "") {
                linesCount++;
            }
        }
        nicEditors.findEditor(textareaId).setContent(before + value + after);
    }

}

$(function () {


    $("#insertFileAjax>tr").draggable({
        appendTo: '.content', //This is just a higher level DOM element
        revert: true,
        cursor: 'pointer',
        zIndex: 1500, // Make sure draggable drags above everything else
        containment: 'DOM',
        helper: 'clone' //Clone it while dragging (keep original intact)
    });


    $("button[name='submitFile']").click(function (e) {

        var myForm = document.getElementById('addFile');
        formData = new FormData(myForm);

        swal({
            title: "Attente...",
            text: "Enregistrement en cours",
            icon: "warning",
            buttons: false,
            dangerMode: false,
        })
        $.ajax({
            url: '<?php echo route('admin.article.addFile') ?>',
            headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
            type: 'POST',
            processData: false,
            contentType: false,
            dataType: "json",
            data: formData,

            success: function (data, textStatus, jqXHR) {
                //   $(location).attr('href',"<?php //echo route(Route::current()->parameterNames[0] . '.index')     ?>");
                swal("Abracadabra! le fichier est sur le serveur !", {
                    icon: "success",
                });
                $("#insertFileAjax").append('<tr draggable="true" class="ui-draggable ui-draggable-handle" data_id="' + data.id_document + '" value="' + data.id_document + '">\n\
                            <td><i class="fa fa-fw ' + ((data.extension == 'pdf') ? 'fa-file-pdf-o' : (data.extension != 'zip') ? 'fa-file-image-o' : 'fa-file-archive-o ') + '"></i></td>\n\
                            <td>' + data.titre + '</td>\n\
                        </tr>');
                $("input[name*='article_']").val("");

            },
            error: function (jqXHR, textStatus, errorThrown) {
                swal("Il semble y avoir une erreur sur le serveur, veuillez réessayer plus tard...", {
                    icon: "error",
                })
            }
        });
    });

});
</script>

@endsection
@section('css')
<!-- DataTables -->
<link rel="stylesheet" href="{{ asset('css/dataTables.bootstrap.min.css') }}">
@endsection
