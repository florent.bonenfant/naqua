<?php

use Illuminate\Support\Carbon;
use App\Models\Urls;
/*
 *  @author  Florent Bonenfant <social1@niweb.eu>
 *  @version 1.0
 */

?>
@extends('back.layouts.app')

@section('content')

<section class="content-header">
    <h1>
        {{__('back_article.titre.mes_articles')}}
    </h1>
    <ol class="breadcrumb">
        <li><a href="{{ route('admin.back.home')}}"><i class="fa fa-dashboard"></i> {{__('back_common.ariane.home')}}</a></li>
        <li><a href="{{ route('admin.category.index')}}">{{__('back_common.ariane.category')}}</a></li>
        <li class="active">{{__('back_common.ariane.article')}}</li>
    </ol>
</section>

<!-- Main content -->
<section class="content">
    <div class="row">
        <div class="col-xs-12">

            <div class="box">
                <!--                <div class="box-header">
                                    <h3 class="box-title">Liste des articles</h3>
                                </div> /.box-header -->
                <div class="box-body">
                        <p class="pull-right">
                            @can('gate-staff')
                                <a href="{{ route('admin.article.create')}}" class="btn btn-info btn-flat">
                                    <i class="fa fa-plus"></i> {{ __('common.ajouter')}}
                                </a>
                            @endcan
                        </p>
                    <table id="articles" class="table table-bordered table-hover">
                        <thead>
                            <tr>
                                <th>#</th>
                                <th>{{__('back_article.list.article')}}</th>
                                <th>{{__('back_article.list.categorie')}}</th>
                                <th>{{__('back_article.list.auteur')}}</th>
                                <th>{{__('back_article.list.date')}}</th>
                                <th></th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($articles as $article)
                            <tr><?php
                                $url = (new Urls)->getByTypeAndId('article', $article->id_article);
                                if (is_object($url)) {
                                    $url = url($url->url);
                                } else {
                                    $url = url('#');
                                }
                            ?>
                                <td>{{ $article->id_article }}</td>
                                <td><a href="{{ $url }}" target="_blank" title="{{ $article->article }}">{{ $article->article }}</a></td>
                                <td>{{ $article->rubrique }}</td>
                                <td>{{ $article->nom }}</td>
                                <td>{{ (isset($article->date_modif) ? Carbon::createFromFormat('Y-m-d H:i:s', $article->date_modif)->format('d/m/Y H:i:s') : '' ) }}</td>
                                <td>
                                    @if (isset($article->id_article))
                                        <a href="{{ route('admin.article.edit', $article->id_article) }}" target="_self"><button class="btn btn-default btn-sm"><i class="fa fa-pencil"></i></button></a>
                                        @can('gate-admin')
                                            <button class="btn btn-danger btn-sm" value="{{ $article->id_article }}"><i class="fa fa-trash-o"></i></button>
                                        @endcan
                                    @endif
                                </td>
                            </tr>
                            @endforeach
                            </tfoot>
                    </table>
                </div><!-- /.box-body -->
            </div><!-- /.box -->
        </div><!-- /.col -->
    </div><!-- /.row -->
</section><!-- /.content -->


@endsection

@section('js')

<!-- DataTables -->
<script src="{{ asset('js/jquery.dataTables.min.js') }}"></script>
<script src="{{ asset('js/dataTables.bootstrap.min.js') }}"></script>
<!-- SlimScroll -->
<script src="{{ asset('js/jquery.slimscroll.min.js') }}"></script>

<script>
$(function () {
//$("#example1").DataTable();
    $('#articles').DataTable({
        "paging": true,
        "lengthChange": false,
        "searching": true,
        "ordering": true,
        "info": true,
        "autoWidth": true,
        "pageLength": 15,
        "order": [[4, "desc"]],
//    "lengthMenu": [ [20, 35, 50, -1], [20, 35, 50, "All"] ]

        "language":
                {
                    "sProcessing": "Traitement en cours...",
                    "sSearch": "Rechercher&nbsp;:",
                    "sLengthMenu": "Afficher _MENU_ &eacute;l&eacute;ments",
                    "sInfo": "Affichage de l'&eacute;l&eacute;ment _START_ &agrave; _END_ sur _TOTAL_ &eacute;l&eacute;ments",
                    "sInfoEmpty": "Affichage de l'&eacute;l&eacute;ment 0 &agrave; 0 sur 0 &eacute;l&eacute;ment",
                    "sInfoFiltered": "(filtr&eacute; de _MAX_ &eacute;l&eacute;ments au total)",
                    "sInfoPostFix": "",
                    "sLoadingRecords": "Chargement en cours...",
                    "sZeroRecords": "Aucun &eacute;l&eacute;ment &agrave; afficher",
                    "sEmptyTable": "Aucune donn&eacute;e disponible dans le tableau",
                    "oPaginate": {
                        "sFirst": "Premier",
                        "sPrevious": "Pr&eacute;c&eacute;dent",
                        "sNext": "Suivant",
                        "sLast": "Dernier"
                    },
                    "oAria": {
                        "sSortAscending": ": activer pour trier la colonne par ordre croissant",
                        "sSortDescending": ": activer pour trier la colonne par ordre d&eacute;croissant"
                    }
                }

    });

});
</script>

@endsection
@section('css')
<!-- DataTables -->
<link rel="stylesheet" href="{{ asset('css/dataTables.bootstrap.min.css') }}">
@endsection
