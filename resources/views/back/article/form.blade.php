{{-- {{ old('titre') ?? $event->titre ?? '' }} --}}


    <div class="box box-info direct-chat direct-chat-info" >
        <div class="box-header with-border bg-info">
            <h3 class="box-title col-md-6 ">&nbsp;</h3>
            <div class="box-tools pull-right">
                <button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                <button class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
            </div>
        </div><!-- /.box-header -->

        <div class="box-body">

                <div class="form-group">
                    <label class="col-md-3 control-label">{{__('back_article.list.categorie')}}</label>
                    <div class="col-sm-9">
                        <select class="form-control" name="rubrique">
                            @foreach($categories  as $categorie)
                                @if($categorie->id_rubrique == $categorie->id_secteur)
                                    <option value="{{ $categorie->id_rubrique }}" >{{ $categorie->titre }}</option>
                                    @foreach($categories  as $ssCategorie)
                                        @if($ssCategorie->id_parent == $categorie->id_rubrique)
                                        <option value="{{ $ssCategorie->id_rubrique }}" >{{ ($categorie->id_parent == $categorie->id_rubrique ?: '-') }} {{ $ssCategorie->titre }}</option>
                                        @endif
                                    @endforeach
                                @endif
                            @endforeach
                        </select>
                    </div>
                </div>

            <div class="form-group">
                <label class="col-md-3 control-label">{{__('back_article.list.titre')}}</label>
                <div class="col-sm-9">
                    <input class="col-md-9 form-control" id="titre" name="titre" value="{{ old('titre') ?? $article->titre ?? '' }}">
                </div>
            </div>

                <label class="col-md-3 control-label">{{__('back_article.list.contenu')}}</label>
                <div class="col-sm-12">
                <textarea class="form-control"  id="nicarea" name="nicarea">{{ old('nicarea') ?? $article->texte ?? '' }}</textarea>
                <span class="input-group-btn ">
                    <button type="submit" class="btn btn-info btn-flat pull-right">{{__('common.enregistrer')}}</button>
                </span>
            </div>
        </div><!-- /.box-body -->

    </div><!--/.direct-chat -->

