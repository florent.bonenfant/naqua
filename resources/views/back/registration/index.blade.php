<?php
/*
 *  @author  Florent Bonenfant <social1@niweb.eu>
 *  @version 1.0
 */
//dd($registration);
?>
@extends('back.layouts.app')

@section('content')
<?php
$couleurInscrit = '#E8E9EA';
//	$couleurAccident = '#FFC6C6';
//$couleurInteret = '#C6DAFF';
$couleurAccident = 'text-align:left; background-color:#FFC6C6;width:200px;';
$couleurInteret = 'text-align:center; background-color:#C6DAFF;width:50px;';
$couleurValide = 'text-align:center; background-color:#4BB756;width:50px;';
$couleurInalide = 'text-align:center; background-color:#DB1E00;width:50px;';
?>



<section class="content-header">
    <h1>
        Inscriptions
    </h1>
    <ol class="breadcrumb">
        <li><a href="{{ route('admin.back.home')}}"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Inscriptions</li>
    </ol>
</section>

<!-- Main content -->
<section class="content">
    <div class="row">
        <div class="col-xs-12">


            <div class="box-body">

                <form action="./export-membres" name="formInscriptions" method="post" accept-charset="utf-8" enctype="multipart/form-data">
                    {{ csrf_field() }}
                    <table  class="table table-bordered table-hover">
                        <thead style="font-weight:bold;background-color:#D3D3D3;">
                        <td></td>
                        <td>Id</td>
                        <td>Date</td>
                        <td>Prenom</td>
                        <td>Nom</td>
                        <td>Tél</td>
                        <td>Contact Accident</td>
                        <td>Relation Accident</td>
                        <td>Tél Accident</td>
                        <td>Allergie à l'aspirine</td>
                        <td>Nage</td>
                        <td>Apnée</td>
                        <td>Palmes</td>
                        <td title="Perfectionnement">Perfect.</td>
                        <td title="Secourisme">Secours</td>
                        <td title="Certificat médicale">Certificat Médicale</td>
                        </thead>
                        <tbody>

                            @foreach($registration as $inscrit)
                            <tr>
                                <td bgcolor="{{ $couleurInscrit }}"><input type="checkbox"  id="id_{{  $inscrit->id }}" name="id_{{  $inscrit->id }}" checked="check"/></td>
                                <td bgcolor="{{ $couleurInscrit }}">{{ $inscrit->id  }}</td>
                                <td bgcolor="{{ $couleurInscrit }}">{{ \Carbon\Carbon::parse($inscrit->date_inscription)->format('d/m/Y') }}</td>
                                <td bgcolor="{{ $couleurInscrit }}"><a href="mailto:{{ $inscrit->email }}" >{{ $inscrit->prenom }}</a></td>
                                <td bgcolor="{{ $couleurInscrit }}">{{ $inscrit->nom }}</td>
                                <td bgcolor="{{ $couleurInscrit }}">{{ $inscrit->teldom }}<br/>{{ $inscrit->telmobile }}</td>
                                <td style="{{ $couleurAccident }}">{{ $inscrit->nomaccident }}</td>
                                <td style="{{ $couleurAccident }}">{{ $inscrit->lienaccident }}</td>
                                <td style="{{ $couleurAccident }}">{{ $inscrit->telaccident }}</td>	
                                <td style="{{ ($inscrit->allergieaspirine == 1) ? $couleurInalide : $couleurValide  }}">{{  ($inscrit->allergieaspirine == '0' ? 'non' : 'oui')  }}</td>
                                <td style="{{ ($inscrit->interetnagelibre == 'oui') ? $couleurValide : $couleurInteret  }}">{{ ($inscrit->interetnagelibre == 'oui') ? 'X' : '-'  }}</td>
                                <td style="{{ ($inscrit->interetapnee == 'oui') ? $couleurValide : $couleurInteret  }}">{{ ($inscrit->interetapnee == 'oui') ? 'X' : '-'  }}</td>
                                <td style="{{ ($inscrit->interetpalmes == 'oui') ? $couleurValide : $couleurInteret  }}">{{ ($inscrit->interetpalmes == 'oui') ? 'X' : '-'  }}</td>
                                <td style="{{ ($inscrit->interetperfect == 'oui') ? $couleurValide : $couleurInteret  }}">{{ ($inscrit->interetperfect == 'oui') ? 'X' : '-'  }}</td>
                                <td style="{{ ($inscrit->interetsecourisme == 'oui') ? $couleurValide : $couleurInteret  }}">{{ ($inscrit->interetsecourisme == 'oui') ? 'X' : '-'  }}</td>
                                <td style="{{ ($inscrit->certificats != '') ? $couleurValide : $couleurInteret  }}">{!! ($inscrit->certificats != '') ? '<a href="' . url(DISK_PATH_CERTIFICATS . $inscrit->certificats) . '" target="_blank" style="color:black;">Afficher</a>' : '-' !!}</td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                    <input type="submit" />
                </form>



            </div><!-- /.box-body -->
            <!--</div> /.box -->
        </div><!-- /.col -->
    </div><!-- /.row -->
</section><!-- /.content -->



@endsection

