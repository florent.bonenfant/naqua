// Menu
jQuery(document).ready(function() {
	$("div#menu ul.menuN2").hide();
	$("div#menu ul#open").show();
	$("div#menu span.menuN1" ).click(function(){
		$("div#menu ul.menuN2:visible").slideUp("medium");
		$("div#menu ul.menuN3:visible").slideUp("medium");
		$(this).next().slideDown("medium");
		return false;
	});

	$("li.menu2_on").parents("ul").show();
	$("li.menu1_on").parents("ul").show();
	$("li.menu1_on").children("ul").show();
});